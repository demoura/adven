// adven-macros <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

extern crate proc_macro;

use proc_macro::TokenStream;
use proc_macro_error::*;
use quote::{quote, quote_spanned};

#[proc_macro]
#[proc_macro_error]
pub fn adven_asset(input: TokenStream) -> TokenStream {
	let input = syn::parse_macro_input!(input as syn::LitStr);

	let uuid = match uuid::Uuid::parse_str(&input.value()) {
		Ok(val) => val,
		Err(what) => abort_call_site!("Couldn't parse uuid {} due to {}", input.value(), what),
	};

	let uuid_fields = uuid.as_fields();
	let ident = quote::format_ident!(
		"ADV_ASSETS_{:X}_{:X}_{:X}_{:X}",
		uuid_fields.0,
		uuid_fields.1,
		uuid_fields.2,
		u64::from_be_bytes(*uuid_fields.3)
	);

	TokenStream::from(quote_spanned! {input.span()=> unsafe { &#ident } })
}

#[proc_macro_derive(Asset)]
pub fn derive_asset(input: TokenStream) -> TokenStream {
	let input = syn::parse_macro_input!(input as syn::DeriveInput);

	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let ident = input.ident;

	let expanded = quote! {
		impl #impl_generics ::adven_assets::Asset for #ident #ty_generics #where_clause {
			fn as_any(&self) -> &dyn ::std::any::Any {
				self
			}

			fn as_exportable_asset(&self) -> Option<&dyn ::adven_assets::ExportableAsset> {
				None
			}
		}
	};

	TokenStream::from(expanded)
}

#[proc_macro_attribute]
pub fn exportable_asset(_args: TokenStream, input: TokenStream) -> TokenStream {
	let input = syn::parse_macro_input!(input as syn::DeriveInput);

	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let ident = input.ident.clone();

	let expanded = quote! {
		#input

		impl #impl_generics ::adven_assets::Asset for #ident #ty_generics #where_clause {
			fn as_any(&self) -> &dyn ::std::any::Any {
				self
			}

			fn as_exportable_asset(&self) -> Option<&dyn ::adven_assets::core::ExportableAsset> {
				Some(self)
			}
		}
	};

	TokenStream::from(expanded)
}

#[proc_macro_derive(SerdeAsset)]
pub fn derive_serde_asset(input: TokenStream) -> TokenStream {
	let input = syn::parse_macro_input!(input as syn::DeriveInput);

	let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();

	let ident = input.ident;

	let expanded = quote! {
		#[typetag::serde]
		impl #impl_generics ::adven_assets::SerdeAsset for #ident #ty_generics #where_clause {
			fn into_asset(self: Box<Self>) -> Box<dyn ::adven_assets::Asset> {
				self
			}
		}
	};

	TokenStream::from(expanded)
}
