// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::display::SpriteShapeSize;
use serde::Deserialize;

use crate::{
	assets::{ImageIndexed4bpp, ImageIndexed8bpp, Sprite, Tiles},
	converters,
	core::{Asset, AssetProcessor, RawAssetId},
	AssetId, AssetRepositoryView,
};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Deserialize, Debug)]
pub struct Sprite4bppProcessor {
	sprite_id: AssetId<Sprite>,
	shape_size: SpriteShapeSize,
	frames: Vec<AssetId<ImageIndexed4bpp>>,
}
#[derive(Deserialize, Debug)]
pub struct Sprite8bppProcessor {
	sprite_id: AssetId<Sprite>,
	shape_size: SpriteShapeSize,
	frames: Vec<AssetId<ImageIndexed8bpp>>,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

#[typetag::deserialize(name = "adven::Sprite4bppProcessor")]
impl AssetProcessor for Sprite4bppProcessor {
	/// The ids of the assets this processor requires as input.
	fn inputs(&self) -> Vec<RawAssetId> {
		self.frames
			.iter()
			.map(|&asset_id| asset_id.into())
			.collect()
	}

	/// The ids of the assets this processor produces.
	fn outputs(&self) -> Vec<RawAssetId> {
		vec![self.sprite_id.into()]
	}

	/// Process the `input` assets and returns the new generate assets.
	fn process(
		&self,
		inputs: &AssetRepositoryView<'_>,
	) -> anyhow::Result<Vec<(RawAssetId, Box<dyn Asset>)>> {
		let mut frames = Vec::<ImageIndexed4bpp>::new();
		for &frame_id in &self.frames {
			frames.push(inputs.get(frame_id)?.clone());
		}

		let frames = converters::sprite::convert_frames4bpp(self.shape_size, frames)?;

		let sprite = Sprite {
			shape_size: self.shape_size,
			frame_count: u16::try_from(self.frames.len())?,
			frames: Tiles::Color4Bit(frames),
		};

		Ok(vec![(self.sprite_id.into(), Box::new(sprite))])
	}
}

#[typetag::deserialize(name = "adven::Sprite8bppProcessor")]
impl AssetProcessor for Sprite8bppProcessor {
	/// The ids of the assets this processor requires as input.
	fn inputs(&self) -> Vec<RawAssetId> {
		self.frames
			.iter()
			.map(|&asset_id| asset_id.into())
			.collect()
	}

	/// The ids of the assets this processor produces.
	fn outputs(&self) -> Vec<RawAssetId> {
		vec![self.sprite_id.into()]
	}

	/// Process the `input` assets and returns the new generate assets.
	fn process(
		&self,
		inputs: &AssetRepositoryView<'_>,
	) -> anyhow::Result<Vec<(RawAssetId, Box<dyn Asset>)>> {
		let mut frames = Vec::<ImageIndexed8bpp>::new();
		for &frame_id in &self.frames {
			frames.push(inputs.get(frame_id)?.clone());
		}

		let frames = converters::sprite::convert_frames8bpp(self.shape_size, frames)?;

		let sprite = Sprite {
			shape_size: self.shape_size,
			frame_count: u16::try_from(self.frames.len())?,
			frames: Tiles::Color8Bit(frames),
		};

		Ok(vec![(self.sprite_id.into(), Box::new(sprite))])
	}
}
