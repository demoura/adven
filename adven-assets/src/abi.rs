// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use core::marker::PhantomData;

use repr_writer::{ByteOrder, LittleEndian, Reloc, ReprAbi};

////////////////////////////////////////////////////////////////////////////////
// Constants
////////////////////////////////////////////////////////////////////////////////

pub const ARM_V4T_ABI: ReprAbi = ReprAbi {
	align_of_bool: 1,
	align_of_char: 4,

	align_of_f32: 4,
	align_of_f64: 8,

	align_of_i8: 1,
	align_of_i16: 2,
	align_of_i32: 4,
	align_of_i64: 8,
	align_of_isize: 4,

	align_of_ptr: 4,

	align_of_u8: 1,
	align_of_u16: 2,
	align_of_u32: 4,
	align_of_u64: 8,
	align_of_usize: 4,

	global_ptr_reloc: Reloc::Raw {
		reloc: ArmReloc::Abs32 as u32,
		addend: 0,
	},

	// TODO: What if isize is smaller than i32
	isize_max: i32::MAX as isize,
	isize_min: i32::MIN as isize,

	size_of_isize: core::mem::size_of::<i32>(),
	size_of_ptr: core::mem::size_of::<u32>(),
	size_of_usize: core::mem::size_of::<u32>(),

	// TODO: What if isize is smaller than i32
	usize_max: u32::MAX as usize,
	usize_min: u32::MIN as usize,
};

////////////////////////////////////////////////////////////////////////////////
// Type Definitions
////////////////////////////////////////////////////////////////////////////////

pub type GbaEndian = LittleEndian;
pub type TargetTriple = target_lexicon::Triple;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

pub struct TargetSpec<'a, B: ByteOrder> {
	pub triple: TargetTriple,
	pub abi: &'a ReprAbi,
	pub _byteorder: PhantomData<B>,
}

////////////////////////////////////////////////////////////////////////////////
// Enums
////////////////////////////////////////////////////////////////////////////////

// https://www.uclibc.org/docs/psABI-arm.pdf
#[repr(u32)]
pub enum ArmReloc {
	None = 0, // Used for dependencies between sections. Doesn't reloc.
	Abs32 = 2,
}
