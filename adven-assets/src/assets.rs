// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

mod image;
mod sprite;
mod tile;

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::display::PaletteAsset;

use crate::core::Asset;
use crate::ExportableAsset;

////////////////////////////////////////////////////////////////////////////////
// Re-Exports
////////////////////////////////////////////////////////////////////////////////

pub use self::image::*;
pub use self::sprite::*;
pub use self::tile::*;

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Asset for PaletteAsset<16> {
	fn as_any(&self) -> &dyn std::any::Any {
		self
	}

	fn as_exportable_asset(&self) -> Option<&dyn crate::core::ExportableAsset> {
		Some(self)
	}
}


impl Asset for PaletteAsset<256> {
	fn as_any(&self) -> &dyn std::any::Any {
		self
	}

	fn as_exportable_asset(&self) -> Option<&dyn crate::core::ExportableAsset> {
		Some(self)
	}
}

impl ExportableAsset for PaletteAsset<16> {
	fn type_name(&self) -> &'static str {
		"adven::display::PaletteAsset<16>"
	}

	fn as_repr_writable(&self) -> &dyn repr_writer::ReprWritable {
		self
	}
}

impl ExportableAsset for PaletteAsset<256> {
	fn type_name(&self) -> &'static str {
		"adven::display::PaletteAsset<256>"
	}

	fn as_repr_writable(&self) -> &dyn repr_writer::ReprWritable {
		self
	}
}
