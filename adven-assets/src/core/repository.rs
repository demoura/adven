// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::{collections::HashMap, ops::Deref};

use anyhow::{anyhow, bail};

use crate::{Asset, AssetId, ExportableAsset, RawAssetId};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

/// Responsible for storing all assets.
#[derive(Default)]
pub struct AssetRepository(HashMap<RawAssetId, Box<dyn Asset>>);

/// View of an `AssetRepository` containing a subset of the assets.
pub struct AssetRepositoryView<'a>(HashMap<RawAssetId, &'a dyn Asset>);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl AssetRepository {
	/// Creates a new `asset` with id `asset_id`. Returns an error if
	/// the asset already exists.
	pub fn create(&mut self, asset_id: RawAssetId, asset: Box<dyn Asset>) -> anyhow::Result<()> {
		if self.0.contains_key(&asset_id) {
			bail!(
				"Failure to create asset [{}], asset with the same id already exists.",
				asset_id
			);
		}

		self.0.insert(asset_id, asset);

		Ok(())
	}

	/// Checks if an asset of any type with the given `asset_id` exists.
	pub fn exists(&self, asset_id: RawAssetId) -> bool {
		self.0.contains_key(&asset_id)
	}

	/// An iterator visiting the exportable assets.
	pub fn exportables(&self) -> impl Iterator<Item = (RawAssetId, &dyn ExportableAsset)> + '_ {
		self.iter().filter_map(|(asset_id, asset)| {
			asset
				.as_exportable_asset()
				.map(|exportable_asset| (asset_id, exportable_asset))
		})
	}

	// Gets an existing asset with the specified `asset_id`.
	// Returns an error if the asset does not exist.
	pub fn get_dyn(&self, asset_id: RawAssetId) -> anyhow::Result<&dyn Asset> {
		let asset: &dyn Asset = self.0.get(&asset_id).map(Box::as_ref).ok_or(anyhow!(
			"Failure to get asset [{}]. No asset with this id exists.",
			asset_id
		))?;

		Ok(asset)
	}

	/// An iterator visiting all assets.
	pub fn iter(&self) -> impl Iterator<Item = (RawAssetId, &dyn Asset)> + '_ {
		self.0
			.iter()
			.map(|(&asset_id, asset)| (asset_id, asset.as_ref()))
	}

	// Creates a view containing the assets with the given `asset_ids`.
	// Returns an error if any of the assets does not exist.
	pub fn view<'a>(&'a self, asset_ids: &[RawAssetId]) -> anyhow::Result<AssetRepositoryView<'a>> {
		let mut view_assets = HashMap::<RawAssetId, &'a dyn Asset>::new();

		for &asset_id in asset_ids {
			let asset: &'a dyn Asset = self.get_dyn(asset_id)?;
			view_assets.insert(asset_id, asset);
		}

		Ok(AssetRepositoryView(view_assets))
	}
}

impl<'a> AssetRepositoryView<'a> {
	// Gets an existing asset with the specified `asset_id` and type `T`.
	// Returns an error if the asset does not exist or has the wrong type.
	pub fn get<T: Asset>(&self, asset_id: AssetId<T>) -> anyhow::Result<&T> {
		let asset: &dyn Asset = self
			.0
			.get(&asset_id.into())
			.map(Deref::deref)
			.ok_or(anyhow!(
				"Failure to get asset [{}]. No asset with this id exists.",
				asset_id
			))?;

		let asset: &T = asset.as_any().downcast_ref().ok_or(anyhow!(
			"Failure to get asset [{}]. Wrong asset type.",
			asset_id
		))?;

		Ok(asset)
	}
}
