// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use crate::{Asset, AssetRepositoryView, RawAssetId};

////////////////////////////////////////////////////////////////////////////////
// Traits
////////////////////////////////////////////////////////////////////////////////

#[typetag::deserialize]
pub trait AssetProcessor {
	/// The ids of the assets this processor requires as input.
	fn inputs(&self) -> Vec<RawAssetId>;

	/// The ids of the assets this processor produces.
	fn outputs(&self) -> Vec<RawAssetId>;

	/// Process the `input` assets and returns the new generate assets.
	fn process(
		&self,
		inputs: &AssetRepositoryView<'_>,
	) -> anyhow::Result<Vec<(RawAssetId, Box<dyn Asset>)>>;
}
