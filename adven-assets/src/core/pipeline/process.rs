// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::{collections::HashMap, fs::File};

use anyhow::anyhow;
use dep_graph::{DepGraph, Node};
use ron::extensions::Extensions;

use crate::{
	core::{repository::AssetRepository, AssetProcessors},
	AssetProcessor, RawAssetId,
};

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn process(assets: &mut AssetRepository, processors: &AssetProcessors) -> anyhow::Result<()> {
	let ordered_processors = order_processors(assets, processors)?;

	for processor in ordered_processors {
		let inputs = assets.view(&processor.inputs())?;

		for (new_asset_id, new_asset) in processor.process(&inputs)? {
			log::info!("New asset from processing: {}", new_asset_id);
			assets.create(new_asset_id, new_asset)?;
		}
	}

	Ok(())
}

pub fn load_processors(processor_paths: Vec<String>) -> anyhow::Result<AssetProcessors> {
	let mut processors = AssetProcessors::new();

	for processor_path in &processor_paths {
		let processor = ron::Options::default()
			.with_default_extension(Extensions::IMPLICIT_SOME)
			.with_default_extension(Extensions::UNWRAP_NEWTYPES)
			.with_default_extension(Extensions::UNWRAP_VARIANT_NEWTYPES)
			.from_reader(File::open(processor_path)?)?;

		log::info!("Loading processor: {}", processor_path);

		processors.push(processor);
	}

	Ok(processors)
}

pub fn order_processors<'a>(
	assets: &AssetRepository,
	processors: &'a AssetProcessors,
) -> anyhow::Result<Vec<&'a dyn AssetProcessor>> {
	let processor_index_by_output_id: HashMap<RawAssetId, usize> =
		map_outputs_to_processor_index(processors);

	let mut nodes = Vec::<Node<usize>>::new();
	for (i, processor) in processors.iter().enumerate() {
		let mut node = Node::new(i);

		for input_id in processor.inputs() {
			if assets.exists(input_id) {
				continue;
			}

			let &source_processor_index = processor_index_by_output_id.get(&input_id).ok_or(
				anyhow!("Failed to find processor that outputs: {}", input_id),
			)?;

			node.add_dep(source_processor_index);
		}

		nodes.push(node);
	}

	let ordered_processors: Vec<&dyn AssetProcessor> = DepGraph::new(&nodes)
		.into_iter()
		.map(|processor_index| processors[processor_index].as_ref())
		.collect();

	Ok(ordered_processors)
}

fn map_outputs_to_processor_index(processors: &AssetProcessors) -> HashMap<RawAssetId, usize> {
	let mut processor_index_by_output_id = HashMap::<RawAssetId, usize>::new();

	for (i, processor) in processors.iter().enumerate() {
		for output_id in processor.outputs() {
			processor_index_by_output_id.insert(output_id, i);
		}
	}

	processor_index_by_output_id
}
