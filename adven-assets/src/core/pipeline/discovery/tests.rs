// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::path::PathBuf;

use super::find_assets;

fn assets_directory() -> PathBuf {
	let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
	path.push("src/finder/tests/resources");
	path
}

#[test]
pub fn should_return_asset_in_root_folder() {
	let assets: Vec<PathBuf> =
		find_assets(assets_directory()).expect("Should find assets without error");

	println!("{:?}", assets);

	assert!(assets.into_iter().any(|asset| match asset.file_name() {
		Some(filename) => filename == "asset1.txt",
		None => false,
	}));
}

#[test]
pub fn should_return_asset_in_sub_folder() {
	let assets: Vec<PathBuf> =
		find_assets(assets_directory()).expect("Should find assets without error");

	println!("{:?}", assets);

	assert!(assets.into_iter().any(|asset| match asset.file_name() {
		Some(filename) => filename == "asset2.txt",
		None => false,
	}));
}

#[test]
pub fn should_return_asset_in_sub_sub_folder() {
	let assets: Vec<PathBuf> =
		find_assets(assets_directory()).expect("Should find assets without error");

	println!("{:?}", assets);

	assert!(assets.into_iter().any(|asset| match asset.file_name() {
		Some(filename) => filename == "asset3.md",
		None => false,
	}));
}

#[test]
pub fn should_not_return_metadata_file() {
	let assets: Vec<PathBuf> =
		find_assets(assets_directory()).expect("Should find assets without error");

	println!("{:?}", assets);

	assert!(assets.into_iter().all(|asset| match asset.file_name() {
		Some(filename) => filename != "asset1.txt.meta",
		None => false,
	}));
}
