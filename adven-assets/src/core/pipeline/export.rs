// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::{fs::File, io::Write, path::Path};

use anyhow::anyhow;
use repr_writer::{Artifact, ByteOrder, faerie::Decl, ArtifactWriter};

use crate::{abi::TargetSpec, core::repository::AssetRepository, ExportableAsset, RawAssetId};

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn export_asset_artifact<B: ByteOrder>(
	assets: &AssetRepository,
	target: &TargetSpec<'_, B>,
	artifact_filename: String,
	artifact_path: &Path,
) -> anyhow::Result<()> {
	let mut artifact = Artifact::new(target.triple.clone(), artifact_filename);

	for (asset_id, asset) in assets.exportables() {
		log::info!("Exporting asset {} to artifact declaration.", asset_id);
		write_artifact_declaration(asset_id, asset, &mut artifact, target)?;
	}

	for (asset_id, asset) in assets.exportables() {
		log::info!("Exporting asset {} to artifact definition.", asset_id);
		write_artifact_definition(asset_id, asset, &mut artifact, target)?;
	}

	let artifact_file = File::create(artifact_path)?;
	artifact.write(artifact_file)?;

	Ok(())
}

pub fn export_asset_library(
	output_directory: &Path,
	artifact_path: &Path,
	library_name: &str,
) -> anyhow::Result<()> {
	cc::Build::new()
		.out_dir(output_directory)
		.object(artifact_path)
		.try_compile(library_name)?;

	Ok(())
}

pub fn export_asset_declarations(
	assets: &AssetRepository,
	declarations_path: &Path,
) -> anyhow::Result<()> {
	let mut declarations = File::create(declarations_path)?;

	writeln!(declarations, "extern \"C\" {{")?;
	for (asset_id, asset) in assets.exportables() {
		log::info!("Exporting asset {} to declarations.", asset_id);
		write_rust_declaration(asset_id, asset, &mut declarations)?;
	}
	writeln!(declarations, "}}")?;

	Ok(())
}

fn write_rust_declaration(
	asset_id: RawAssetId,
	asset: &dyn ExportableAsset,
	declarations: &mut File,
) -> anyhow::Result<()> {
	writeln!(
		declarations,
		"\tstatic {}: {};",
		asset_id.to_symbol_name(),
		asset.type_name()
	)?;

	Ok(())
}

fn write_artifact_declaration<B: ByteOrder>(
	asset_id: RawAssetId,
	asset: &dyn ExportableAsset,
	artifact: &mut Artifact,
	target: &TargetSpec<'_, B>,
) -> anyhow::Result<()> {

	let align = u64::try_from(asset.align_dyn(target.abi))?;

	artifact
		.declare(asset_id.to_symbol_name(), Decl::data().global().with_align(Some(align)))?;

	Ok(())
}

fn write_artifact_definition<B: ByteOrder>(
	asset_id: RawAssetId,
	asset: &dyn ExportableAsset,
	artifact: &mut Artifact,
	target: &TargetSpec<'_, B>,
) -> anyhow::Result<()> {
	let symbol_name = asset_id.to_symbol_name();

	let mut writer = ArtifactWriter::<B>::new(artifact, target.abi, &symbol_name);

	asset.write(&mut writer)
		.map_err(|e| anyhow!("Failed to write asset: {}", e))?;

	let output = writer.get_output();

	artifact.define(symbol_name, output)?;

	Ok(())
}
