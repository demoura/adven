// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests;

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::{collections::HashMap, path::Path};

use anyhow::anyhow;
use walkdir::WalkDir;

////////////////////////////////////////////////////////////////////////////////
// Enums
////////////////////////////////////////////////////////////////////////////////

#[derive(Debug)]
pub enum DiscoveredAssets {
	New {
		asset_path: String,
	},
	Existing {
		asset_path: String,
		metadata_path: String,
	},
	Missing {
		metadata_path: String,
	},
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl DiscoveredAssets {
	pub fn from_asset_path(asset_path: String) -> Self {
		DiscoveredAssets::New { asset_path }
	}

	pub fn from_metadata_path(metadata_path: String) -> Self {
		DiscoveredAssets::Missing { metadata_path }
	}

	pub fn with_asset_path(self, asset_path: String) -> Self {
		match self {
			DiscoveredAssets::New { .. } => DiscoveredAssets::New { asset_path },
			DiscoveredAssets::Existing { metadata_path, .. } => DiscoveredAssets::Existing {
				asset_path,
				metadata_path,
			},
			DiscoveredAssets::Missing { metadata_path } => DiscoveredAssets::Existing {
				asset_path,
				metadata_path,
			},
		}
	}

	pub fn with_metadata_path(self, metadata_path: String) -> Self {
		match self {
			DiscoveredAssets::New { asset_path } => DiscoveredAssets::Existing {
				asset_path,
				metadata_path,
			},
			DiscoveredAssets::Existing { asset_path, .. } => DiscoveredAssets::Existing {
				asset_path,
				metadata_path,
			},
			DiscoveredAssets::Missing { metadata_path } => {
				DiscoveredAssets::Missing { metadata_path }
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn find_assets<P: AsRef<Path>>(assets_directory: P) -> anyhow::Result<Vec<DiscoveredAssets>> {
	let mut assets = HashMap::<String, DiscoveredAssets>::new();

	for entry in WalkDir::new(assets_directory) {
		let entry = entry?;

		log::debug!("Found entry: {:?}", entry);

		if !entry.file_type().is_file() {
			log::debug!("Ignored entry: Not a file");
			continue;
		}

		let path = entry
			.into_path()
			.into_os_string()
			.into_string()
			.map_err(|path| anyhow!("Found non UTF-8 path: {:?}", path))?;

		if path.ends_with(".processor") {
			continue;
		}

		if let Some(key) = path.strip_suffix(".meta") {
			let import_paths = match assets.remove(key) {
				Some(import_paths) => import_paths.with_metadata_path(path.clone()),
				None => DiscoveredAssets::from_metadata_path(path.clone()),
			};

			assets.insert(key.to_owned(), import_paths);
		} else {
			let import_paths = match assets.remove(&path) {
				Some(import_paths) => import_paths.with_asset_path(path.clone()),
				None => DiscoveredAssets::from_asset_path(path.clone()),
			};

			assets.insert(path, import_paths);
		}
	}

	assets
		.values()
		.for_each(|asset| log::debug!("Found: {:?}", asset));

	Ok(assets.into_values().collect())
}

pub fn find_processors<P: AsRef<Path>>(processors_directory: P) -> anyhow::Result<Vec<String>> {
	let mut processors = Vec::<String>::new();

	for entry in WalkDir::new(processors_directory) {
		let entry = entry?;

		log::debug!("Found entry: {:?}", entry);

		if !entry.file_type().is_file() {
			log::debug!("Ignored entry: Not a file");
			continue;
		}

		let path = entry
			.into_path()
			.into_os_string()
			.into_string()
			.map_err(|path| anyhow!("Found non UTF-8 path: {:?}", path))?;

		if path.ends_with(".processor") {
			processors.push(path);
		}
	}

	processors
		.iter()
		.for_each(|processor| log::debug!("Found processor: {:?}", processor));

	Ok(processors)
}
