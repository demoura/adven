// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::fs::File;

use ron::{extensions::Extensions, ser::PrettyConfig};

use super::discovery::DiscoveredAssets;
use crate::core::repository::AssetRepository;
use crate::core::AssetImporter;

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

pub fn import_assets(asset_paths: Vec<DiscoveredAssets>) -> anyhow::Result<AssetRepository> {
	let mut assets = AssetRepository::default();

	for asset_path in asset_paths {
		let (asset_path, mut importer) = match &asset_path {
			DiscoveredAssets::New { asset_path } => {
				log::info!("Importing new asset [{}].", asset_path);
				continue;
			}
			DiscoveredAssets::Existing {
				asset_path,
				metadata_path,
			} => {
				log::info!("Importing metadata [{}] for existing asset.", metadata_path);
				let metadata = load_importer_metadata(metadata_path)?;

				log::info!("Importing existing asset [{}].", asset_path);
				(asset_path.as_str(), metadata)
			}
			DiscoveredAssets::Missing { metadata_path } => {
				log::warn!("Found metadata [{}] for missing asset.", metadata_path);
				continue;
			}
		};

		let mut asset_file = File::open(asset_path)?;
		let imported_assets = importer.import(&mut asset_file)?;

		save_importer_metadata(&(asset_path.to_owned() + ".meta"), importer.as_ref())?;

		for (asset_id, asset) in imported_assets {
			assets.create(asset_id, asset)?;
		}
	}

	Ok(assets)
}

fn load_importer_metadata(metadata_path: &str) -> anyhow::Result<Box<dyn AssetImporter>> {
	let mut file = File::open(metadata_path)?;

	let importer = ron::Options::default()
		.with_default_extension(Extensions::IMPLICIT_SOME)
		.with_default_extension(Extensions::UNWRAP_NEWTYPES)
		.with_default_extension(Extensions::UNWRAP_VARIANT_NEWTYPES)
		.from_reader(&mut file)?;

	Ok(importer)
}

fn save_importer_metadata(metadata_path: &str, metadata: &dyn AssetImporter) -> anyhow::Result<()> {
	let mut file = File::create(metadata_path)?;

	ron::Options::default()
		.with_default_extension(Extensions::IMPLICIT_SOME)
		.with_default_extension(Extensions::UNWRAP_NEWTYPES)
		.with_default_extension(Extensions::UNWRAP_VARIANT_NEWTYPES)
		.to_writer_pretty(&mut file, metadata, PrettyConfig::default())?;

	Ok(())
}
