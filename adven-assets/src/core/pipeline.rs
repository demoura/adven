// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Modules
////////////////////////////////////////////////////////////////////////////////

mod discovery;
mod export;
mod import;
mod process;

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::path::PathBuf;

use repr_writer::ByteOrder;

use self::discovery::DiscoveredAssets;
use crate::core::repository::AssetRepository;
use crate::{abi::TargetSpec, AssetProcessors};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

pub struct AssetPipeline<'a, B: ByteOrder> {
	input_directory: PathBuf,
	output_directory: PathBuf,
	target: TargetSpec<'a, B>,
	artifact_filename: String,
	declarations_filename: String,
	library_name: String,
}

impl<'a, B: ByteOrder> AssetPipeline<'a, B> {
	pub fn new(
		input_directory: PathBuf,
		output_directory: PathBuf,
		target: TargetSpec<'a, B>,
	) -> Self {
		Self {
			input_directory,
			output_directory,
			target,
			artifact_filename: "assets.o".to_owned(),
			declarations_filename: "assets.rs".to_owned(),
			library_name: "assets".to_owned(),
		}
	}

	pub fn with_artifact_filename(mut self, artifact_filename: String) -> Self {
		self.artifact_filename = artifact_filename;
		self
	}

	pub fn with_declarations_filename(mut self, declarations_filename: String) -> Self {
		self.declarations_filename = declarations_filename;
		self
	}

	pub fn with_library_name(mut self, library_name: String) -> Self {
		self.library_name = library_name;
		self
	}

	pub fn build(self) -> anyhow::Result<()> {
		let asset_paths: Vec<DiscoveredAssets> = discovery::find_assets(&self.input_directory)?;
		let processor_paths: Vec<String> = discovery::find_processors(&self.input_directory)?;

		let mut assets: AssetRepository = import::import_assets(asset_paths)?;
		let processors: AssetProcessors = process::load_processors(processor_paths)?;

		process::process(&mut assets, &processors)?;

		let declarations_path = {
			let mut path = self.output_directory.clone();
			path.push(self.declarations_filename);
			path
		};
		export::export_asset_declarations(&assets, declarations_path.as_path())?;

		let artifact_path = {
			let mut path = self.output_directory.clone();
			path.push(&self.artifact_filename);
			path
		};
		export::export_asset_artifact(
			&assets,
			&self.target,
			self.artifact_filename.clone(),
			artifact_path.as_path(),
		)?;

		export::export_asset_library(
			self.output_directory.as_path(),
			&artifact_path,
			&self.library_name,
		)?;

		println!(
			"cargo:rerun-if-changed={}",
			&self.input_directory.to_string_lossy()
		);
		println!("cargo:rustc-link-lib=static={}", &self.library_name);

		Ok(())
	}
}
