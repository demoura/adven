// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::fmt::{Debug, Display};
use std::{any::Any, marker::PhantomData};

use repr_writer::ReprWritable;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

////////////////////////////////////////////////////////////////////////////////
// Traits
////////////////////////////////////////////////////////////////////////////////

pub trait Asset: Any {
	fn as_any(&self) -> &dyn Any;
	fn as_exportable_asset(&self) -> Option<&dyn ExportableAsset>;
}

pub trait ExportableAsset: Asset + ReprWritable {
	fn type_name(&self) -> &'static str;
	fn as_repr_writable(&self) -> &dyn ReprWritable;
}

#[typetag::deserialize]
pub trait SerdeAsset: Asset {
	fn into_asset(self: Box<Self>) -> Box<dyn Asset>;
}

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[repr(transparent)]
#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct RawAssetId(Uuid);

#[repr(transparent)]
#[derive(Deserialize, Serialize)]
pub struct AssetId<T: Asset>(RawAssetId, #[serde(skip)] PhantomData<T>);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl RawAssetId {
	pub fn generate() -> Self {
		RawAssetId(Uuid::new_v4())
	}

	pub(crate) fn to_symbol_name(self) -> String {
		let uuid_fields = self.0.as_fields();

		format!(
			"ADV_ASSETS_{:X}_{:X}_{:X}_{:X}",
			uuid_fields.0,
			uuid_fields.1,
			uuid_fields.2,
			u64::from_be_bytes(*uuid_fields.3)
		)
	}
}

impl Display for RawAssetId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		std::fmt::Display::fmt(&self.0, f)
	}
}

impl<T: Asset> From<AssetId<T>> for RawAssetId {
	fn from(value: AssetId<T>) -> Self {
		value.0
	}
}

impl ReprWritable for RawAssetId {
	fn align(abi: &repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		abi.align_of_ptr
	}

	fn align_dyn(&self, abi: &repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn repr_writer::ReprWriter) -> repr_writer::Result<()> {
		writer.write_reference(&self.to_symbol_name())
	}
}

impl<T: Asset> AssetId<T> {
	pub fn generate() -> Self {
		Self(RawAssetId::generate(), PhantomData::default())
	}
}

impl<T: Asset> Clone for AssetId<T> {
	fn clone(&self) -> Self {
		Self(self.0, self.1)
	}
}

impl<T: Asset> Copy for AssetId<T> {}

impl<T: Asset> Debug for AssetId<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_tuple("AssetId").field(&self.0).finish()
	}
}

impl<T: Asset> Display for AssetId<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		std::fmt::Display::fmt(&self.0, f)
	}
}

impl<T: Asset> Eq for AssetId<T> {}

impl<T: Asset> PartialEq for AssetId<T> {
	fn eq(&self, other: &Self) -> bool {
		self.0.eq(&other.0)
	}
}

impl<T: Asset> ReprWritable for AssetId<T> {
	fn align(abi: &repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		RawAssetId::align(abi)
	}

	fn align_dyn(&self, abi: &repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn repr_writer::ReprWriter) -> repr_writer::Result<()> {
		self.0.write(writer)
	}
}
