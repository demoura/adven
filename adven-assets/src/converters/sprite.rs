// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven::display::{SpriteShapeSize, Tile, Tile4bpp, Tile8bpp};

use super::tile::{convert_to_tile4bpp, convert_to_tile8bpp, TileConversionError};
use crate::assets::{ImageIndexed4bpp, ImageIndexed8bpp};

#[derive(thiserror::Error, Debug)]
pub enum SpriteConversionError {
	#[error("Image for SpriteImport frames must be the size of SpriteImport")]
	InvalidSpriteSize,

	#[error("Failed to convert tiles, due to: {0}")]
	TileConversionError(#[from] TileConversionError),
}

fn convert_frame4bpp(
	frame: &ImageIndexed4bpp,
	output: &mut Vec<Tile4bpp>,
) -> Result<(), SpriteConversionError> {
	let tiles_x = frame.width() as usize / Tile4bpp::WIDTH;
	let tiles_y = frame.height() as usize / Tile4bpp::HEIGHT;

	for ty in 0..tiles_y {
		for tx in 0..tiles_x {
			let tile = convert_to_tile4bpp(frame, tx * Tile4bpp::WIDTH, ty * Tile4bpp::HEIGHT)?;

			output.push(tile);
		}
	}

	Ok(())
}

fn convert_frame8bpp(
	frame: &ImageIndexed8bpp,
	output: &mut Vec<Tile8bpp>,
) -> Result<(), SpriteConversionError> {
	let tiles_x = frame.width() as usize / Tile8bpp::WIDTH;
	let tiles_y = frame.height() as usize / Tile8bpp::HEIGHT;

	for ty in 0..tiles_y {
		for tx in 0..tiles_x {
			let tile = convert_to_tile8bpp(frame, tx * Tile8bpp::WIDTH, ty * Tile8bpp::HEIGHT)?;

			output.push(tile);
		}
	}

	Ok(())
}

pub fn convert_frames4bpp(
	shape_size: SpriteShapeSize,
	frames: Vec<ImageIndexed4bpp>,
) -> Result<Vec<Tile4bpp>, SpriteConversionError> {
	let mut sprite_data = vec![];

	for frame in frames {
		if frame.width() != shape_size.width() as u32
			|| frame.height() != shape_size.height() as u32
		{
			return Err(SpriteConversionError::InvalidSpriteSize);
		}

		convert_frame4bpp(&frame, &mut sprite_data)?;
	}

	Ok(sprite_data)
}

pub fn convert_frames8bpp(
	shape_size: SpriteShapeSize,
	frames: Vec<ImageIndexed8bpp>,
) -> Result<Vec<Tile8bpp>, SpriteConversionError> {
	let mut sprite_data = vec![];

	for frame in frames {
		if frame.width() != shape_size.width() as u32
			|| frame.height() != shape_size.height() as u32
		{
			return Err(SpriteConversionError::InvalidSpriteSize);
		}

		convert_frame8bpp(&frame, &mut sprite_data)?;
	}

	Ok(sprite_data)
}
