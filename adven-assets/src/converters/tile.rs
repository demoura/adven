// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven::{
	display::{Tile, Tile4bpp, Tile8bpp},
	math::Vector2,
};
use thiserror::Error;

use crate::assets::{ImageIndexed4bpp, ImageIndexed8bpp};

#[derive(Error, Debug)]
pub enum TileConversionError {
	#[error("Offset X is out of bounds")]
	OffsetXOutOfBounds,
	#[error("Offset Y is out of bounds")]
	OffsetYOutOfBounds,
}

pub fn convert_to_tile4bpp(
	image: &ImageIndexed4bpp,
	offset_x: usize,
	offset_y: usize,
) -> Result<Tile4bpp, TileConversionError> {
	if offset_x + Tile4bpp::WIDTH > image.width() as usize {
		return Err(TileConversionError::OffsetXOutOfBounds);
	}

	if offset_y + Tile4bpp::HEIGHT > image.height() as usize {
		return Err(TileConversionError::OffsetYOutOfBounds);
	}

	let mut tile = Tile4bpp::default();

	for x in 0..Tile4bpp::WIDTH {
		for y in 0..Tile4bpp::HEIGHT {
			let pixel = image[Vector2 {
				x: x + offset_x,
				y: y + offset_y,
			}];

			tile.set_pixel(Vector2 { x, y }, pixel.into())
		}
	}

	Ok(tile)
}

pub fn convert_to_tile8bpp(
	image: &ImageIndexed8bpp,
	offset_x: usize,
	offset_y: usize,
) -> Result<Tile8bpp, TileConversionError> {
	if offset_x + Tile8bpp::WIDTH > image.width() as usize {
		return Err(TileConversionError::OffsetXOutOfBounds);
	}

	if offset_y + Tile8bpp::HEIGHT > image.height() as usize {
		return Err(TileConversionError::OffsetYOutOfBounds);
	}

	let mut tile = Tile8bpp::default();

	for x in 0..Tile4bpp::WIDTH {
		for y in 0..Tile4bpp::HEIGHT {
			let pixel = image[Vector2 {
				x: x + offset_x,
				y: y + offset_y,
			}];

			tile.set_pixel(Vector2 { x, y }, pixel.into())
		}
	}

	Ok(tile)
}
