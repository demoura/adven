// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use ron::extensions::Extensions;
use serde::{Deserialize, Serialize};

use crate::{
	core::{AssetImporter, RawAssetId, SerdeAsset},
	Asset,
};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Deserialize, Serialize)]
pub struct RonImporter {
	asset_id: RawAssetId,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

#[typetag::serde(name = "adven::RonImporter")]
impl AssetImporter for RonImporter {
	fn import(
		&mut self,
		input: &mut dyn std::io::Read,
	) -> anyhow::Result<Vec<(RawAssetId, Box<dyn Asset>)>> {
		let asset: Box<dyn SerdeAsset> = ron::Options::default()
			.with_default_extension(Extensions::IMPLICIT_SOME)
			.with_default_extension(Extensions::UNWRAP_NEWTYPES)
			.with_default_extension(Extensions::UNWRAP_VARIANT_NEWTYPES)
			.from_reader(input)?;

		Ok(vec![(self.asset_id, asset.into_asset())])
	}
}

impl Default for RonImporter {
	fn default() -> Self {
		Self {
			asset_id: RawAssetId::generate(),
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
// Tests
////////////////////////////////////////////////////////////////////////////////
#[cfg(test)]
mod tests {
	use serde::{Deserialize, Serialize};

	use super::RonImporter;
	use crate::core::{Asset, AssetImporter, RawAssetId, SerdeAsset};

	#[derive(Asset, Deserialize, Serialize)]
	struct TestAsset {
		id: RawAssetId,
		text: String,
	}

	#[typetag::deserialize]
	impl SerdeAsset for TestAsset {
		fn into_asset(self: Box<Self>) -> Box<dyn Asset> {
			self
		}
	}

	#[test]
	fn should_deserialize_asset() {
		let asset_ron = r###"
		{
			"TestAsset": (
				id: "f9199032-0cb0-468b-9f88-3c7c86a0f155",
				text: "Hello"
			)
		}"###;

		let mut importer = RonImporter::default();

		let imported_assets = importer.import(&mut asset_ron.as_bytes()).unwrap();

		assert_eq!(1, imported_assets.len());
	}
}
