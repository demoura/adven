// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

pub use image::ImageFormat;
use image::*;
use serde::{Deserialize, Serialize};
use std::{convert::TryInto, fmt::Debug, io::BufReader};

use crate::{assets::Image, core::AssetImporter};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum ImgPaletteType {
	/// Used for non-paletted images, and importing paletted
	/// images as non-paletted.
	None,
	/// Extract palette from paletted image.
	Extract,
	/// Generate palette from non-paletted image.
	/// Colors are added to palette in the order they appear.
	Generate,
}

#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum ImgImportType {
	Image { palette_type: ImgPaletteType },
	Palette,
}

impl Default for ImgImportType {
	fn default() -> Self {
		ImgImportType::Image {
			palette_type: ImgPaletteType::None,
		}
	}
}


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ImgImportOptionsV2 {
	import_type: ImgImportType,
	image_pipeline: Option<AssetUuid>,
	palette_pipeline: Option<AssetUuid>,
}


#[derive(Clone, Debug, Deserialize, Serialize, TypeUuid)]
#[uuid = "295f5e70-3a1a-496b-83e6-700774d6e5a2"]
pub enum ImgImportOptions {
	V1 { import_type: ImgImportType },
	V2(ImgImportOptionsV2),
}

impl ImgImportOptions {

	pub fn updated(&self) -> ImgImportOptionsV2 {
		match self {
			Self::V1 { import_type } => ImgImportOptionsV2 {
				import_type: *import_type,
				image_pipeline: None,
				palette_pipeline: None
			},
			Self::V2(options) => options.clone(),
		}
	}
}

impl Default for ImgImportOptions {
	fn default() -> Self {
		ImgImportOptions::V2(ImgImportOptionsV2 {
			import_type: Default::default(),
			image_pipeline: None,
			palette_pipeline: None
		})
	}
}


#[derive(Debug, Deserialize, Serialize, TypeUuid)]
#[uuid = "aa46e23a-a449-4535-84f6-4cebb7381b22"]
pub enum ImgImportState {
	V1 {
		image_id: Option<AssetUuid>,
		palette_id: Option<AssetUuid>,
	},
}

impl Default for ImgImportState {
	fn default() -> Self {
		ImgImportState::V1 {
			image_id: None,
			palette_id: None,
		}
	}
}

impl ImgImportState {
	fn image_id_or_gen(&mut self) -> AssetUuid {
		match self {
			ImgImportState::V1 { image_id, .. } => match image_id {
				Some(id) => *id,
				None => {
					let uuid = AssetUuid(*Uuid::new_v4().as_bytes());
					*image_id = Some(uuid);
					uuid
				}
			},
		}
	}

	fn palette_id_or_gen(&mut self) -> AssetUuid {
		match self {
			ImgImportState::V1 { palette_id, .. } => match palette_id {
				Some(id) => *id,
				None => {
					let uuid = AssetUuid(*Uuid::new_v4().as_bytes());
					*palette_id = Some(uuid);
					uuid
				}
			},
		}
	}
}


#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ImageImporter {
	img_format: ImageFormat,
}

impl ImageImporter {
	pub fn new(format: ImageFormat) -> Self {
		Self { img_format: format }
	}

	fn import_image(
		&self,
		state: &mut ImgImportState,
		img_bytes: &[u8],
		palette_type: ImgPaletteType,
		options: ImgImportOptionsV2,
	) -> ImportResult<ImporterValue> {
		log::info!("Import");
		match palette_type {
			ImgPaletteType::None => {

				let dyn_image = image::load_from_memory_with_format(img_bytes, self.img_format)
					.map_err(|e| ImportError::Boxed(Box::new(e)))?;

				let image: Image = dyn_image.into();

				Ok(ImporterValue {
					assets: vec![ImportedAsset {
						id: state.image_id_or_gen(),
						search_tags: vec![],
						build_deps: vec![],
						load_deps: vec![],
						build_pipeline: options.image_pipeline,
						asset_data: Box::new(image),
					}],
				})
			}
			ImgPaletteType::Extract => {
				unimplemented!()
			}
			ImgPaletteType::Generate => {
				log::info!("Generate");
				let dyn_image = image::load_from_memory_with_format(img_bytes, self.img_format)
					.map_err(|e| ImportError::Boxed(Box::new(e)))?;

				let (image, palette) = match &dyn_image {
					DynamicImage::ImageRgb8(img) => {
						let mut palette: Vec<u8> = Vec::new();
						let image = self.generate_palette(img, &mut palette)?;

						(image, Palette::PaletteRgb8(palette))
					}
					DynamicImage::ImageRgba8(img) => {
						log::info!("Rgba");
						let mut palette: Vec<u8> = Vec::new();
						let image = self.generate_palette(img, &mut palette)?;

						(image, Palette::PaletteRgba8(palette))
					}
					_ => unimplemented!(),
				};

				Ok(ImporterValue {
					assets: vec![
						ImportedAsset {
							id: state.image_id_or_gen(),
							search_tags: vec![],
							build_deps: vec![],
							load_deps: vec![],
							build_pipeline: options.image_pipeline,
							asset_data: Box::new(image),
						},
						ImportedAsset {
							id: state.palette_id_or_gen(),
							search_tags: vec![],
							build_deps: vec![],
							load_deps: vec![],
							build_pipeline: options.palette_pipeline,
							asset_data: Box::new(palette),
						},
					],
				})
			}
		}
	}

	fn generate_palette<V>(
		&self,
		img: &V,
		palette: &mut Vec<<V::Pixel as Pixel>::Subpixel>,
	) -> ImportResult<Image>
	where
		V: GenericImageView,
		V::Pixel: Eq,
	{
		let mut data = Vec::new();

		for (_x, _y, pixel) in img.pixels() {
			let mut index = None;
			{
				let mut i = 0;
				while index.is_none() && i < palette.len() {
					let channels = pixel.channels();
					let mut is_equal = true;
					for j in 0..channels.len() {
						if palette[i + j] != channels[j] {
							is_equal = false;
							break;
						}
					}

					if is_equal {
						index = Some(i / channels.len());
					} else {
						i += channels.len();
					}
				}
			}

			let index = match index {
				Some(index) => index,
				None => {
					for channel in pixel.channels() {
						palette.push(*channel);
					}
					(palette.len() - 1) / pixel.channels().len()
				}
			};

			data.push(
				index
					.try_into()
					.map_err(|e| ImportError::Boxed(Box::new(e)))?,
			);
		}

		Ok(Image::Indexed8(ImageData {
			width: img.width(),
			height: img.height(),
			data,
		}))
	}
}

#[typetag::deserialize(name = "adven::ImageImporter")]
impl AssetImporter for ImageImporter {

	fn import(&mut self, input: &mut dyn std::io::Read) -> anyhow::Result<crate::core::Assets> {

		let dyn_image = image::io::Reader::with_format(BufReader::new(input), self.img_format)
			.decode()?;

	}
}
