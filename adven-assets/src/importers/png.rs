// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use adven::{
	color::BGR5,
	display::{Palette4bppIndex, Palette8bppIndex, PaletteAsset},
};
use serde::{Deserialize, Serialize};

use crate::{
	assets::{ImageIndexed4bpp, ImageIndexed8bpp},
	Asset, AssetImporter, RawAssetId,
};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PngImporter {
	image_id: RawAssetId,
	palette_id: Option<RawAssetId>,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl PngImporter {
	fn palette_id_or_new(&mut self) -> RawAssetId {
		match self.palette_id {
			Some(palette_id) => palette_id,
			None => {
				let palette_id = RawAssetId::generate();
				self.palette_id = Some(palette_id);
				palette_id
			}
		}
	}
}

#[typetag::serde(name = "adven::PngImporter")]
impl AssetImporter for PngImporter {
	fn import(
		&mut self,
		input: &mut dyn std::io::Read,
	) -> anyhow::Result<Vec<(RawAssetId, Box<dyn Asset>)>> {
		let mut assets = Vec::new();

		let decoder = png::Decoder::new(input);
		let mut reader = decoder.read_info()?;

		if let Some(palette) = import_palette(reader.info())? {
			assets.push((self.palette_id_or_new(), palette));
		}

		let mut buffer = vec![0u8; reader.output_buffer_size()];
		reader.next_frame(&mut buffer)?;

		let asset = match reader.info().color_type {
			png::ColorType::Indexed => import_indexed(reader.info(), &buffer),
			png::ColorType::Grayscale => todo!(),
			png::ColorType::Rgb => todo!(),
			png::ColorType::GrayscaleAlpha => todo!(),
			png::ColorType::Rgba => todo!(),
		}?;

		assets.push((self.image_id, asset));

		Ok(assets)
	}
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////

fn import_indexed(png_info: &png::Info, buffer: &[u8]) -> anyhow::Result<Box<dyn Asset>> {
	match png_info.bit_depth {
		// 16 color or smaller palettes
		png::BitDepth::One | png::BitDepth::Two | png::BitDepth::Four => {
			let mut data = Vec::<Palette4bppIndex>::new();
			for index in buffer {
				data.push(Palette4bppIndex::try_from(*index).unwrap());
			}

			let image = ImageIndexed4bpp::new(png_info.width, png_info.height, data)?;

			Ok(Box::new(image))
		}
		// 256 color palettes
		png::BitDepth::Eight | png::BitDepth::Sixteen => {
			let mut data = Vec::<Palette8bppIndex>::new();
			for index in buffer {
				data.push(Palette8bppIndex::from(*index));
			}

			let image = ImageIndexed8bpp::new(png_info.width, png_info.height, data)?;

			Ok(Box::new(image))
		}
	}
}

fn import_palette(png_info: &png::Info) -> anyhow::Result<Option<Box<dyn Asset>>> {
	let palette = match &png_info.palette {
		Some(palette) => palette,
		None => return Ok(None),
	};

	let palette: Vec<BGR5> = palette
		.chunks_exact(3)
		.map(|rgb| BGR5::from_rgb888(rgb[0], rgb[1], rgb[2]))
		.collect();

	match png_info.bit_depth {
		// 16 color or smaller palettes
		png::BitDepth::One | png::BitDepth::Two | png::BitDepth::Four => {
			let mut palette16 = PaletteAsset::<16>([BGR5::BLACK; 16]);
			palette16.0[0..palette.len()].copy_from_slice(&palette);
			Ok(Some(Box::new(palette16)))
		}
		// 256 color palettes
		png::BitDepth::Eight | png::BitDepth::Sixteen => {
			let mut palette256 = PaletteAsset::<256>([BGR5::BLACK; 256]);
			palette256.0[0..palette.len()].copy_from_slice(&palette);
			Ok(Some(Box::new(palette256)))
		}
	}
}
