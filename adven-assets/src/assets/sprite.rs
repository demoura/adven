// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use adven::display::{SpriteShapeSize, TilesRef};
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};
use serde::Deserialize;

use super::tile::Tiles;
use crate::{core::Asset, ExportableAsset};

#[derive(Deserialize, Debug)]
pub struct Sprite {
	pub shape_size: SpriteShapeSize,
	pub frame_count: u16,
	pub frames: Tiles,
}

impl Asset for Sprite {
	fn as_any(&self) -> &dyn std::any::Any {
		self
	}

	fn as_exportable_asset(&self) -> Option<&dyn ExportableAsset> {
		Some(self)
	}
}

impl ExportableAsset for Sprite {
	fn type_name(&self) -> &'static str {
		"adven::display::SpriteAsset<'static>"
	}

	fn as_repr_writable(&self) -> &dyn adven::repr_writer::ReprWritable {
		self
	}
}

impl ReprWritable for Sprite {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[
			SpriteShapeSize::align(abi),
			u16::align(abi),
			TilesRef::<'_>::align(abi),
		]
		.iter()
		.max()
		.unwrap()
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;
		seq.write_element(&self.shape_size)?;
		seq.write_element(&self.frame_count)?;
		seq.write_element(&self.frames)?;
		seq.end()
	}
}
