// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use std::{fmt::Debug, ops::Index};

use adven::{
	color::BGR5,
	display::{Palette4bppIndex, Palette8bppIndex},
	math::Vector2,
};
use thiserror::Error;

use crate::core::Asset;

////////////////////////////////////////////////////////////////////////////////
// Type Definitions
////////////////////////////////////////////////////////////////////////////////

/// Indexed image using a 4-bit per pixel palette.
pub type ImageIndexed4bpp = Image<Palette4bppIndex>;

/// Indexed image using a 8-bit per pixel palette.
pub type ImageIndexed8bpp = Image<Palette8bppIndex>;

/// Bitmap image using 5-bit per pixel color in BGR order.
pub type BitmapImage = Image<BGR5>;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

/// Simplified Image type.
#[derive(Asset, Clone, Debug)]
pub struct Image<T: Clone + Debug + 'static> {
	/// Image data (row major)
	data: Vec<T>,
	/// Image width (pixels per row / column count)
	width: u32,
}

////////////////////////////////////////////////////////////////////////////////
// Enums
////////////////////////////////////////////////////////////////////////////////

#[derive(Error, Debug)]
pub enum ImageCreationError {
	#[error(
		"Image size mismatch. Size [{expected_len} bytes] calculated from width [{width}],
		and height [{height}], differs from actual size [{actual_len} bytes]."
	)]
	ImageSizeMismatch {
		width: u32,
		height: u32,
		expected_len: u32,
		actual_len: usize,
	},
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl<T: Clone + Debug> Image<T> {
	pub fn new(width: u32, height: u32, data: Vec<T>) -> Result<Self, ImageCreationError> {
		let expected_len = width * height;
		if expected_len as usize != data.len() {
			return Err(ImageCreationError::ImageSizeMismatch {
				width,
				height,
				expected_len,
				actual_len: data.len(),
			});
		}

		Ok(Self { data, width })
	}

	/// Image height (row count)
	pub fn height(&self) -> u32 {
		self.data.len() as u32 / self.width
	}

	/// Image width (pixels per row / column count)
	pub fn width(&self) -> u32 {
		self.width
	}
}

impl<T: Clone + Debug> Index<Vector2<usize>> for Image<T> {
	type Output = T;

	fn index(&self, index: Vector2<usize>) -> &Self::Output {
		&self.data[index.y * self.width as usize + index.x]
	}
}
