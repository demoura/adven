// adven-assets <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::borrow::Borrow;

use adven::{
	display::{Tile4bpp, Tile8bpp},
	SliceRef,
};
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};
use serde::Deserialize;

use crate::core::{exportable_asset, ExportableAsset};

#[derive(Clone, Debug, Deserialize)]
#[exportable_asset]
pub enum Tiles {
	Color4Bit(Vec<Tile4bpp>),
	Color8Bit(Vec<Tile8bpp>),
}

impl ExportableAsset for Tiles {
	fn type_name(&self) -> &'static str {
		"adven::display::TilesRef<'static>"
	}

	fn as_repr_writable(&self) -> &dyn ReprWritable {
		self
	}
}

impl ReprWritable for Tiles {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[
			// Enum tag is part of the struct with repr(u16)
			abi.align_of_u16,
			SliceRef::<'static, Tile4bpp>::align(abi),
			SliceRef::<'static, Tile8bpp>::align(abi),
		]
		.iter()
		.max()
		.unwrap()
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;

		match self {
			Tiles::Color4Bit(tiles) => {
				seq.write_element(&0u16)?;
				seq.write_element(&SliceRef::from(tiles.borrow()))?;
			}
			Tiles::Color8Bit(tiles) => {
				seq.write_element(&1u16)?;
				seq.write_element(&SliceRef::from(tiles.borrow()))?;
			}
		}

		seq.end()
	}
}
