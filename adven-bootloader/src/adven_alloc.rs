// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use alloc::alloc::GlobalAlloc;
use core::{
	alloc::{Allocator, Layout},
	ptr::NonNull,
	slice,
};

use adven::sync::Mutex;
use adven_allocators::{BuddyAlloc, RawAlloc};

#[global_allocator]
pub static GLOBAL_ALLOC: AdvenAlloc = AdvenAlloc(Mutex::new(None));

extern "C" {
	static mut __eheap_start: u8;
	static mut __eheap_end: u8;
}

pub struct AdvenAlloc(Mutex<Option<BuddyAlloc<RawAlloc<'static>>>>);

impl AdvenAlloc {
	pub fn init(&self) {
		let heap_start = unsafe { (&mut __eheap_start) as *mut u8 };
		let heap_end = unsafe { (&mut __eheap_end) as *mut u8 };
		let heap_size =
			adven_math::previous_power_of_two((heap_end as usize) - (heap_start as usize));

		#[cfg(feature = "log-mgba")]
		log::trace!(
			"Initializing heap with start at {:#?}, end at {:#?} and usable size {}",
			heap_start,
			heap_end,
			heap_size
		);

		let heap_slice = unsafe { slice::from_raw_parts_mut::<'static>(heap_start, heap_size) };
		let heap_raw = RawAlloc::new(heap_slice);
		let layout = unsafe { Layout::from_size_align_unchecked(heap_size, 1) };

		#[cfg(feature = "log-mgba")]
		log::trace!("Initializing buddy allocator with layout {:#?}", layout);

		let heap_buddy = BuddyAlloc::new_self_contained(layout, 64, heap_raw).unwrap();

		#[cfg(feature = "log-mgba")]
		log::trace!("Buddy allocator initialized");

		let mut lock = self
			.0
			.try_lock()
			.expect("Failed to lock mutex to set global allocator");

		*lock = Some(heap_buddy);
	}
}

unsafe impl GlobalAlloc for AdvenAlloc {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		// #[cfg(feature = "log-mgba")]
		// log::trace!("Global alloc called with layout: {:?}", layout);

		if let Some(mutex_guard) = self.0.try_lock() {
			let alloc_ref = mutex_guard
				.as_ref()
				.expect("Global allocator should be initialized");

			if let Ok(memory_slice) = alloc_ref.allocate(layout) {
				return memory_slice.as_mut_ptr();
			}
		} else {
			#[cfg(feature = "log-mgba")]
			log::warn!("alloc: Failed to lock global allocator");
		}
		#[cfg(feature = "log-mgba")]
		log::warn!("Global allocation failure.");

		core::ptr::null_mut()
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		// log::trace!("Global dealloc called for {:p} with {:?}", ptr, layout);
		if let Some(ptr) = NonNull::new(ptr) {
			if let Some(mut mutex_guard) = self.0.try_lock() {
				let alloc_ref = mutex_guard
					.as_mut()
					.expect("Global allocator should be initialized");

				alloc_ref.deallocate(ptr, layout);
			} else {
				#[cfg(feature = "log-mgba")]
				log::warn!("dealloc: Failed to lock global allocator");
			}
		}
	}
}
