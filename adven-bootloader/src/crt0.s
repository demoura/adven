@ adven-bootloader <https://gitlab.com/demoura/adven>
@
@ This Source Code Form is subject to the terms of the Mozilla Public
@ License, v. 2.0. If a copy of the MPL was not distributed with this
@ file, You can obtain one at https://mozilla.org/MPL/2.0/.

@ File modified from the original at:
@ https://github.com/rust-console/gba/blob/09a7c7300464da6c9ce700c2c5e7ca0f0ec9c86b/crt0.s
@ rust-console/gba is licensed under apache2, which can be found at:
@ ./third-party-licenses/rust-console-gba-LICENSE-APACHE

    .arm
    .align 4, 0
    .global __start
    .type __start STT_FUNC
__start:
    b .Linit

    @ ROM header. This gets filled with the
    @ required data after running gbafix.
    .fill 156,1,0

    @ game title Can be set by gbafix.
    .fill 12,1,0

    @ game code. Can be set by gbafix.
    .byte 0x00,0x00,0x00,0x00

    @ maker code. Can be set by gbafix.
    .byte 0x00,0x00

    .byte 0x96

    @ main unit code
    .byte 0x00

    @ device type (0x00 retail, 0x80 debug)
    .byte 0x00

    @ reserved
    .byte 0x00,0x00,0x00,0x00,0x00,0x00,0x00

    @ software version. Can be set by gbafix.
    .byte 0x00

    @ complement check. Will be set by gbafix.
    .byte 0x00

    @ reserved area
    .space 2

.Linit:
    @ Set address of user IRQ handler
    ldr r0, =MainIrqHandler
    ldr r1, =0x03FFFFFC
    str r0, [r1]

    @ The stack pointer are already inited by the BIOS.
    @ https://problemkaputt.de/gbatek.htm#gbainterruptcontrol
    @ I will leave the code here because it's very cool.

    @ This switches to IRQ mode then sets it's stack pointer.
    @ https://problemkaputt.de/gbatek.htm#armcpuflagsconditionfieldcond
    @ set IRQ stack pointer
    @mov r0, #0x12           @ IRQ mode flags
    @msr CPSR_c, r0          @ Set CPSR_control flags
    @ldr sp, =0x3007fa0      @ Move stack pointer.

    @ Same as above but for user stack.
    @ set user stack pointer
    @mov r0, #0x1f           @ System mode. No not the BIOS's supervisor mode. 
    @msr CPSR_c, r0          
    @ldr sp, =0x3007f00      

    @ copy .data section to IWRAM
    ldr r0, =__data_lma     @ source address
    ldr r1, =__data_start   @ destination address
    ldr r2, =__data_end
    subs r2, r1             @ length
    @ these instructions are only executed if r2 is nonzero
    @ (i.e. don't bother copying an empty .data section)
    addne r2, #3
    asrne r2, #2
    addne r2, #0x04000000
    swine 0xb0000

    @ Call a thumb function, because rust doesn't seem to
    @ properly return to an arm function.
    ldr r0, =CallUserCode
    bx r0

    .thumb_func
    .align 4, 0
    .global CallUserCode
    .type CallUserCode STT_FUNC
CallUserCode:
    @ call adven-bootloader boot()
    @ Initializes the logger and global heap allocator.
    bl __adven_bootloader_boot

    @ jump to user code
    bl main

    .arm
    .align 4, 0
    .global MainIrqHandler
    .type MainIrqHandler STT_FUNC
MainIrqHandler:
    @ Load base I/O register address
    mov r2, #0x04000000
    add r2, r2, #0x200

    @ Save IRQ stack pointer and IME
    mrs r0, spsr
    ldrh r1, [r2, #8]
    stmdb sp!, {r0-r2,lr}

    @ Disable all interrupts by writing to IME
    @ r2 (0x4000200) can be used as we only care about bit 0 being unset
    strh r2, [r2, #8]

    @ Acknowledge all received interrupts that were enabled in IE
    ldr r3, [r2, #0]
    and r0, r3, r3, lsr #16
    strh r0, [r2, #2]

    @ Switch from IRQ mode to system mode
    @ cpsr_c = 0b000_10010u8 | 0b000_01101u8
    mrs r2, cpsr
    orr r2, r2, #0xD
    msr cpsr_c, r2

    @ Jump to user specified IRQ handler
    ldr r2, =__IRQ_HANDLER
    ldr r1, [r2]
    stmdb sp!, {lr}
    adr lr, .Lreturn
    bx r1
.Lreturn:
    ldmia sp!, {lr}

    @ Switch from ??? mode to IRQ mode, disable IRQ
    @ cpsr_c = ( !0b000_01101u8 & cpsr_c ) | 0b100_10010u8
    mrs r2, cpsr
    bic r2, r2, #0xD
    orr r2, r2, #0x92
    msr cpsr_c, r2

    @ Restore IRQ stack pointer and IME
    ldmia sp!, {r0-r2,lr}
    strh r1, [r2, #8]
    msr spsr_cf, r0

    @ Return to BIOS IRQ handler
    bx lr
    .pool

@ vim:ft=armv4
