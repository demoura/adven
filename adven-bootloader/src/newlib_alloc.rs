// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use alloc::alloc::GlobalAlloc;
use core::alloc::Layout;

struct NewlibAlloc;

unsafe impl GlobalAlloc for NewlibAlloc {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		libc::malloc(layout.size()).cast()
	}

	unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
		libc::free(ptr.cast())
	}
}

#[global_allocator]
static GLOBAL_ALLOC: NewlibAlloc = NewlibAlloc;
