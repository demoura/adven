@ adven-bootloader <https://gitlab.com/demoura/adven>
@
@ This Source Code Form is subject to the terms of the Mozilla Public
@ License, v. 2.0. If a copy of the MPL was not distributed with this
@ file, You can obtain one at https://mozilla.org/MPL/2.0/.

    .arm
    .align 4, 0
    .global __sync_val_compare_and_swap_8
    .type __sync_val_compare_and_swap_8 STT_FUNC
@ Swaps new_val into *ptr if cmp_val == *ptr.
@ r0: ptr: *mut u64.
@ r1: padding
@ r2: cmp_val0: u64 low.
@ r3: cmp_val1: u64 high.
@ returns: Value of *ptr (before swap).
__sync_val_compare_and_swap_8:
    ptr .req r0
    cmp_val0 .req r2
    cmp_val1 .req r3

    push { r4, r5, r6, r7 }

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Disable interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ime_addr .req r4
    @ Load IME address.
    mov ime_addr, #0x04000000
    add ime_addr, ime_addr, #0x208

    ime_val .req r5
    mov ime_val, #0                     @ false
    swp ime_val, ime_val, [ime_addr]    @ Disable interrupts, and keep last value. 

    mov r12, ptr
    .unreq ptr
    ptr .req r12

    ldm ptr, {r0, r1}
    cmp r0, r2          @ Compare cmp_val0 and *ptr
    cmpeq r1, r3        @ If equal, compare cmp_val1 and *ptr

    new_val0 .req r6
    new_val1 .req r7

    ldreq new_val0, [sp, #16]
    ldreq new_val1, [sp, #20]
    stmeq ptr, { new_val0, new_val1 } @ If equal, move new_val into *ptr

    pop { r4, r5, r6, r7 }

    bx lr

    .unreq ptr
    .unreq cmp_val0
    .unreq cmp_val1
    .unreq new_val0
    .unreq new_val1
    .unreq ime_addr
    .unreq ime_val

    .arm
    .align 4, 0
    .global __sync_lock_test_and_set_8
    .type __sync_lock_test_and_set_8 STT_FUNC
@ r0: ptr: *mut u64.
@ r1: padding
@ r2: val0: u64 low.
@ r3: val1: u64 low.
@ returns: new value
__sync_lock_test_and_set_8:
    ptr .req r0
    val0 .req r2
    val1 .req r3

    push { r4, r5 }

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Disable interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ime_addr .req r4
    @ Load IME address.
    mov ime_addr, #0x04000000
    add ime_addr, ime_addr, #0x208

    ime_val .req r5
    mov ime_val, #0                     @ false
    swp ime_val, ime_val, [ime_addr]    @ Disable interrupts, and keep last value. 

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Swap values
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    mov r12, ptr
    .unreq ptr
    ptr .req r12

    ldm ptr, {r0, r1}   @ Load r0, and r1 starting from ptr
    stm ptr, {r2, r3}

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Restore Interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    str ime_val, [ime_addr] @ Restore old ime value

    pop { r4, r5 }

    bx lr

    .unreq ptr
    .unreq val0
    .unreq val1
    .unreq ime_addr
    .unreq ime_val

    .arm
    .align 4, 0
    .global __sync_fetch_and_add_8
    .type __sync_fetch_and_add_8 STT_FUNC
@ r0: ptr: *mut u64.
@ r1: padding
@ r2: val0: u64 low
@ r3: val1: u64 high
@ returns: *ptr + val
__sync_fetch_and_add_8:
    ptr .req r0
    val0 .req r2
    val1 .req r3

    push { r4, r5 }     @ Save to stack

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Disable interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ime_addr .req r4
    @ Load IME address.
    mov ime_addr, #0x04000000
    add ime_addr, ime_addr, #0x208

    ime_val .req r5
    mov ime_val, #0                     @ false
    swp ime_val, ime_val, [ime_addr]    @ Disable interrupts, and keep last value. 

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Add doubleword
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    mov r12, ptr
    .unreq ptr
    ptr .req r12

    ldm ptr, {r0, r1}   @ Load r0, and r1 starting from ptr
    adds r0, r0, val0   @ Add low word
    adc r1, r1, val1    @ Add high word with carry
    stm ptr, {r0, r1}   @ Store r0, r1 into ptr

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Restore Interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    str ime_val, [ime_addr] @ Restore old ime value

    pop { r4, r5 }          @ Pop saved from stack

    bx lr

    .unreq ptr
    .unreq val0
    .unreq val1
    .unreq ime_addr
    .unreq ime_val

    .arm
    .align 4, 0
    .global __sync_fetch_and_sub_8
    .type __sync_fetch_and_sub_8 STT_FUNC
@ r0: ptr: *mut u64.
@ r1: padding
@ r2: val0: u64 low
@ r3: val1: u64 high
@ returns: *ptr - val
__sync_fetch_and_sub_8:
    ptr .req r0
    val0 .req r2
    val1 .req r3

    push { r4, r5 }     @ Save to stack

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Disable interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    ime_addr .req r4
    @ Load IME address.
    mov ime_addr, #0x04000000
    add ime_addr, ime_addr, #0x208

    ime_val .req r5
    mov ime_val, #0                     @ false
    swp ime_val, ime_val, [ime_addr]    @ Disable interrupts, and keep last value. 

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Add doubleword
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    mov r12, ptr
    .unreq ptr
    ptr .req r12

    ldm ptr, {r0, r1}   @ Load r0, and r1 starting from ptr
    subs r0, r0, val0   @ Subtract low word
    sbc r1, r1, val1    @ Subtract high word with carry
    stm ptr, {r0, r1}   @ Store r0, r1 into ptr

    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    @ Restore Interrupts
    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    str ime_val, [ime_addr] @ Restore old ime value

    pop { r4, r5 }          @ Pop saved from stack

    bx lr

    .unreq ptr
    .unreq val0
    .unreq val1
    .unreq ime_addr
    .unreq ime_val

@ vim:ft=armv4
