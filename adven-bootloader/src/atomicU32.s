@ adven-bootloader <https://gitlab.com/demoura/adven>
@
@ This Source Code Form is subject to the terms of the Mozilla Public
@ License, v. 2.0. If a copy of the MPL was not distributed with this
@ file, You can obtain one at https://mozilla.org/MPL/2.0/.

    .arm
    .align 4, 0
    .global __sync_fetch_and_add_4
    .type __sync_fetch_and_add_4 STT_FUNC
@ r0: ptr: *mut u32.
@ r1: add_val: u32.
@ returns: *ptr + add_val
__sync_fetch_and_add_4:
    @ Load IME address.
    mov r3, #0x04000000
    add r3, r3, #0x208

    mov r2, #0          @ false
    swp r2, r2, [r3]    @ Disable interrupts, and keep last value. 

    ldr r12, [r0]       @ Load *ptr into r12
    add r12, r12, r1    @ r12, *ptr + add_val
    str r12, [r0]       @ If equal, move new_val into *ptr
    
    swp r2, r2, [r3]    @ Restore interrupts.

    mov r0, r12         @ Return *ptr + add_val.

    bx lr

    .arm
    .align 4, 0
    .global __sync_fetch_and_sub_4
    .type __sync_fetch_and_sub_4 STT_FUNC
@ r0: ptr: *mut u32.
@ r1: sub_val: u32.
@ returns: *ptr - sub_val
__sync_fetch_and_sub_4:
    @ Load IME address.
    mov r3, #0x04000000
    add r3, r3, #0x208

    mov r2, #0          @ false
    swp r2, r2, [r3]    @ Disable interrupts, and keep last value. 

    ldr r12, [r0]       @ Load *ptr into r12
    subs r12, r12, r1   @ r12, *ptr - sub_val
    str r12, [r0]       @ If equal, move new_val into *ptr
    
    swp r2, r2, [r3]    @ Restore interrupts.

    mov r0, r12         @ Return *ptr - sub_val.

    bx lr


    .arm
    .align 4, 0
    .global __sync_lock_test_and_set_4
    .type __sync_lock_test_and_set_4 STT_FUNC
@ r0: ptr: *mut u32.
@ r1: new_val: u32.
@ returns: new value
__sync_lock_test_and_set_4:
    swp r1, r1, [r0]    @ Swap new_val and *ptr 
    mov r0, r1          @ Set new_val as return.

    bx lr

    .arm
    .align 4, 0
    .global __sync_val_compare_and_swap_4
    .type __sync_val_compare_and_swap_4 STT_FUNC
@ Swaps new_val into *ptr if cmp_val == *ptr.
@ r0: ptr: *mut u32.
@ r1: cmp_val: u32.
@ r2: new_val: u32.
@ returns: Value of *ptr (before swap).
__sync_val_compare_and_swap_4:
    @ Save non-scrath register to the stack.
    push {r4}

    @ Load IME address.
    mov r3, #0x04000000
    add r3, r3, #0x208

    mov r4, #0          @ false
    swp r4, r4, [r3]    @ Disable interrupts, and keep last value. 

    ldr r12, [r0]       @ Load *ptr into r12
    cmp r1, r12         @ Compare old_val and *ptr
    streq r2, [r0]      @ If equal, move new_val into *ptr
    
    swp r4, r4, [r3]    @ Restore interrupts.

    mov r0, r12         @ Return *ptr (before swap).
    pop {r4}            @ Restore registers.

    bx lr

@ vim:ft=armv4
