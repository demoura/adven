// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! A little crate to define some boilerplate needed to code for the GBA, such as:
//! - A linker script.
//! - The crt0 code that runs on boot.
//! - Definining and initializing a heap allocator (with feature `newlib` or `adven-alloc`).
//! - Initializing a logging backend (Currently only mGBA emulator is supported).
//! - Defining panic and alloc error handlers.

#![no_std]
#![cfg_attr(
	feature = "adven-alloc",
	feature(allocator_api),
	feature(slice_ptr_get)
)]
#![cfg_attr(
	any(feature = "adven-alloc", feature = "newlib"),
	feature(alloc_error_handler)
)]

#[cfg(any(feature = "adven-alloc", feature = "newlib"))]
extern crate alloc;
#[cfg(feature = "newlib")]
extern crate libc;

#[cfg(feature = "adven-alloc")]
mod adven_alloc;
#[cfg(feature = "newlib")]
mod newlib_alloc;

#[no_mangle]
extern "C" fn __adven_bootloader_boot() {
	#[cfg(feature = "log-mgba")]
	let _ = mgba_logger::init();
	#[cfg(feature = "enable-logging")]
	log::info!("Booting up!");

	#[cfg(feature = "adven-alloc")]
	{
		adven_alloc::GLOBAL_ALLOC.init();

		#[cfg(feature = "enable-logging")]
		log::info!("HeapAlloctor up!");
	}
}

#[no_mangle]
static __IRQ_HANDLER: extern "C" fn() = irq_handler;

extern "C" fn irq_handler() {}

#[panic_handler]
pub fn panic(_info: &core::panic::PanicInfo) -> ! {
	#[cfg(feature = "log-mgba")]
	{
		use core::fmt::Write;

		let mut writer = mgba_logger::MgbaWriteAdapter::new(mgba_logger::LogLevel::Fatal);
		let _ = write!(writer, "Panic: {}", _info);
	} // Let the writer get dropped before we infinite loop.

	// TODO: Instead of looping, go to sleep.
	loop {}
}

#[cfg(any(feature = "adven-alloc", feature = "newlib"))]
#[alloc_error_handler]
pub fn handle_alloc_error(layout: core::alloc::Layout) -> ! {
	#[cfg(feature = "log-mgba")]
	{
		use core::fmt::Write;

		let mut writer = mgba_logger::MgbaWriteAdapter::new(mgba_logger::LogLevel::Fatal);
		let _ = write!(
			writer,
			"Failed to allocate layout with size {} and align {}",
			layout.size(),
			layout.align()
		);
	} // Let the writer get dropped before we infinite loop.

	// TODO: Instead of looping, go to sleep.
	loop {}
}
