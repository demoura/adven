@ adven-bootloader <https://gitlab.com/demoura/adven>
@
@ This Source Code Form is subject to the terms of the Mozilla Public
@ License, v. 2.0. If a copy of the MPL was not distributed with this
@ file, You can obtain one at https://mozilla.org/MPL/2.0/.

    .arm
    .align 4, 0
    .global __sync_lock_test_and_set_1
    .type __sync_lock_test_and_set_1 STT_FUNC
@ r0: ptr: *mut u8.
@ r1: new_val: u8.
@ returns: new value
__sync_lock_test_and_set_1:
    swpb r1, r1, [r0]   @ Swap new_val and *ptr 
    mov r0, r1          @ Set new_val as return.

    bx lr


    .thumb_func         @ No need for cool instructions here.
    .align 4, 0
    .global __sync_synchronize
    .type __sync_synchronize STT_FUNC
@ This should issue a full memory barrier.
@ But there's no need for/nor a way to that.
@ So it's a nop.
__sync_synchronize:
    bx lr

    .arm
    .align 4, 0
    .global __sync_val_compare_and_swap_1
    .type __sync_val_compare_and_swap_1 STT_FUNC
@ Swaps new_val into *ptr if cmp_val == *ptr.
@ r0: ptr: *mut u8.
@ r1: cmp_val: u8.
@ r2: new_val: u8.
@ returns: Value of *ptr (before swap).
__sync_val_compare_and_swap_1:
    @ Save non-scrath register to the stack.
    push {r4}

    @ Load IME address.
    mov r3, #0x04000000
    add r3, r3, #0x208

    mov r4, #0          @ false
    swp r4, r4, [r3]    @ Disable interrupts, and keep last value. 

    ldrb r12, [r0]      @ Load *ptr into r12
    cmp r1, r12         @ Compare old_val and *ptr
    streqb r2, [r0]     @ If equal, move new_val into *ptr
    
    swp r4, r4, [r3]    @ Restore interrupts.

    mov r0, r12         @ Return *ptr (before swap).
    pop {r4}            @ Restore registers.

    bx lr

@ vim:ft=armv4
