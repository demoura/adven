// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate adven_bootloader;

mod atomicU64;

#[no_mangle]
extern "C" fn main() -> ! {
	test_main();

	loop {}
}

// https://os.phil-opp.com/testing/
trait Testable {
	fn run(&self);
}

impl<T> Testable for T
where
	T: Fn(),
{
	fn run(&self) {
		log::info!("{}...\t", core::any::type_name::<T>());
		self();
		log::info!("[ok]");
	}
}

fn test_runner(tests: &[&dyn Testable]) {
	log::info!("Test runner launched, with {} tests", tests.len());

	for test in tests {
		test.run();
	}
}
