// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

extern "C" {
	fn __sync_val_compare_and_swap_8(ptr: *mut u64, cmp_val: u64, new_val: u64) -> u64;
	fn __sync_lock_test_and_set_8(ptr: *mut u64, val: u64) -> u64;
	fn __sync_fetch_and_add_8(ptr: *mut u64, val: u64) -> u64;
	fn __sync_fetch_and_sub_8(ptr: *mut u64, val: u64) -> u64;
}

#[test_case]
fn should_compare_and_swap() {
	let mut a: u64 = u64::MAX;

	let out = unsafe { __sync_val_compare_and_swap_8(&mut a, u64::MAX, 20) };

	assert_eq!(20, a, "a must contain new value");
	assert_eq!(u64::MAX, out, "should return old value");

	let mut a: u64 = u64::MAX;

	let out = unsafe { __sync_val_compare_and_swap_8(&mut a, u32::MAX.into(), 20) };

	assert_eq!(u64::MAX, a, "a must contain old value");
	assert_eq!(u64::MAX, out, "should return old value");
}

#[test_case]
fn should_test_and_set() {
	let mut a: u64 = u64::MAX;

	let out = unsafe { __sync_lock_test_and_set_8(&mut a, 20) };

	assert_eq!(20, a, "should set value");
	assert_eq!(u64::MAX, out, "should return old value");
}

#[test_case]
fn should_add() {
	let mut a: u64 = 10;

	let out = unsafe { __sync_fetch_and_add_8(&mut a, 20) };

	assert_eq!(30, a, "a must be 30");
	assert_eq!(a, out, "a must equal out");

	a = u32::MAX.into();

	let out = unsafe { __sync_fetch_and_add_8(&mut a, 1) };

	assert_eq!(u64::from(u32::MAX) + 1u64, a, "*ptr must be correct");
	assert_eq!(a, out, "a must equal out");

	a = u32::MAX.into();

	let out = unsafe { __sync_fetch_and_add_8(&mut a, u32::MAX.into()) };

	assert_eq!(
		u64::from(u32::MAX) + u64::from(u32::MAX),
		a,
		"*ptr must be correct"
	);
	assert_eq!(a, out, "a must equal out");
}

#[test_case]
fn should_sub() {
	let mut a: u64 = 30;

	let out = unsafe { __sync_fetch_and_sub_8(&mut a, 10) };

	assert_eq!(20, a, "should subtract value");
	assert_eq!(a, out, "should return result");

	a = u64::from(u32::MAX) + 1;

	let out = unsafe { __sync_fetch_and_sub_8(&mut a, 1) };

	assert_eq!(u64::from(u32::MAX), a, "should subtract value");
	assert_eq!(a, out, "should return result");

	a = u32::MAX.into();

	let out = unsafe { __sync_fetch_and_sub_8(&mut a, u32::MAX.into()) };

	assert_eq!(0, a, "should subtract value");
	assert_eq!(a, out, "should return result");
}
