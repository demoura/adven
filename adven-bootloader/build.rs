// adven-bootloader <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

fn main() {
	let out_dir = &PathBuf::from(env::var("OUT_DIR").unwrap());

	// Automatically build crt0.s into libcrt0.a
	cc::Build::new()
		.compiler("arm-none-eabi-gcc")
		.archiver("arm-none-eabi-ar")
		.target("arm-none-eabi")
		.warnings_into_errors(true)
		.flag("-marm")
		.flag("-march=armv4t")
		.flag("-mcpu=arm7tdmi")
		.flag("-mthumb-interwork")
		.flag("-Wa,--fatal-warnings")
		.file("src/atomic.s")
		.file("src/atomicU32.s")
		.file("src/atomicU64.s")
		.file("src/crt0.s")
		.compile("crt0");

	// Copy linker script to the output directory,
	// so it will be on the linker's search path.
	// The user still needs to specify it by name, but at least it can be
	// shipped as a crate.
	{
		let linker_script = include_bytes!("src/adven-bootloader.ld");
		let mut out_file = File::create(out_dir.join("adven-bootloader.ld"))
			.expect("Failed copy linker file. Couldn't create file.");

		out_file
			.write_all(linker_script)
			.expect("Failed copy linker file. Failed to write to file.");
	}

	println!("cargo:rerun-if-changed=src/atomic.s");
	println!("cargo:rerun-if-changed=src/atomicU32.s");
	println!("cargo:rerun-if-changed=src/atomicU64.s");
	println!("cargo:rerun-if-changed=src/crt0.s");
	println!("cargo:rerun-if-changed=src/adven-bootloader.ld");
}
