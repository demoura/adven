// adven-ecs <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use hashbrown::HashMap;

use super::AnyStorage;
use crate::{Component, Entity, Storage};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

pub struct HashmapStorage<T: Component>(HashMap<Entity, T>);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl<T: Component> AnyStorage for HashmapStorage<T> {
	fn as_any(&self) -> &dyn core::any::Any {
		self
	}

	fn as_any_mut(&mut self) -> &mut dyn core::any::Any {
		self
	}

	fn contains_entity(&self, entity: Entity) -> bool {
		self.0.contains_key(&entity)
	}

	fn into_any(self: alloc::boxed::Box<Self>) -> alloc::boxed::Box<dyn core::any::Any> {
		self
	}

	fn remove(&mut self, entity: Entity) {
		Storage::remove(self, entity);
	}
}

impl<T: Component> Storage<T> for HashmapStorage<T> {
	fn get(&self, entity: Entity) -> Option<&T> {
		self.0.get(&entity)
	}

	fn get_mut(&mut self, entity: Entity) -> Option<&mut T> {
		self.0.get_mut(&entity)
	}

	fn insert(&mut self, entity: Entity, component: T) -> Option<T> {
		self.0.insert(entity, component)
	}

	fn remove(&mut self, entity: Entity) -> Option<T> {
		self.0.remove(&entity)
	}
}

impl<T: Component> Default for HashmapStorage<T> {
	fn default() -> Self {
		Self(Default::default())
	}
}
