// adven-ecs <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use alloc::boxed::Box;
use core::{
	any::{Any, TypeId},
	cell::{Ref, RefCell, RefMut},
};

use hashbrown::HashMap;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Default)]
pub struct Resources(HashMap<TypeId, RefCell<Box<dyn Any>>>);

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Resources {
	pub fn get<T: Any>(&self) -> Ref<'_, T> {
		self.try_get().unwrap()
	}

	pub fn get_mut<T: Any>(&self) -> RefMut<'_, T> {
		self.try_get_mut().unwrap()
	}

	pub fn insert<T: Any>(&mut self, resource: T) -> Option<T> {
		self.0
			.insert(TypeId::of::<T>(), RefCell::new(Box::new(resource)))
			.map(|old_resource| *old_resource.into_inner().downcast::<T>().unwrap())
	}

	pub fn remove<T: Any>(&mut self) -> Option<T> {
		self.0
			.remove(&TypeId::of::<T>())
			.map(|old_resource| *old_resource.into_inner().downcast::<T>().unwrap())
	}

	pub fn try_get<T: Any>(&self) -> Option<Ref<'_, T>> {
		self.0.get(&TypeId::of::<T>()).map(|resource| {
			Ref::map(resource.borrow(), |resource| {
				resource.downcast_ref().unwrap()
			})
		})
	}

	pub fn try_get_mut<T: Any>(&self) -> Option<RefMut<'_, T>> {
		self.0.get(&TypeId::of::<T>()).map(|resource| {
			RefMut::map(resource.borrow_mut(), |resource| {
				resource.downcast_mut().unwrap()
			})
		})
	}
}
