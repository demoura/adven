// adven-ecs <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Imports
////////////////////////////////////////////////////////////////////////////////

use alloc::vec::Vec;
use core::ops::IndexMut;

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Clone, Copy, Hash, Eq, PartialEq)]
pub struct Entity {
	index: u16,
	version: u16,
}

#[derive(Clone, Copy)]
struct EntityState(u16, u16);

#[derive(Default)]
pub struct EntityFactory {
	entities: Vec<EntityState>,
	amount_to_recycle: u16,
	front_deleted_index: u16,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl Entity {
	pub fn index(self) -> u16 {
		self.index
	}

	pub fn version(self) -> u16 {
		self.version
	}

	fn is_active(self, entity_state: EntityState) -> bool {
		self.index == entity_state.index() && self.version == entity_state.version()
	}
}

impl EntityState {
	fn active(entity: Entity) -> EntityState {
		Self(entity.index, entity.version)
	}

	fn deleted(entity: Entity) -> EntityState {
		Self(u16::MAX, entity.version)
	}

	fn index(self) -> u16 {
		self.0
	}

	fn last_version(self) -> u16 {
		self.1
	}

	fn next_deleted_index(self) -> u16 {
		self.0
	}

	fn set_next_deleted_index(&mut self, index: u16) {
		self.0 = index
	}

	fn version(self) -> u16 {
		self.1
	}
}

impl EntityFactory {
	pub fn active_entities(&self) -> impl Iterator<Item = Entity> + '_ {
		self.entities
			.iter()
			.enumerate()
			.filter(|&(i, entity_state)| i == entity_state.index() as usize)
			.map(|(_i, entity_state)| Entity {
				index: entity_state.index(),
				version: entity_state.version(),
			})
	}

	pub fn create(&mut self) -> Entity {
		if self.amount_to_recycle == 0 {
			let next_index =
				u16::try_from(self.entities.len()).expect("Active entity limit exceeded");

			let new_entity = Entity {
				index: next_index,
				version: 0,
			};

			self.entities.push(EntityState::active(new_entity));
			return new_entity;
		}

		let deleted_entity = self.entities[self.front_deleted_index as usize];

		let new_entity = Entity {
			index: self.front_deleted_index,
			version: deleted_entity.last_version() + 1,
		};

		self.entities[self.front_deleted_index as usize] = EntityState::active(new_entity);
		self.front_deleted_index = deleted_entity.next_deleted_index();
		self.amount_to_recycle -= 1;

		new_entity
	}

	pub fn delete(&mut self, entity: Entity) {
		if !self.is_active(entity) {
			return;
		}

		self.entities[entity.index() as usize] = EntityState::deleted(entity);

		let last_deleted_index = {
			let mut deleted_index = self.front_deleted_index;
			for _ in 1..self.amount_to_recycle {
				let deleted_entity = self.entities[deleted_index as usize];
				deleted_index = deleted_entity.next_deleted_index();
			}
			deleted_index
		};

		self.entities
			.index_mut(last_deleted_index as usize)
			.set_next_deleted_index(entity.index);
	}

	pub fn is_active(&self, entity: Entity) -> bool {
		match self.entities.get(entity.index() as usize) {
			Some(&entity_state) => entity.is_active(entity_state),
			None => false,
		}
	}
}
