// adven-ecs <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

////////////////////////////////////////////////////////////////////////////////
// Traits
////////////////////////////////////////////////////////////////////////////////

use alloc::boxed::Box;
use core::{
	any::TypeId,
	cell::{Ref, RefCell, RefMut},
};

use hashbrown::HashMap;

use crate::{storage::AnyStorage, Component, Entity, EntityFactory, Storage};

////////////////////////////////////////////////////////////////////////////////
// Structs
////////////////////////////////////////////////////////////////////////////////

#[derive(Default)]
pub struct World {
	entity_factory: EntityFactory,
	storages: HashMap<TypeId, RefCell<Box<dyn AnyStorage>>>,
}

#[derive(Debug)]
pub struct DeletedEntityError;

#[derive(Debug)]
pub enum InsertError {
	MissingStorage,
	DeletedEntity,
}

////////////////////////////////////////////////////////////////////////////////
// Implementations
////////////////////////////////////////////////////////////////////////////////

impl World {
	pub fn create_entity(&mut self) -> Entity {
		self.entity_factory.create()
	}

	pub fn delete_entity(&mut self, entity: Entity) {
		self.entity_factory.delete(entity);

		for storage in self.storages.values_mut() {
			storage.get_mut().remove(entity)
		}
	}

	pub fn get<T: Component>(&self, entity: Entity) -> Ref<'_, T> {
		self.try_get(entity).unwrap()
	}

	pub fn get_mut<T: Component>(&self, entity: Entity) -> RefMut<'_, T> {
		self.try_get_mut(entity).unwrap()
	}

	pub fn insert<T: Component>(
		&self,
		entity: Entity,
		component: T,
	) -> Result<Option<T>, InsertError> {
		if !self.entity_factory.is_active(entity) {
			return Err(InsertError::DeletedEntity);
		}

		self.try_get_storage_mut::<T>()
			.ok_or(InsertError::MissingStorage)
			.map(|mut storage| {
				storage
					.as_any_mut()
					.downcast_mut::<T::Storage>()
					.unwrap()
					.insert(entity, component)
			})
	}

	pub fn register<T: Component>(&mut self) {
		let _ = self
			.storages
			.try_insert(TypeId::of::<T>(), RefCell::<Box<T::Storage>>::default());
	}

	pub fn remove<T: Component>(&self, entity: Entity) -> Result<Option<T>, DeletedEntityError> {
		if !self.entity_factory.is_active(entity) {
			return Err(DeletedEntityError);
		}

		Ok(self.try_get_storage_mut::<T>().and_then(|mut storage| {
			let storage = storage.as_any_mut().downcast_mut::<T::Storage>().unwrap();
			Storage::<T>::remove(storage, entity)
		}))
	}

	pub fn query<'a, 'b: 'a>(
		&'a self,
		components: &'b [TypeId],
	) -> impl Iterator<Item = Entity> + 'a {
		self.entity_factory
			.active_entities()
			.filter(move |&entity| {
				components.iter().all(|component| {
					self.storages.iter().any(|(type_id, storage)| {
						type_id == component && storage.borrow().contains_entity(entity)
					})
				})
			})
	}

	pub fn try_get<T: Component>(&self, entity: Entity) -> Option<Ref<'_, T>> {
		self.try_get_storage::<T>().and_then(|storage| {
			Ref::filter_map(storage, |storage| {
				storage
					.as_any()
					.downcast_ref::<T::Storage>()
					.unwrap()
					.get(entity)
			})
			.ok()
		})
	}

	pub fn try_get_mut<T: Component>(&self, entity: Entity) -> Option<RefMut<'_, T>> {
		self.try_get_storage_mut::<T>().and_then(|storage| {
			RefMut::filter_map(storage, |storage| {
				storage
					.as_any_mut()
					.downcast_mut::<T::Storage>()
					.unwrap()
					.get_mut(entity)
			})
			.ok()
		})
	}

	fn try_get_storage<T: Component>(&self) -> Option<Ref<'_, T::Storage>> {
		self.storages.get(&TypeId::of::<T>()).map(|storage| {
			Ref::map(storage.borrow(), |storage| {
				storage.as_any().downcast_ref().unwrap()
			})
		})
	}

	fn try_get_storage_mut<T: Component>(&self) -> Option<RefMut<'_, T::Storage>> {
		self.storages.get(&TypeId::of::<T>()).map(|storage| {
			RefMut::map(storage.borrow_mut(), |storage| {
				storage.as_any_mut().downcast_mut().unwrap()
			})
		})
	}
}
