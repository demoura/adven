// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::{
	alloc::{AllocError, Layout},
	fmt::{self, Display, Formatter},
};

#[derive(Debug)]
pub enum CreationError {
	AlignMismatch {
		align: usize,
		min_block_size: usize,
	},
	BackingAllocationFailed {
		layout: Layout,
		error: AllocError,
	},
	MemorySizeNotPowerOfTwo(usize),
	MinBlockSizeNotPowerOfTwo(usize),
	SizeMismatch {
		min_block_size: usize,
		memory_size: usize,
	},
}

impl Display for CreationError {
	fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
		match self {
			CreationError::AlignMismatch {
				align,
				min_block_size,
			} => {
				write!(
					fmt,
					"Requested align, {}, can't be greater than min_block_size, {}",
					align, min_block_size
				)
			}
			CreationError::BackingAllocationFailed { layout, error } => {
				write!(
					fmt,
					"Failed to allocate backing storage, with layout {:?}, due to: {}",
					layout, error
				)
			}
			CreationError::MemorySizeNotPowerOfTwo(size) => {
				write!(
					fmt,
					"memory_layout.size(), {}, has to be a power of two",
					size
				)
			}
			CreationError::MinBlockSizeNotPowerOfTwo(size) => {
				write!(fmt, "min_block_size, {}, has to be a power of two", size)
			}
			CreationError::SizeMismatch {
				min_block_size,
				memory_size,
			} => {
				write!(
					fmt,
					"min_block_size, {}, can't be greater than memory_size, {}.",
					min_block_size, memory_size
				)
			}
		}
	}
}
