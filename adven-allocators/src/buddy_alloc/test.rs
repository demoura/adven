// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use alloc::{alloc::Global, format};
use core::{
	alloc::{AllocError, Allocator, Layout},
	cell::Cell,
	ptr::NonNull,
};

use super::BuddyAlloc;

struct MockAllocator {
	expected_alloc_layout: Layout,
	memory: NonNull<[u8]>,
	did_alloc: Cell<bool>,
}

impl MockAllocator {
	fn new(expected_alloc_layout: Layout) -> Self {
		MockAllocator {
			expected_alloc_layout,
			memory: Global.allocate(expected_alloc_layout).unwrap(),
			did_alloc: Cell::new(false),
		}
	}
}

unsafe impl Allocator for MockAllocator {
	fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
		if self.did_alloc.get() {
			panic!("Repeated call to MockAllocator.alloc()")
		}

		if layout != self.expected_alloc_layout {
			panic!(
				"Unexpected alloction to mock allocator with layout: {:?}, expected: {:?}",
				layout, self.expected_alloc_layout
			)
		}

		self.did_alloc.set(true);
		Ok(self.memory)
	}

	unsafe fn deallocate(&self, _ptr: NonNull<u8>, _layout: Layout) {}
}

impl Drop for MockAllocator {
	fn drop(&mut self) {
		unsafe { Global.deallocate(self.memory.as_non_null_ptr(), self.expected_alloc_layout) };
	}
}

#[test]
fn create_alloc_gba_vram() {
	let layout = Layout::new::<[u16; 8192]>();
	let mock_alloc = MockAllocator::new(layout);

	{
		BuddyAlloc::new(layout, 64, mock_alloc.by_ref()).unwrap();
	}

	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}

#[test]
fn create_alloc_gba_heap() {
	let layout = Layout::new::<[u8; 256 * 1024]>();
	let mock_alloc = MockAllocator::new(layout);

	{
		BuddyAlloc::new(layout, 64, mock_alloc.by_ref()).unwrap();
	}

	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}

#[test]
fn allocate_root_block() {
	let layout = Layout::array::<u8>(4096).unwrap();
	let mock_alloc = MockAllocator::new(layout);
	let alloc_ptr = mock_alloc.memory.as_mut_ptr();

	{
		let balloc = BuddyAlloc::new(layout, 64, mock_alloc.by_ref()).unwrap();

		let block = balloc.allocate(layout).expect("Allocation should succeed");

		assert_eq!(block.as_mut_ptr(), alloc_ptr);
		assert_eq!(block.len(), layout.size());
	}

	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}

#[test]
fn allocate_root_children() {
	let layout = Layout::array::<u8>(4096).unwrap();
	let mock_alloc = MockAllocator::new(layout);
	let alloc_ptr = mock_alloc.memory.as_mut_ptr();

	{
		let balloc = BuddyAlloc::new(layout, 64, mock_alloc.by_ref()).unwrap();

		let child_layout = Layout::array::<u8>(2048).unwrap();

		let block1 = balloc
			.allocate(child_layout)
			.expect("First child allocation should succeed");

		assert_eq!(
			block1.as_mut_ptr(),
			alloc_ptr,
			"First child pointer should match"
		);
		assert_eq!(
			block1.len(),
			layout.size() / 2,
			"First child size should match"
		);

		let block2 = balloc
			.allocate(child_layout)
			.expect("Second child allocation should succeed");

		assert_eq!(
			block2.as_mut_ptr() as usize,
			alloc_ptr as usize + layout.size() / 2,
			"Second child pointer should match"
		);
		assert_eq!(
			block2.len(),
			layout.size() / 2,
			"Second child size should match"
		);
	}
	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}

#[test]
fn allocate_min_blocks() {
	let layout = Layout::array::<u8>(4096).unwrap();
	let mock_alloc = MockAllocator::new(layout);
	let alloc_ptr = mock_alloc.memory.as_mut_ptr();

	{
		let balloc = BuddyAlloc::new(layout, 64, mock_alloc.by_ref()).unwrap();

		let child_layout = Layout::array::<u8>(64).unwrap();

		for i in 0..4096 / 64 {
			let alloc_err = format!("Node allocation should succeed, index_in_level: {}", i);

			let block = balloc.allocate(child_layout).expect(&alloc_err);

			assert_eq!(
				block.len(),
				64,
				"Node size should match, index_in_level: {}",
				i
			);

			let info = balloc.alloc_info.take();
			assert_eq!(
				block.as_mut_ptr() as usize,
				alloc_ptr as usize + 64 * i,
				"Node pointer should match, index_in_level: {}, {:#?}, {:#?}",
				i,
				&info.free_blocks(),
				&info.split_blocks()
			);

			balloc.alloc_info.set(info);
		}
	}

	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}

#[test]
fn allocate_align_base() {
	let layout = Layout::array::<u8>(4096).unwrap();
	let mock_alloc = MockAllocator::new(layout);
	let alloc_ptr = mock_alloc.memory.as_mut_ptr();

	{
		let balloc = BuddyAlloc::new(layout, 128, mock_alloc.by_ref()).unwrap();

		let child_layout = Layout::new::<u64>();

		for i in 0..4096 / 128 {
			let alloc_err = format!("Node allocation should succeed, index_in_level: {}", i);

			let block = balloc.allocate(child_layout).expect(&alloc_err);

			assert_eq!(
				block.len(),
				128,
				"Node size should match, index_in_level: {}",
				i
			);

			let info = balloc.alloc_info.take();
			assert_eq!(
				block.as_mut_ptr() as usize,
				alloc_ptr as usize + 128 * i,
				"Node pointer should match, index_in_level: {}, {:#?}, {:#?}",
				i,
				&info.free_blocks(),
				&info.split_blocks()
			);

			balloc.alloc_info.set(info);
		}
	}

	assert!(
		mock_alloc.did_alloc.get(),
		"Should have allocated backing storage"
	);
}
