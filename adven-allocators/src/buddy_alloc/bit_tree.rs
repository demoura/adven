// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Binary tree backed by a bitvec.

use core::fmt;

use adven_math::Pow2;
use bitvec::prelude::*;

#[derive(Copy, Clone)]
pub struct BitNode(pub usize);

pub trait BitTree {
	fn node(self, level: u32, index: usize) -> Option<BitNode>;
	fn parent(self, node: BitNode) -> Option<BitNode>;
	fn children(self, node: BitNode) -> Option<[BitNode; 2]>;
	fn value(self, node: BitNode) -> bool;
	fn level(self, node: BitNode) -> u32;
	fn index_in_level(self, node: BitNode) -> (u32, usize);
	fn pprint_tree(self, node: BitNode) -> fmt::Result;
}

pub trait BitTreeMut {
	fn set_value(&mut self, node: BitNode, value: bool);
}

impl<'a, R, O> BitTree for &'a BitSlice<R, O>
where
	R: BitStore,
	O: BitOrder,
{
	fn node(self, level: u32, index: usize) -> Option<BitNode> {
		let level_node_count = usize::checked_pow2(level / 2)?;

		if index >= level_node_count {
			// Index out of level bounds
			return None;
		}

		let subtree_node_count = 2 * level_node_count - 1;

		if subtree_node_count > self.len() {
			// Level out of bounds
			return None;
		}

		// Tree size before level is equal to level size - 1
		let level_start_index = level_node_count - 1;

		let flat_index = level_start_index + index;

		Some(BitNode(flat_index))
	}

	fn parent(self, node: BitNode) -> Option<BitNode> {
		if node.0 != 0 {
			let flat_index = node.0.checked_add(1)? / 2 - 1;
			Some(BitNode(flat_index))
		} else {
			None
		}
	}

	fn children(self, node: BitNode) -> Option<[BitNode; 2]> {
		let right_child_index = node.0.checked_add(1)? * 2;

		if right_child_index < self.len() {
			Some([BitNode(right_child_index - 1), BitNode(right_child_index)])
		} else {
			None
		}
	}

	fn value(self, node: BitNode) -> bool {
		*self.get(node.0).unwrap()
	}

	fn level(self, node: BitNode) -> u32 {
		adven_math::log2(node.0 + 1)
	}

	fn index_in_level(self, node: BitNode) -> (u32, usize) {
		let level = self.level(node);
		let index = node.0 - (usize::pow2(level) - 1);

		(level, index)
	}

	/// Copyed from: https://vallentin.dev/2019/05/14/pretty-print-tree
	fn pprint_tree(self, node: BitNode) -> fmt::Result {
		use alloc::string::{String, ToString};

		fn pprint_tree<R, O>(
			tree: &BitSlice<R, O>,
			node: BitNode,
			prefix: String,
			last: bool,
		) -> fmt::Result
		where
			R: BitStore,
			O: BitOrder,
		{
			let prefix_current = if last { "`- " } else { "|- " };

			log::info!(
				"{}{}{}: {}",
				prefix,
				prefix_current,
				node.0,
				tree.value(node)
			);

			let prefix_child = if last { "   " } else { "|  " };
			let prefix = prefix + prefix_child;

			if let Some(children) = tree.children(node) {
				if let Err(error) = pprint_tree(tree, children[0], prefix.to_string(), false) {
					return Err(error);
				} else {
					return pprint_tree(tree, children[1], prefix, true);
				}
			}

			Ok(())
		}

		pprint_tree(self, node, "".to_string(), true)
	}
}

impl<'a, R, O> BitTreeMut for &'a mut BitSlice<R, O>
where
	R: BitStore,
	O: BitOrder,
{
	fn set_value(&mut self, node: BitNode, value: bool) {
		self.set(node.0, value)
	}
}
