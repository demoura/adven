// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![no_std]
#![feature(allocator_api)]
#![feature(alloc_layout_extra)]
#![feature(slice_ptr_get)]
#![feature(slice_ptr_len)]

//! # Allocators for no_std projects.
//!

extern crate alloc;

mod buddy_alloc;
mod raw_alloc;
mod rc_alloc;

pub use buddy_alloc::BuddyAlloc;
pub use raw_alloc::RawAlloc;
pub use rc_alloc::RcAlloc;
