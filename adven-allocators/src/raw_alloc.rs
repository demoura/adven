// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::{
	alloc::{AllocError, Allocator, Layout},
	ptr::NonNull,
	sync::atomic::{AtomicBool, Ordering},
};

/// Memory allocator intended to be used at the base of an allocator stack,
/// in a no_std, no operating system environment.
///
/// "Allocates" a single memory block backed by the assigned memory region.
///
/// # Examples
///
/// ```
/// #![feature(allocator_api)]
/// extern crate alloc;
/// use core::ptr::NonNull;
/// use core::alloc::{Allocator, Layout};
/// use adven_allocators::RawAlloc;
///
/// // Suppose that there is 16kb slice of free memory that can
/// // be safely acccesed.
/// let mut backing_memory = [0; 16384];
/// let mut raw_alloc = RawAlloc::new(&mut backing_memory[..]);
///
/// // RawAlloc can only allocate a single block, as it's is meant to be used
/// // by other allocators, and not directly. But suppose you really want an array
/// // of 8192 u16's at that specfic region.
/// let layout = Layout::new::<[u16; 8192]>();
/// let memory = match raw_alloc.allocate(layout) {
///     Ok(memory) => memory,
///     // Failure might occur due to requesting a memory block that is too big,
///     // or due to aligment issues.
///     Err(_) => alloc::alloc::handle_alloc_error(layout),
/// };
///
/// // Deallocate memory once you are done with it.
/// unsafe { raw_alloc.deallocate(memory.cast::<u8>(), layout) };
/// ```
pub struct RawAlloc<'a> {
	memory: &'a mut [u8],
	allocated: AtomicBool,
}

/// # Safety
/// This can be safely moved between threads as it should
/// be the sole owner of it's memory slice.
unsafe impl Send for RawAlloc<'_> {}

/// # Safety
/// The only mutable internal state, is the allocated flag,
/// which is atomic.
unsafe impl Sync for RawAlloc<'_> {}

impl<'a> RawAlloc<'a> {
	/// Creates a new raw allocator for the region defined by `memory`.
	pub fn new(memory: &'a mut [u8]) -> Self {
		RawAlloc::<'a> {
			memory,
			allocated: AtomicBool::new(false),
		}
	}
}

unsafe impl Allocator for RawAlloc<'_> {
	fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
		let offset = self.memory.as_ptr().align_offset(layout.align());

		// Can't align pointer.
		if offset == usize::MAX {
			log::error!("Can't offset pointer");
			return Err(AllocError);
		}

		let aligned_ptr = self.memory.as_ptr() as usize + offset;
		let memory_end = self.memory.as_ptr() as usize + self.memory.len() - 1;

		// Can't align pointer and stay within memory block.
		if aligned_ptr > memory_end {
			log::error!("Can't align pointer");
			return Err(AllocError);
		}

		// Request allocation is too big.
		if memory_end - aligned_ptr + 1 < layout.size() {
			log::error!("Too big allocation");
			return Err(AllocError);
		}

		// It is non null because it's an offset of a nonnull pointer.
		let aligned_ptr = unsafe { NonNull::new_unchecked(aligned_ptr as *mut u8) };

		let memory_slice = NonNull::slice_from_raw_parts(aligned_ptr, self.memory.len() - offset);

		if self
			.allocated
			.compare_exchange(false, true, Ordering::AcqRel, Ordering::Acquire)
			.is_err()
		{
			log::error!("Already allocated");
			return Err(AllocError);
		}

		Ok(memory_slice)
	}

	unsafe fn deallocate(&self, _ptr: NonNull<u8>, _layout: Layout) {
		self.allocated.store(true, Ordering::Relaxed);
	}
}
