// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

mod bit_tree;
mod error;
#[cfg(test)]
mod test;

use core::{
	alloc::{AllocError, Allocator, Layout},
	cell::Cell,
	ptr::NonNull,
};

use bit_tree::{BitNode, BitTree, BitTreeMut};
use bitvec::prelude::*;
pub use error::CreationError;

#[derive(Clone, Copy)]
enum SplitNode {
	Synced(BitNode),
	NonSynced,
}

#[derive(Clone, Copy)]
struct Block {
	free_node: BitNode,
	split_node: SplitNode,
}

enum BuddyAllocInfo<R, O>
where
	R: BitStore,
	O: BitOrder,
{
	External {
		free_blocks: BitBox<R, O>,
		split_blocks: BitBox<R, O>,
	},
	SelfContained {
		free_blocks: NonNull<[R]>,
		split_blocks: NonNull<[R]>,
	},
}

impl<R, O> Default for BuddyAllocInfo<R, O>
where
	R: BitStore,
	O: BitOrder,
{
	fn default() -> BuddyAllocInfo<R, O> {
		BuddyAllocInfo::External {
			free_blocks: BitBox::default(),
			split_blocks: BitBox::default(),
		}
	}
}

impl<R, O> BuddyAllocInfo<R, O>
where
	R: BitStore,
	O: BitOrder,
{
	fn free_blocks(&self) -> &BitSlice<R, O> {
		match self {
			BuddyAllocInfo::External { free_blocks, .. } => free_blocks.as_bitslice(),
			BuddyAllocInfo::SelfContained { free_blocks, .. } => unsafe {
				BitSlice::from_slice_unchecked(free_blocks.as_ref())
			},
		}
	}

	fn free_blocks_mut(&mut self) -> &mut BitSlice<R, O> {
		match self {
			BuddyAllocInfo::External { free_blocks, .. } => free_blocks.as_mut_bitslice(),
			BuddyAllocInfo::SelfContained { free_blocks, .. } => unsafe {
				BitSlice::from_slice_unchecked_mut(free_blocks.as_mut())
			},
		}
	}

	fn split_blocks(&self) -> &BitSlice<R, O> {
		match self {
			BuddyAllocInfo::External { split_blocks, .. } => split_blocks.as_bitslice(),
			BuddyAllocInfo::SelfContained { split_blocks, .. } => unsafe {
				BitSlice::from_slice_unchecked(split_blocks.as_ref())
			},
		}
	}

	fn split_blocks_mut(&mut self) -> &mut BitSlice<R, O> {
		match self {
			BuddyAllocInfo::External { split_blocks, .. } => split_blocks.as_mut_bitslice(),
			BuddyAllocInfo::SelfContained { split_blocks, .. } => unsafe {
				BitSlice::from_slice_unchecked_mut(split_blocks.as_mut())
			},
		}
	}

	fn level(&self, block: Block) -> u32 {
		self.free_blocks().level(block.free_node)
	}

	fn index_in_level(&self, block: Block) -> (u32, usize) {
		self.free_blocks().index_in_level(block.free_node)
	}

	fn is_split(&self, block: Block) -> bool {
		match block.split_node {
			SplitNode::Synced(node) => self.split_blocks().value(node),
			SplitNode::NonSynced { .. } => false,
		}
	}

	fn is_free(&self, block: Block) -> bool {
		self.free_blocks().value(block.free_node)
	}

	fn parent(&self, block: Block) -> Option<Block> {
		Some(Block {
			free_node: self.free_blocks().parent(block.free_node)?,
			split_node: SplitNode::Synced(self.split_blocks().parent(block.free_node)?),
		})
	}

	fn children(&self, block: Block) -> Option<[Block; 2]> {
		// free_node has one more level than split_nodes

		// Try get children of free_node
		if let Some(free_nodes) = self.free_blocks().children(block.free_node) {
			// free node is synced with split node, except in the last free_blocks level
			// So as free_node has children, split_node should be guaranteed to be synced.
			if let SplitNode::Synced(split_node) = block.split_node {
				// Try get children of split_node
				if let Some(split_nodes) = self.split_blocks().children(split_node) {
					Some([
						Block {
							free_node: free_nodes[0],
							split_node: SplitNode::Synced(split_nodes[0]),
						},
						Block {
							free_node: free_nodes[1],
							split_node: SplitNode::Synced(split_nodes[1]),
						},
					])
				} else {
					// Free_node had children, but not split_node, so this is the last level.
					let unsynced_split_node = SplitNode::NonSynced;

					Some([
						Block {
							free_node: free_nodes[0],
							split_node: unsynced_split_node,
						},
						Block {
							free_node: free_nodes[1],
							split_node: unsynced_split_node,
						},
					])
				}
			} else {
				unreachable!()
			}
		} else {
			// If free_node has no children, split_node is guaranteed to not have any.
			None
		}
	}

	fn block_size(&self, block: Block, memory_size: usize) -> usize {
		memory_size >> self.level(block)
	}

	fn find_block(&self, memory_size: usize, desired_size: usize) -> Option<Block> {
		// This is the start of the next row following the desired_size row.
		let end_index = memory_size / (desired_size / 2) - 1;

		// Only the blocks from start to the end of the desired_row matter.
		let slice = &self.free_blocks()[0..end_index];

		// Search backwards as we want the smallest block that fits.
		let node = BitNode(slice.last_one()?);

		Some(Block {
			free_node: node,
			split_node: if node.0 < self.split_blocks().len() {
				SplitNode::Synced(node)
			} else {
				SplitNode::NonSynced
			},
		})
	}

	/// # Panics
	/// Panics if desired_size is smaller min_block_size,
	/// if free_blocks is smaller than split_blocks
	fn split_block(&mut self, mut block: Block, memory_size: usize, desired_size: usize) -> Block {
		loop {
			if self.block_size(block, memory_size) == desired_size {
				return block;
			} else {
				self.split_blocks_mut().set_value(
					match block.split_node {
						SplitNode::Synced(node) => node,
						SplitNode::NonSynced { .. } => unreachable!(), //If desired_size is smaller than minimum size.
					},
					true,
				);

				match self.children(block) {
					Some(children) => {
						self.free_blocks_mut()
							.set_value(children[1].free_node, true);
						block = children[0];
					}
					None => unreachable!(), //If free_blocks is smaller than split_blocks
				}
			}
		}
	}

	fn root_block(&self) -> Block {
		// The first allocator block always exists.
		Block {
			free_node: self.free_blocks().node(0, 0).unwrap(),
			split_node: SplitNode::Synced(self.split_blocks().node(0, 0).unwrap()),
		}
	}
}

// Default type parameters don't quite work with inference.
type O = Lsb0;
type R = usize;

/// [Buddy memory allocator](https://en.wikipedia.org/wiki/Buddy_memory_allocation)
/// implemented using two binary trees stored in a `BitVec`.
///
/// This allocator can either be entirely self-contained, and store the management
/// data in it's own memory, effectively allocating the first block(s) for itself.
/// Or it can allocate the management data in the global heap, and not allocate any
/// memory from itself.
///
/// The [self-contained method](Self::new_self_contained) is needed when using BuddyAlloc
/// as the global allocator, as there is nowhere else to allocate memory from.
/// [Using the global allocator](Self::new) is desirable in some situations, such as when
/// using BuddyAlloc as a video ram allocator, where you don't want to lose precious VRAM blocks.
pub struct BuddyAlloc<A>
where
	A: Allocator,
	// O: BitOrder,
	// R: BitStore + BitMemory,
{
	base_allocator: A,
	memory_slice: NonNull<[u8]>,
	memory_layout: Layout,
	min_block_size: usize,
	alloc_info: Cell<BuddyAllocInfo<R, O>>,
}

unsafe impl<A: Allocator + Send> Send for BuddyAlloc<A> {}

impl<A> BuddyAlloc<A>
where
	A: Allocator,
	// O: BitOrder,
	// R: BitStore + BitMemory,
{
	/// Creates a new `BuddyAlloc` using `base_allocator` to allocate the backing storage.
	/// The management data structures will be allocated on the global heap. If this is not
	/// desired, use the [new_self_contained()] function instead.
	///
	/// # Arguments
	/// `memory_layout`: Layout for the backing storage, it's size must be a power of two,
	/// and the alignment should be mininum aligment wanted for the blocks `BuddyAlloc` allocates.
	/// However the alignment can't be greater than `min_block_size`, as the blocks can't have
	/// padding between them.
	///
	/// `min_block_size`: The minimum allocation block size, any allocations smaller than this
	/// will be rounded up to min_block_size.
	///
	/// `base_allocator`: Allocator for the backing storage, which is fully allocated
	/// immediately, and then only deallocated on drop.
	///
	/// # Errors
	///
	/// This function errors if any of the arguments are invalid:
	/// - `min_block_size` is greater the backing storage size.
	/// - Either the `min_block_size` or the `memory_layout.size()` is not a power of two.
	/// - `memory_layout.align()` is greater than `min_block_size`.
	///
	/// An error is also returned if `base_allocator` fails to allocate `memory_layout`.
	///
	/// [AllocError]: https://doc.rust-lang.org/core/alloc/struct.AllocError.html
	/// [new_self_contained()]: Self::new_self_contained
	pub fn new(
		memory_layout: Layout,
		min_block_size: usize,
		base_allocator: A,
	) -> Result<BuddyAlloc<A>, CreationError> {
		Self::new_base(
			memory_layout,
			min_block_size,
			base_allocator,
			Self::init_info_global,
		)
	}

	/// Creates a new `BuddyAlloc` using `base_allocator` to allocate the backing storage.
	/// The management data structures will be allocated internally in this `BuddyAlloc`'s
	/// memory. If this is not desired, use the [new()] function instead.
	///
	/// # Arguments
	/// `memory_layout`: Layout for the backing storage, it's size must be a power of two,
	/// and the alignment should be mininum aligment wanted for the blocks `BuddyAlloc` allocates.
	/// However the alignment can't be greater than `min_block_size`, as the blocks can't have
	/// padding between them.
	///
	/// `min_block_size`: The minimum allocation block size, any allocations smaller than this
	/// will be rounded up to min_block_size.
	///
	/// `base_allocator`: Allocator for the backing storage, which is fully allocated
	/// immediately, and then only deallocated on drop.
	///
	/// # Errors
	///
	/// This function errors if any of the arguments are invalid:
	/// - `min_block_size` is greater the backing storage size.
	/// - Either the `min_block_size` or the `memory_layout.size()` is not a power of two.
	/// - `memory_layout.align()` is greater than `min_block_size`.
	///
	/// An error is also returned if `base_allocator` fails to allocate `memory_layout`.
	///
	/// [AllocError]: https://doc.rust-lang.org/core/alloc/struct.AllocError.html
	/// [new()]: Self::new
	pub fn new_self_contained(
		memory_layout: Layout,
		min_block_size: usize,
		base_allocator: A,
	) -> Result<Self, CreationError> {
		Self::new_base(
			memory_layout,
			min_block_size,
			base_allocator,
			Self::init_info_self_contained,
		)
	}

	fn new_base<F>(
		memory_layout: Layout,
		min_block_size: usize,
		base_allocator: A,
		init_info: F,
	) -> Result<Self, CreationError>
	where
		F: FnOnce(NonNull<[u8]>, usize) -> (BuddyAllocInfo<R, O>, Option<Layout>),
	{
		// The wall of preconditions.
		if min_block_size > memory_layout.size() {
			return Err(CreationError::SizeMismatch {
				min_block_size,
				memory_size: memory_layout.size(),
			});
		}
		if !min_block_size.is_power_of_two() {
			return Err(CreationError::MinBlockSizeNotPowerOfTwo(min_block_size));
		}
		if !memory_layout.size().is_power_of_two() {
			return Err(CreationError::MemorySizeNotPowerOfTwo(memory_layout.size()));
		}
		if memory_layout.align() > min_block_size {
			return Err(CreationError::AlignMismatch {
				align: memory_layout.align(),
				min_block_size,
			});
		}
		// End wall of preconditions

		let memory_slice = base_allocator
			.allocate_zeroed(memory_layout)
			.map_err(|error| CreationError::BackingAllocationFailed {
				layout: memory_layout,
				error,
			})?;

		let memory_layout = {
			// Allocator may return more memory than requested, if possible use it.
			let memory_size = adven_math::previous_power_of_two(memory_slice.len());

			// Try to layout with the new memory_size, if it fails fall back to the
			// requested layout.
			Layout::from_size_align(memory_size, memory_layout.align()).unwrap_or(memory_layout)
		};

		// node_count = memory_size / min_block_size * 2 - 1
		let node_count = (memory_layout.size() >> adven_math::log2(min_block_size / 2)) - 1;

		let (mut alloc_info, required_alloc) = init_info(memory_slice, node_count);
		let root = alloc_info.free_blocks().node(0, 0).unwrap();
		alloc_info.free_blocks_mut().set_value(root, true);

		let buddy_alloc = BuddyAlloc {
			base_allocator,
			memory_slice,
			memory_layout,
			min_block_size,
			alloc_info: Cell::new(alloc_info),
		};

		if let Some(required_alloc) = required_alloc {
			buddy_alloc
				.allocate(required_alloc)
				.expect("Allocating the free and split blocks bittrees should never fail");
		}

		Ok(buddy_alloc)
	}

	fn init_info_global(
		_memory_slice: NonNull<[u8]>,
		node_count: usize,
	) -> (BuddyAllocInfo<R, O>, Option<Layout>) {
		let info = BuddyAllocInfo::External {
			free_blocks: bitbox![R, O; 0; node_count],
			split_blocks: bitbox![R, O; 0; node_count >> 1],
		};

		(info, None)
	}

	fn init_info_self_contained(
		memory_slice: NonNull<[u8]>,
		node_count: usize,
	) -> (BuddyAllocInfo<R, O>, Option<Layout>) {
		let free_block_len = bitvec::mem::elts::<R>(node_count);

		let mut free_blocks = NonNull::slice_from_raw_parts(
			memory_slice.as_non_null_ptr().cast::<R>(),
			free_block_len,
		);

		unsafe {
			for i in 0..free_block_len {
				free_blocks.as_mut()[i] = R::default();
			}
		}

		let split_blocks_start = unsafe {
			let start = memory_slice.as_mut_ptr().cast::<R>().add(free_block_len);
			NonNull::new_unchecked(start)
		};

		// Blocks with min size (last level) can't be split.
		let split_blocks_len = bitvec::mem::elts::<R>(node_count >> 1);

		let mut split_blocks = NonNull::slice_from_raw_parts(split_blocks_start, split_blocks_len);

		unsafe {
			for i in 0..split_blocks_len {
				split_blocks.as_mut()[i] = R::default();
			}
		}

		let info = BuddyAllocInfo::SelfContained {
			free_blocks,
			split_blocks,
		};
		let internal_alloc = unsafe {
			Layout::from_size_align_unchecked(
				(free_block_len + split_blocks_len) * core::mem::size_of::<R>(),
				core::mem::align_of::<R>(),
			)
		};

		(info, Some(internal_alloc))
	}
}

impl<A> BuddyAlloc<A>
where
	A: Allocator,
	// O: BitOrder,
	// R: BitStore + BitMemory,
{
	fn block_contains_ptr(
		&self,
		blocks: &BuddyAllocInfo<R, O>,
		block: Block,
		ptr: NonNull<u8>,
	) -> bool {
		let block_size = blocks.block_size(block, self.memory_layout.size());
		let block_start =
			self.memory_slice.as_mut_ptr() as usize + blocks.index_in_level(block).1 * block_size;

		let block_end = block_start + block_size;
		let ptr = ptr.as_ptr() as usize;

		ptr >= block_start && ptr < block_end
	}

	/// Finds an allocated block, by it's pointer.
	/// [ptr]: Must be within the allocators memory.
	fn find_allocated_block(
		&self,
		blocks: &BuddyAllocInfo<R, O>,
		mut block: Block,
		ptr: NonNull<u8>,
		allocation_size: usize,
	) -> Option<Block> {
		loop {
			let block_size = blocks.block_size(block, self.memory_layout.size());
			if block_size < allocation_size || blocks.is_free(block) {
				// All blocks are smaller than required, return.
				// Free blocks are definetly not allocated.
				return None;
			} else if !blocks.is_split(block) {
				return Some(block);
			} else {
				match blocks.children(block) {
					Some(children) => {
						if self.block_contains_ptr(blocks, children[0], ptr) {
							// Loop
							block = children[0];
						} else if self.block_contains_ptr(blocks, children[1], ptr) {
							// Loop
							block = children[1];
						} else {
							unreachable!("ptr has to be contained in one of the children")
						}
					}
					None => unreachable!("Block was split, so there has to be children."),
				}
			}
		}
	}
}

unsafe impl<A> Allocator for BuddyAlloc<A>
where
	A: Allocator,
	// O: BitOrder,
	// R: BitStore + BitMemory,
{
	fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
		let desired_size = if layout.size() > self.min_block_size {
			layout
				.size()
				.checked_next_power_of_two()
				.ok_or(AllocError)?
		} else {
			self.min_block_size
		};

		let memory_size = self.memory_layout.size();
		let mut blocks = self.alloc_info.take();

		let mut best_block = match blocks.find_block(memory_size, desired_size) {
			Some(block) => block,
			None => {
				self.alloc_info.set(blocks);
				return Err(AllocError);
			}
		};

		// Mark the best block as in use.
		blocks
			.free_blocks_mut()
			.set_value(best_block.free_node, false);

		if blocks.block_size(best_block, memory_size) != desired_size {
			best_block = blocks.split_block(best_block, memory_size, desired_size);
		}

		let block_ptr = unsafe {
			let pos = self.memory_slice.as_mut_ptr() as usize
				+ blocks.index_in_level(best_block).1 * desired_size;
			NonNull::new_unchecked(pos as *mut u8)
		};

		self.alloc_info.set(blocks);
		let memory_slice = NonNull::slice_from_raw_parts(block_ptr, desired_size);

		Ok(memory_slice)
	}

	unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
		let mut blocks = self.alloc_info.take();
		let root_block = blocks.root_block();

		// Sanity checks.
		assert!(
			self.block_contains_ptr(&blocks, root_block, ptr),
			"Invalid pointer to dealloc: {:?}",
			ptr
		);
		assert!(
			layout.size() < self.memory_slice.len(),
			"Too large size to dealloc: {:?}",
			layout
		);

		// Get BitNode for ptr.
		let mut block = self
			.find_allocated_block(&blocks, root_block, ptr, layout.size())
			.expect("There should be a block to dealloc");

		while let Some(parent) = blocks.parent(block) {
			let is_even = block.free_node.0 & 1 == 0;
			let neighbor = BitNode(if is_even {
				// This block is to the right of it's pair.
				block.free_node.0 - 1
			} else {
				// This block is to the left of it's pair.
				block.free_node.0 + 1
			});

			let can_merge = blocks.free_blocks().value(neighbor);

			if can_merge {
				blocks.free_blocks_mut().set_value(neighbor, false);
				blocks.split_blocks_mut().set_value(parent.free_node, false);
				block = parent;
			} else {
				break;
			}
		}

		// Mark highest level block as free.
		blocks.free_blocks_mut().set_value(block.free_node, true);
		self.alloc_info.set(blocks);
	}
}

impl<A> Drop for BuddyAlloc<A>
where
	A: Allocator,
	// O: BitOrder,
	// R: BitStore + BitMemory,
{
	fn drop(&mut self) {
		unsafe {
			self.base_allocator
				.deallocate(self.memory_slice.as_non_null_ptr(), self.memory_layout);
		}
	}
}
