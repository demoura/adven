// adven-allocators <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use alloc::rc::Rc;
use core::{
	alloc::{AllocError, Allocator, Layout},
	ptr::NonNull,
};

pub struct RcAlloc<A: Allocator>(pub Rc<A>);

impl<A> Clone for RcAlloc<A>
where
	A: Allocator,
{
	fn clone(&self) -> Self {
		Self(Rc::clone(&self.0))
	}
}

unsafe impl<A> Allocator for RcAlloc<A>
where
	A: Allocator,
{
	fn allocate(&self, layout: Layout) -> Result<NonNull<[u8]>, AllocError> {
		(*self.0).allocate(layout)
	}

	unsafe fn deallocate(&self, ptr: NonNull<u8>, layout: Layout) {
		(*self.0).deallocate(ptr, layout)
	}
}
