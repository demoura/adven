# Adven

Adven is a game engine for developing Gameboy Advance games with Rust.

Very much a WIP proof of concept. API will break.

<sub>There was an earlier version of Adven, written in C++. It has since been abandoned.</sub>

### License

Adven is licensed under the [Mozilla Public License 2.0](./LICENSE).
Some components of Adven also contain or are based on third party code:

- Contains modified [rust-console/gba](https://github.com/rust-console/gba) code, original code is licensed under [Apache 2.0](./third-party-licenses/rust-console-gba-LICENSE-APACHE).
	- [adven-bootloader linker script](./adven-bootloader/src/adven-bootloader.ld)

For more details, read the linked file headers and the [third party license files](./third-party-licenses).
