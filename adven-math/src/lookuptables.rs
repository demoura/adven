// adven-math <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::{convert::TryInto, ops::Index};

use az::{SaturatingAs, SaturatingCast};
use fixed::{
	types::{extra::LeEqU32, I2F30},
	FixedI32,
};

use super::{Cos, Pow2, Sin};

static COS_LUT: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/cos_lut"));
static SIN_LUT: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/sin_lut"));

pub fn lookup_lut(lut: &[u8], x: usize) -> I2F30 {
	let y: [u8; 4] = lut.index(x * 4..(x * 4) + 4).try_into().unwrap();
	I2F30::from_le_bytes(y)
}

pub fn lut_lerp32<FracSrc>(lut: &[u8], x: FixedI32<FracSrc>) -> I2F30
where
	FracSrc: LeEqU32,
	I2F30: SaturatingCast<FixedI32<FracSrc>>,
{
	let xa = x.to_num::<usize>();

	let ya = lookup_lut(lut, xa);
	let yb = lookup_lut(lut, xa + 1);

	ya + ((yb - ya) * (x - FixedI32::<FracSrc>::from_num(xa)).saturating_as::<I2F30>())
}

fn wrap_angle<FracSrc>(angle: FixedI32<FracSrc>) -> FixedI32<FracSrc>
where
	FracSrc: LeEqU32,
{
	let mask =
		FixedI32::from_num(511) | FixedI32::from_bits((usize::pow2(FracSrc::to_u32()) - 1) as i32);

	angle & mask
}

impl<FracSrc, FracDst> Cos<FixedI32<FracSrc>> for FixedI32<FracDst>
where
	FracSrc: LeEqU32,
	FracDst: LeEqU32,
	I2F30: SaturatingCast<FixedI32<FracSrc>>,
{
	type Output = Self;

	fn cos(angle: FixedI32<FracSrc>) -> Self::Output {
		Self::from_num(lut_lerp32(COS_LUT, wrap_angle(angle)))
	}
}

impl<FracDst: LeEqU32> Cos<usize> for FixedI32<FracDst> {
	type Output = Self;

	fn cos(angle: usize) -> Self::Output {
		Self::from_num(lookup_lut(COS_LUT, angle & 511))
	}
}

impl<FracSrc, FracDst> Sin<FixedI32<FracSrc>> for FixedI32<FracDst>
where
	FracSrc: LeEqU32,
	FracDst: LeEqU32,
	I2F30: SaturatingCast<FixedI32<FracSrc>>,
{
	type Output = Self;

	fn sin(angle: FixedI32<FracSrc>) -> Self::Output {
		Self::from_num(lut_lerp32(SIN_LUT, wrap_angle(angle)))
	}
}

impl<FracDst: LeEqU32> Sin<usize> for FixedI32<FracDst> {
	type Output = Self;

	fn sin(angle: usize) -> Self::Output {
		Self::from_num(lookup_lut(SIN_LUT, angle & 511))
	}
}
// impl<T> Sin<usize> for T
// where
// 	I2F30: SaturatingCast<T>,
// {
//     type Output = Self;

//     fn sin(angle: usize) -> Self::Output {
// 		lookup_lut(SIN_LUT, angle).saturating_cast()
//     }
// }

#[cfg(test)]
mod test {
	use core::f32;

	use approx::*;
	use fixed::types::{I16F16, I2F30};
	use fixed_macro::fixed;

	use crate::{Cos, Sin};

	#[test]
	fn test_cos_lookup() {
		for i in 0..=512usize {
			let radians = i as f32 * f32::consts::PI / 256.0;
			// println!("radians: {}, angle: {}, expected: {}, actual: {}",
			// 	radians, i,
			// 	f32::cos(radians), I2F30::cos(i).to_num::<f32>());
			assert_abs_diff_eq!(
				f32::cos(radians),
				I2F30::cos(i).to_num::<f32>(),
				epsilon = 0.0000002
			);
		}
	}

	#[test]
	fn test_cos_interpolation() {
		for i in 0..=512usize {
			for d in 0..4 {
				let angle = I16F16::from_num(i) + fixed!(0.25: I16F16) * d;
				let radians = angle.to_num::<f32>() * f32::consts::PI / 256.0;

				// println!("radians: {}, angle: {}, expected: {}, actual: {}",
				// 	radians, angle,
				// 	f32::cos(radians), I2F30::cos(angle).to_num::<f32>());
				assert_abs_diff_eq!(
					f32::cos(radians),
					I2F30::cos(angle).to_num::<f32>(),
					epsilon = 0.0001
				);
			}
		}
	}

	#[test]
	fn test_sin_lookup() {
		for i in 0..=512usize {
			let radians = i as f32 * f32::consts::PI / 256.0;
			// println!("radians: {}, angle: {}, expected: {}, actual: {}",
			// 	radians, i,
			// 	f32::sin(radians), I2F30::sin(i).to_num::<f32>());
			assert_abs_diff_eq!(
				f32::sin(radians),
				I2F30::sin(i).to_num::<f32>(),
				epsilon = 0.0000002
			);
		}
	}

	#[test]
	fn test_sin_interpolation() {
		for i in 0..=512usize {
			for d in 0..4 {
				let angle = I16F16::from_num(i) + fixed!(0.25: I16F16) * d;
				let radians = angle.to_num::<f32>() * f32::consts::PI / 256.0;

				// println!("radians: {}, angle: {}, expected: {}, actual: {}",
				// 	radians, angle,
				// 	f32::sin(radians), I2F30::sin(angle).to_num::<f32>());
				assert_abs_diff_eq!(
					f32::sin(radians),
					I2F30::sin(angle).to_num::<f32>(),
					epsilon = 0.0001
				);
			}
		}
	}
}
