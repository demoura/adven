// adven-math <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![cfg_attr(not(test), no_std)]

mod lookuptables;
mod matrix3;
mod vector2;

pub use az;
pub use bounded_integer::BoundedI16 as bi16;
pub use bounded_integer::BoundedI32 as bi32;
pub use bounded_integer::BoundedI64 as bi64;
pub use bounded_integer::BoundedI8 as bi8;
pub use bounded_integer::BoundedIsize as bisize;
pub use bounded_integer::BoundedU16 as bu16;
pub use bounded_integer::BoundedU32 as bu32;
pub use bounded_integer::BoundedU64 as bu64;
pub use bounded_integer::BoundedU8 as bu8;
pub use bounded_integer::BoundedUsize as busize;
pub use fixed;
pub use matrix3::Matrix3;
pub use num_traits;
pub use vector2::Vector2;

pub trait Cos<T = Self> {
	type Output;

	fn cos(angle: T) -> Self::Output;
}

pub trait Sin<T = Self> {
	type Output;

	fn sin(angle: T) -> Self::Output;
}

pub trait Pow2 {
	type Output;

	fn pow2(exp: u32) -> Self::Output;
	fn checked_pow2(exp: u32) -> Option<Self::Output>;
	fn overflowing_pow2(exp: u32) -> (Self::Output, bool);
	fn saturating_pow2(exp: u32) -> Self::Output;
	fn wrapping_pow2(exp: u32) -> Self::Output;
}

macro_rules! pow2_impl {
    ($($t:ty)*) => ($(
        impl Pow2 for $t {
            type Output = $t;

            #[inline]
            fn pow2(exp: u32) -> $t { (1 as $t) << exp }

            #[inline]
            fn checked_pow2(exp: u32) -> Option<$t> { (1 as $t).checked_shl(exp) }

            #[inline]
            fn overflowing_pow2(exp: u32) -> ($t, bool) { (1 as $t).overflowing_shl(exp) }

            #[inline]
            fn saturating_pow2(exp: u32) -> $t {
               match Self::checked_pow2(exp) {
                    Some(x) => x,
                    None => Self::max_value(),
                }
            }

            #[inline]
            fn wrapping_pow2(exp: u32) -> $t { (1 as $t).wrapping_shl(exp) }
        }
    )*)
}

pow2_impl! { u8 u16 u32 u64 u128 usize }

/// Computes the base 2 logarithm of `num`.
///
/// # Examples
///
/// Result is always rounded down.
///
/// ```
/// # use adven_math::log2;
/// assert_eq!(10, log2(1024)); // log2(1024) == floor(10.000)
/// assert_eq!(10, log2(1025)); // log2(1025) == floor(10.001)
/// assert_eq!(10, log2(2047)); // log2(2047) == floor(10.999)
/// ```
///
/// # Panics
///
/// Panics if `num` is zero.
/// ```should_panic
/// # use adven_math::log2;
/// // The logarithm of zero is undefined.
/// log2(0);
/// ```
pub fn log2(num: usize) -> u32 {
	if num != 0 {
		let num_bits = usize::BITS;
		num_bits - num.leading_zeros() - 1
	} else {
		panic!("Logarithm of zero is undefined")
	}
}

pub fn previous_power_of_two(num: usize) -> usize {
	usize::pow2(log2(num))
}

#[cfg(test)]
mod test {
	use core::mem::size_of;

	use super::*;

	#[test]
	fn log2_should_return_0_when_num_is_1() {
		assert_eq!(0, log2(1));
	}

	#[test]
	fn log2_should_calculate_for_usize_max() {
		assert_eq!(8 * size_of::<usize>() as u32 - 1, log2(usize::MAX));
	}
}
