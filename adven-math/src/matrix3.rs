// adven-math <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

use num_traits::{One, Zero};

use super::{Cos, Sin, Vector2};

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Copy, Clone, Default)]
pub struct Matrix3<T> {
	pub m00: T,
	pub m01: T,
	pub m02: T,
	pub m10: T,
	pub m11: T,
	pub m12: T,
	pub m20: T,
	pub m21: T,
	pub m22: T,
}

impl<T: One + Neg<Output = T> + Zero> Matrix3<T> {
	pub fn rotation<U: Copy>(angle: U) -> Matrix3<T>
	where
		U: Cos<Output = T> + Sin<Output = T>,
	{
		Self {
			m00: U::cos(angle),
			m01: -U::sin(angle),
			m02: T::zero(),
			m10: U::sin(angle),
			m11: U::cos(angle),
			m12: T::zero(),
			m20: T::zero(),
			m21: T::zero(),
			m22: T::one(),
		}
	}

	pub fn trs<U>(translation: Vector2<T>, angle: U, scale: Vector2<T>) -> Matrix3<T>
	where
		T: Copy,
		U: Copy + Cos<Output = T> + Sin<Output = T>,
	{
		Self {
			m00: scale.x * U::cos(angle),
			m01: -scale.y * U::sin(angle),
			m02: translation.x,
			m10: scale.x * U::sin(angle),
			m11: scale.y * U::cos(angle),
			m12: translation.y,
			m20: T::zero(),
			m21: T::zero(),
			m22: T::one(),
		}
	}
}

impl<T: One + Zero> Matrix3<T> {
	pub fn identity() -> Matrix3<T> {
		Self {
			m00: T::one(),
			m01: T::zero(),
			m02: T::zero(),
			m10: T::zero(),
			m11: T::one(),
			m12: T::zero(),
			m20: T::zero(),
			m21: T::zero(),
			m22: T::one(),
		}
	}

	pub fn scale(scale: Vector2<T>) -> Matrix3<T> {
		Self {
			m00: scale.x,
			m01: T::zero(),
			m02: T::zero(),
			m10: T::zero(),
			m11: scale.y,
			m12: T::zero(),
			m20: T::zero(),
			m21: T::zero(),
			m22: T::one(),
		}
	}

	pub fn translate(translation: Vector2<T>) -> Matrix3<T> {
		Self {
			m00: T::one(),
			m01: T::zero(),
			m02: translation.x,
			m10: T::zero(),
			m11: T::one(),
			m12: translation.y,
			m20: T::zero(),
			m21: T::zero(),
			m22: T::one(),
		}
	}

	pub fn transform_point_fast(&self, vector: Vector2<T>) -> Vector2<T>
	where
		T: Copy,
	{
		Vector2 {
			x: self.m00 * vector.x + self.m01 * vector.y + self.m02,
			y: self.m11 * vector.x + self.m11 * vector.y + self.m12,
		}
	}

	pub fn transform_point(&self, vector: Vector2<T>) -> Vector2<T>
	where
		T: Copy + PartialEq,
		Vector2<T>: Div<T, Output = Vector2<T>>,
	{
		let out_vec = Vector2 {
			x: self.m00 * vector.x + self.m01 * vector.y + self.m02,
			y: self.m11 * vector.x + self.m11 * vector.y + self.m12,
		};

		let h = self.m21 * vector.x + self.m21 * vector.y + self.m22;

		if !h.is_one() {
			out_vec / h
		} else {
			out_vec
		}
	}
}

impl<T: Eq> Eq for Matrix3<T> {}

impl<T: PartialEq> PartialEq for Matrix3<T> {
	fn eq(&self, rhs: &Self) -> bool {
		self.m00 == rhs.m00
			&& self.m01 == rhs.m01
			&& self.m02 == rhs.m02
			&& self.m10 == rhs.m10
			&& self.m11 == rhs.m11
			&& self.m12 == rhs.m12
			&& self.m20 == rhs.m20
			&& self.m21 == rhs.m21
			&& self.m22 == rhs.m22
	}
}

impl<T: Add<Output = T>> Add for Matrix3<T> {
	type Output = Matrix3<T>;

	fn add(self, rhs: Self) -> Self::Output {
		Self::Output {
			m00: self.m00 + rhs.m00,
			m01: self.m01 + rhs.m01,
			m02: self.m02 + rhs.m02,
			m10: self.m10 + rhs.m10,
			m11: self.m11 + rhs.m11,
			m12: self.m12 + rhs.m12,
			m20: self.m20 + rhs.m20,
			m21: self.m21 + rhs.m21,
			m22: self.m22 + rhs.m22,
		}
	}
}

impl<T: AddAssign> AddAssign for Matrix3<T> {
	fn add_assign(&mut self, rhs: Self) {
		self.m00 += rhs.m00;
		self.m01 += rhs.m01;
		self.m02 += rhs.m02;
		self.m10 += rhs.m10;
		self.m11 += rhs.m11;
		self.m12 += rhs.m12;
		self.m20 += rhs.m20;
		self.m21 += rhs.m21;
		self.m22 += rhs.m22;
	}
}

impl<T: Copy + Div<Output = T>> Div<T> for Matrix3<T> {
	type Output = Matrix3<T>;

	fn div(self, rhs: T) -> Self::Output {
		Self::Output {
			m00: self.m00 / rhs,
			m01: self.m01 / rhs,
			m02: self.m02 / rhs,
			m10: self.m10 / rhs,
			m11: self.m11 / rhs,
			m12: self.m12 / rhs,
			m20: self.m20 / rhs,
			m21: self.m21 / rhs,
			m22: self.m22 / rhs,
		}
	}
}

impl<T: Copy + DivAssign> DivAssign<T> for Matrix3<T> {
	fn div_assign(&mut self, rhs: T) {
		self.m00 /= rhs;
		self.m01 /= rhs;
		self.m02 /= rhs;
		self.m10 /= rhs;
		self.m11 /= rhs;
		self.m12 /= rhs;
		self.m20 /= rhs;
		self.m21 /= rhs;
		self.m22 /= rhs;
	}
}

impl<T: Add<Output = T> + Copy + Mul<Output = T>> Mul for Matrix3<T> {
	type Output = Matrix3<T>;

	fn mul(self, rhs: Self) -> Self::Output {
		#[allow(clippy::suspicious_operation_groupings)]
		Self::Output {
			m00: self.m00 * rhs.m00 + self.m01 * rhs.m10 + self.m02 * rhs.m20,
			m01: self.m00 * rhs.m01 + self.m01 * rhs.m11 + self.m02 * rhs.m21,
			m02: self.m00 * rhs.m02 + self.m01 * rhs.m12 + self.m02 * rhs.m22,
			m10: self.m10 * rhs.m00 + self.m11 * rhs.m10 + self.m12 * rhs.m20,
			m11: self.m10 * rhs.m01 + self.m11 * rhs.m11 + self.m12 * rhs.m21,
			m12: self.m10 * rhs.m02 + self.m11 * rhs.m12 + self.m12 * rhs.m22,
			m20: self.m20 * rhs.m00 + self.m21 * rhs.m10 + self.m22 * rhs.m20,
			m21: self.m20 * rhs.m01 + self.m21 * rhs.m11 + self.m22 * rhs.m21,
			m22: self.m20 * rhs.m02 + self.m21 * rhs.m12 + self.m22 * rhs.m22,
		}
	}
}

impl<T: Add<Output = T> + Copy + Mul<Output = T>> MulAssign for Matrix3<T> {
	#[allow(clippy::suspicious_operation_groupings)]
	fn mul_assign(&mut self, rhs: Self) {
		self.m00 = self.m00 * rhs.m00 + self.m01 * rhs.m10 + self.m02 * rhs.m20;
		self.m01 = self.m00 * rhs.m01 + self.m01 * rhs.m11 + self.m02 * rhs.m21;
		self.m02 = self.m00 * rhs.m02 + self.m01 * rhs.m12 + self.m02 * rhs.m22;
		self.m10 = self.m10 * rhs.m00 + self.m11 * rhs.m10 + self.m12 * rhs.m20;
		self.m11 = self.m10 * rhs.m01 + self.m11 * rhs.m11 + self.m12 * rhs.m21;
		self.m12 = self.m10 * rhs.m02 + self.m11 * rhs.m12 + self.m12 * rhs.m22;
		self.m20 = self.m20 * rhs.m00 + self.m21 * rhs.m10 + self.m22 * rhs.m20;
		self.m21 = self.m20 * rhs.m01 + self.m21 * rhs.m11 + self.m22 * rhs.m21;
		self.m22 = self.m20 * rhs.m02 + self.m21 * rhs.m12 + self.m22 * rhs.m22;
	}
}

impl<T: Copy + Mul<Output = T>> Mul<T> for Matrix3<T> {
	type Output = Matrix3<T>;

	fn mul(self, rhs: T) -> Self::Output {
		Self::Output {
			m00: self.m00 * rhs,
			m01: self.m01 * rhs,
			m02: self.m02 * rhs,
			m10: self.m10 * rhs,
			m11: self.m11 * rhs,
			m12: self.m12 * rhs,
			m20: self.m20 * rhs,
			m21: self.m21 * rhs,
			m22: self.m22 * rhs,
		}
	}
}

impl<T: Copy + MulAssign> MulAssign<T> for Matrix3<T> {
	fn mul_assign(&mut self, rhs: T) {
		self.m00 *= rhs;
		self.m01 *= rhs;
		self.m02 *= rhs;
		self.m10 *= rhs;
		self.m11 *= rhs;
		self.m12 *= rhs;
		self.m20 *= rhs;
		self.m21 *= rhs;
		self.m22 *= rhs;
	}
}

impl<T: Neg<Output = T>> Neg for Matrix3<T> {
	type Output = Matrix3<T>;

	fn neg(self) -> Self::Output {
		Self::Output {
			m00: -self.m00,
			m01: -self.m01,
			m02: -self.m02,
			m10: -self.m10,
			m11: -self.m11,
			m12: -self.m12,
			m20: -self.m20,
			m21: -self.m21,
			m22: -self.m22,
		}
	}
}

impl<T: Sub<Output = T>> Sub for Matrix3<T> {
	type Output = Matrix3<T>;

	fn sub(self, rhs: Self) -> Self::Output {
		Self::Output {
			m00: self.m00 - rhs.m00,
			m01: self.m01 - rhs.m01,
			m02: self.m02 - rhs.m02,
			m10: self.m10 - rhs.m10,
			m11: self.m11 - rhs.m11,
			m12: self.m12 - rhs.m12,
			m20: self.m20 - rhs.m20,
			m21: self.m21 - rhs.m21,
			m22: self.m22 - rhs.m22,
		}
	}
}

impl<T: SubAssign> SubAssign for Matrix3<T> {
	fn sub_assign(&mut self, rhs: Self) {
		self.m00 -= rhs.m00;
		self.m01 -= rhs.m01;
		self.m02 -= rhs.m02;
		self.m10 -= rhs.m10;
		self.m11 -= rhs.m11;
		self.m12 -= rhs.m12;
		self.m20 -= rhs.m20;
		self.m21 -= rhs.m21;
		self.m22 -= rhs.m22;
	}
}

impl<T, U> az::Cast<Matrix3<U>> for Matrix3<T>
where
	T: az::Cast<U>,
{
	fn cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.cast(),
			m01: self.m01.cast(),
			m02: self.m02.cast(),
			m10: self.m10.cast(),
			m11: self.m11.cast(),
			m12: self.m12.cast(),
			m20: self.m20.cast(),
			m21: self.m21.cast(),
			m22: self.m22.cast(),
		}
	}
}

impl<T, U> az::Cast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::Cast<U>,
{
	fn cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.cast(),
			m01: self.m01.cast(),
			m02: self.m02.cast(),
			m10: self.m10.cast(),
			m11: self.m11.cast(),
			m12: self.m12.cast(),
			m20: self.m20.cast(),
			m21: self.m21.cast(),
			m22: self.m22.cast(),
		}
	}
}

impl<T, U> az::CheckedCast<Matrix3<U>> for Matrix3<T>
where
	T: az::CheckedCast<U>,
{
	fn checked_cast(self) -> Option<Matrix3<U>> {
		Some(Matrix3 {
			m00: self.m00.checked_cast()?,
			m01: self.m01.checked_cast()?,
			m02: self.m02.checked_cast()?,
			m10: self.m10.checked_cast()?,
			m11: self.m11.checked_cast()?,
			m12: self.m12.checked_cast()?,
			m20: self.m20.checked_cast()?,
			m21: self.m21.checked_cast()?,
			m22: self.m22.checked_cast()?,
		})
	}
}

impl<T, U> az::CheckedCast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::CheckedCast<U>,
{
	fn checked_cast(self) -> Option<Matrix3<U>> {
		Some(Matrix3 {
			m00: self.m00.checked_cast()?,
			m01: self.m01.checked_cast()?,
			m02: self.m02.checked_cast()?,
			m10: self.m10.checked_cast()?,
			m11: self.m11.checked_cast()?,
			m12: self.m12.checked_cast()?,
			m20: self.m20.checked_cast()?,
			m21: self.m21.checked_cast()?,
			m22: self.m22.checked_cast()?,
		})
	}
}

impl<T, U> az::OverflowingCast<Matrix3<U>> for Matrix3<T>
where
	T: az::OverflowingCast<U>,
{
	fn overflowing_cast(self) -> (Matrix3<U>, bool) {
		let m00 = self.m00.overflowing_cast();
		let m01 = self.m01.overflowing_cast();
		let m02 = self.m02.overflowing_cast();
		let m10 = self.m10.overflowing_cast();
		let m11 = self.m11.overflowing_cast();
		let m12 = self.m12.overflowing_cast();
		let m20 = self.m20.overflowing_cast();
		let m21 = self.m21.overflowing_cast();
		let m22 = self.m22.overflowing_cast();

		let overflow =
			m00.1 || m01.1 || m02.1 || m10.1 || m11.1 || m12.1 || m20.1 || m21.1 || m22.1;

		(
			Matrix3 {
				m00: m00.0,
				m01: m01.0,
				m02: m02.0,
				m10: m10.0,
				m11: m11.0,
				m12: m12.0,
				m20: m20.0,
				m21: m21.0,
				m22: m22.0,
			},
			overflow,
		)
	}
}

impl<T, U> az::OverflowingCast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::OverflowingCast<U>,
{
	fn overflowing_cast(self) -> (Matrix3<U>, bool) {
		let m00 = self.m00.overflowing_cast();
		let m01 = self.m01.overflowing_cast();
		let m02 = self.m02.overflowing_cast();
		let m10 = self.m10.overflowing_cast();
		let m11 = self.m11.overflowing_cast();
		let m12 = self.m12.overflowing_cast();
		let m20 = self.m20.overflowing_cast();
		let m21 = self.m21.overflowing_cast();
		let m22 = self.m22.overflowing_cast();

		let overflow =
			m00.1 || m01.1 || m02.1 || m10.1 || m11.1 || m12.1 || m20.1 || m21.1 || m22.1;

		(
			Matrix3 {
				m00: m00.0,
				m01: m01.0,
				m02: m02.0,
				m10: m10.0,
				m11: m11.0,
				m12: m12.0,
				m20: m20.0,
				m21: m21.0,
				m22: m22.0,
			},
			overflow,
		)
	}
}

impl<T, U> az::SaturatingCast<Matrix3<U>> for Matrix3<T>
where
	T: az::SaturatingCast<U>,
{
	fn saturating_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.saturating_cast(),
			m01: self.m01.saturating_cast(),
			m02: self.m02.saturating_cast(),
			m10: self.m10.saturating_cast(),
			m11: self.m11.saturating_cast(),
			m12: self.m12.saturating_cast(),
			m20: self.m20.saturating_cast(),
			m21: self.m21.saturating_cast(),
			m22: self.m22.saturating_cast(),
		}
	}
}

impl<T, U> az::SaturatingCast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::SaturatingCast<U>,
{
	fn saturating_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.saturating_cast(),
			m01: self.m01.saturating_cast(),
			m02: self.m02.saturating_cast(),
			m10: self.m10.saturating_cast(),
			m11: self.m11.saturating_cast(),
			m12: self.m12.saturating_cast(),
			m20: self.m20.saturating_cast(),
			m21: self.m21.saturating_cast(),
			m22: self.m22.saturating_cast(),
		}
	}
}

impl<T, U> az::UnwrappedCast<Matrix3<U>> for Matrix3<T>
where
	T: az::UnwrappedCast<U>,
{
	fn unwrapped_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.unwrapped_cast(),
			m01: self.m01.unwrapped_cast(),
			m02: self.m02.unwrapped_cast(),
			m10: self.m10.unwrapped_cast(),
			m11: self.m11.unwrapped_cast(),
			m12: self.m12.unwrapped_cast(),
			m20: self.m20.unwrapped_cast(),
			m21: self.m21.unwrapped_cast(),
			m22: self.m22.unwrapped_cast(),
		}
	}
}

impl<T, U> az::UnwrappedCast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::UnwrappedCast<U>,
{
	fn unwrapped_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.unwrapped_cast(),
			m01: self.m01.unwrapped_cast(),
			m02: self.m02.unwrapped_cast(),
			m10: self.m10.unwrapped_cast(),
			m11: self.m11.unwrapped_cast(),
			m12: self.m12.unwrapped_cast(),
			m20: self.m20.unwrapped_cast(),
			m21: self.m21.unwrapped_cast(),
			m22: self.m22.unwrapped_cast(),
		}
	}
}

impl<T, U> az::WrappingCast<Matrix3<U>> for Matrix3<T>
where
	T: az::WrappingCast<U>,
{
	fn wrapping_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.wrapping_cast(),
			m01: self.m01.wrapping_cast(),
			m02: self.m02.wrapping_cast(),
			m10: self.m10.wrapping_cast(),
			m11: self.m11.wrapping_cast(),
			m12: self.m12.wrapping_cast(),
			m20: self.m20.wrapping_cast(),
			m21: self.m21.wrapping_cast(),
			m22: self.m22.wrapping_cast(),
		}
	}
}

impl<T, U> az::WrappingCast<Matrix3<U>> for &Matrix3<T>
where
	for<'a> &'a T: az::WrappingCast<U>,
{
	fn wrapping_cast(self) -> Matrix3<U> {
		Matrix3 {
			m00: self.m00.wrapping_cast(),
			m01: self.m01.wrapping_cast(),
			m02: self.m02.wrapping_cast(),
			m10: self.m10.wrapping_cast(),
			m11: self.m11.wrapping_cast(),
			m12: self.m12.wrapping_cast(),
			m20: self.m20.wrapping_cast(),
			m21: self.m21.wrapping_cast(),
			m22: self.m22.wrapping_cast(),
		}
	}
}
