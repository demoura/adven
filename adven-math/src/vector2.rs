// adven-math <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::ops::{
	Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Rem, RemAssign, Shl, ShlAssign, Shr,
	ShrAssign, Sub, SubAssign,
};

use num_traits::{One, Zero};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
#[derive(Copy, Clone)]
pub struct Vector2<T> {
	pub x: T,
	pub y: T,
}

impl<T> Vector2<T> {
	pub fn new(x: T, y: T) -> Self {
		Vector2 { x, y }
	}

	pub fn one() -> Self
	where
		T: One,
	{
		Vector2 {
			x: T::one(),
			y: T::one(),
		}
	}
}

impl<T: Zero> Zero for Vector2<T> {
	fn is_zero(&self) -> bool {
		self.x.is_zero() && self.y.is_zero()
	}

	fn zero() -> Self {
		Vector2 {
			x: T::zero(),
			y: T::zero(),
		}
	}
}

impl<T: Eq> Eq for Vector2<T> {}

impl<T: PartialEq> PartialEq for Vector2<T> {
	fn eq(&self, other: &Self) -> bool {
		self.x == other.x && self.y == other.y
	}
}

impl<T: Add<Output = T>> Add for Vector2<T> {
	type Output = Vector2<T>;

	fn add(self, rhs: Self) -> Self::Output {
		Self::Output {
			x: self.x + rhs.x,
			y: self.y + rhs.y,
		}
	}
}

impl<T: AddAssign> AddAssign for Vector2<T> {
	fn add_assign(&mut self, rhs: Self) {
		self.x += rhs.x;
		self.y += rhs.y;
	}
}

impl<T: Copy + Div<Output = T>> Div<T> for Vector2<T> {
	type Output = Vector2<T>;

	fn div(self, rhs: T) -> Self::Output {
		Self::Output {
			x: self.x / rhs,
			y: self.y / rhs,
		}
	}
}

impl<T: Copy + DivAssign> DivAssign<T> for Vector2<T> {
	fn div_assign(&mut self, rhs: T) {
		self.x /= rhs;
		self.y /= rhs;
	}
}

impl<T: Copy + Mul<Output = T>> Mul<T> for Vector2<T> {
	type Output = Vector2<T>;

	fn mul(self, rhs: T) -> Self::Output {
		Self::Output {
			x: self.x * rhs,
			y: self.y * rhs,
		}
	}
}

impl<T: Copy + MulAssign> MulAssign<T> for Vector2<T> {
	fn mul_assign(&mut self, rhs: T) {
		self.x *= rhs;
		self.y *= rhs;
	}
}

impl<T: Neg<Output = T>> Neg for Vector2<T> {
	type Output = Vector2<T>;

	fn neg(self) -> Self::Output {
		Self::Output {
			x: -self.x,
			y: -self.y,
		}
	}
}

impl<T: Copy + Rem<Output = T>> Rem<T> for Vector2<T> {
	type Output = Vector2<T>;

	fn rem(self, rhs: T) -> Self::Output {
		Self::Output {
			x: self.x % rhs,
			y: self.y % rhs,
		}
	}
}

impl<T: Copy + RemAssign> RemAssign<T> for Vector2<T> {
	fn rem_assign(&mut self, rhs: T) {
		self.x %= rhs;
		self.y %= rhs;
	}
}

impl<T: Copy + Shl<Output = T>> Shl<T> for Vector2<T> {
	type Output = Vector2<T>;

	fn shl(self, rhs: T) -> Self::Output {
		Self::Output {
			x: self.x << rhs,
			y: self.y << rhs,
		}
	}
}

impl<T: Copy + ShlAssign> ShlAssign<T> for Vector2<T> {
	fn shl_assign(&mut self, rhs: T) {
		self.x <<= rhs;
		self.y <<= rhs;
	}
}
impl<T: Copy + Shr<Output = T>> Shr<T> for Vector2<T> {
	type Output = Vector2<T>;

	fn shr(self, rhs: T) -> Self::Output {
		Self::Output {
			x: self.x >> rhs,
			y: self.y >> rhs,
		}
	}
}

impl<T: Copy + ShrAssign> ShrAssign<T> for Vector2<T> {
	fn shr_assign(&mut self, rhs: T) {
		self.x >>= rhs;
		self.y >>= rhs;
	}
}

impl<T: Sub<Output = T>> Sub for Vector2<T> {
	type Output = Vector2<T>;

	fn sub(self, rhs: Self) -> Self::Output {
		Self::Output {
			x: self.x - rhs.x,
			y: self.y - rhs.y,
		}
	}
}

impl<T: SubAssign> SubAssign for Vector2<T> {
	fn sub_assign(&mut self, rhs: Self) {
		self.x -= rhs.x;
		self.y -= rhs.y;
	}
}
