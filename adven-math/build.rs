// adven-math <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{env, fs::File, io::Write, path::Path};

use fixed::types::I2F30;

fn main() {
	let out_dir = env::var("OUT_DIR").unwrap();
	let mut cos_lut = File::create(&Path::new(&out_dir).join("cos_lut"))
		.expect("Failed to create output cos_lut file");

	let mut sin_lut = File::create(&Path::new(&out_dir).join("sin_lut"))
		.expect("Failed to create output sin_lut file");

	// One extra for interpolation
	for i in 0..=513 {
		let angle = i as f32 * std::f32::consts::PI / 256f32;
		let cos = I2F30::from_num(f32::cos(angle));

		cos_lut
			.write_all(&cos.to_le_bytes())
			.expect("Should write cosines to file");

		let sin = I2F30::from_num(f32::sin(angle));

		sin_lut
			.write_all(&sin.to_le_bytes())
			.expect("Should write sines to file");
	}

	println!("cargo:rerun-if-changed=build.rs");
}
