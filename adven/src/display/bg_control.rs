// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::{
	convert::{From, TryInto},
	marker::PhantomData,
	ops::Range,
};

use bitvec::prelude::*;

use super::{
	AffineRenderMode, ColorMode, FlipMode, OutOfRange, PaletteBankIdx, Priority, RenderMode,
};

pub trait BackgroundSize: Into<u8> {}

#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum AffineBackgroundSize {
	Square16x16 = 0,
	Square32x32 = 1,
	Square64x64 = 2,
	Square128x128 = 3,
}

impl From<AffineBackgroundSize> for u8 {
	fn from(size: AffineBackgroundSize) -> u8 {
		size as u8
	}
}

impl BackgroundSize for AffineBackgroundSize {}

#[repr(u8)]
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum RegularBackgroundSize {
	Square32x32 = 0,
	Wide64x32 = 1,
	Tall32x64 = 2,
	Square64x64 = 3,
}

impl From<RegularBackgroundSize> for u8 {
	fn from(size: RegularBackgroundSize) -> u8 {
		size as u8
	}
}

impl BackgroundSize for RegularBackgroundSize {}

#[derive(Copy, Clone, Default, Eq, PartialEq)]
#[repr(transparent)]
pub struct BackgroundControlRegister(u16);

#[derive(Copy, Clone, Default, Eq, PartialEq)]
#[repr(transparent)]
pub struct BackgroundOffsetRegister([u16; 2]);

#[derive(Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct BackgroundControl<R: RenderMode>(BitArray<u16, Lsb0>, PhantomData<R>);

impl<R: RenderMode> BackgroundControl<R> {
	pub const TILE_BASE_BLOCK_MAX: usize = 3;
	pub const SCREEN_BASE_BLOCK_MAX: usize = 31;

	const PRIORITY: Range<usize> = 0..2;
	const TILE_BASE_BLOCK: Range<usize> = 2..4;
	const MOSAIC: usize = 6;
	const COLOR_MODE: usize = 7;
	const SCREEN_BASE_BLOCK: Range<usize> = 8..13;
	const AFFINE_WRAPPING: usize = 13;
	const SIZE: Range<usize> = 14..16;

	pub(crate) fn from_register(register: &BackgroundControlRegister) -> &Self {
		unsafe {
			core::mem::transmute::<&BackgroundControlRegister, &BackgroundControl<R>>(register)
		}
		// &BackgroundControl(BitArray::from(register.0), PhantomData::<R>)
	}

	pub fn get_is_mosaic(&self) -> bool {
		self.0[Self::MOSAIC]
	}

	pub fn get_priority(&self) -> Priority {
		self.0[Self::PRIORITY]
			.load::<u8>()
			.try_into()
			.expect("Shouldn't be possible to be out of range.")
	}

	pub fn new<S: BackgroundSize>(
		tile_base_block: usize,
		screen_base_block: usize,
		color_mode: ColorMode,
		size: S,
	) -> Result<Self, OutOfRange> {
		if tile_base_block > Self::TILE_BASE_BLOCK_MAX {
			return Err(OutOfRange::new(
				tile_base_block,
				0..Self::TILE_BASE_BLOCK_MAX + 1,
			));
		}
		if screen_base_block > Self::SCREEN_BASE_BLOCK_MAX {
			return Err(OutOfRange::new(
				screen_base_block,
				0..Self::SCREEN_BASE_BLOCK_MAX + 1,
			));
		}

		let mut info = BitArray::ZERO;

		info[Self::TILE_BASE_BLOCK].store(tile_base_block);
		info.set(Self::COLOR_MODE, color_mode as u8 == 1);
		info[Self::SCREEN_BASE_BLOCK].store(screen_base_block);
		info[Self::SIZE].store::<u8>(size.into());

		Ok(Self(info, PhantomData::<R>))
	}

	pub fn set_is_mosaic(&mut self, is_mosaic: bool) {
		self.0.set(Self::MOSAIC, is_mosaic)
	}

	pub fn set_priority(&mut self, priority: Priority) {
		self.0[Self::PRIORITY].store(priority as u8);
	}
}

impl BackgroundControl<AffineRenderMode> {
	pub fn set_affine_wrapping(&mut self, wrap: bool) {
		self.0.set(Self::AFFINE_WRAPPING, wrap)
	}
}

#[derive(Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct ScreenEntry(BitArray<u16, Lsb0>);

impl ScreenEntry {
	pub const TILE_ID_MAX: usize = 511;
	const TILE_ID: Range<usize> = 0..10;
	const HFLIP: usize = 10;
	const VFLIP: usize = 11;
	const PALETTE: Range<usize> = 12..16;

	pub fn new(tile_id: usize) -> Result<Self, OutOfRange> {
		if tile_id > Self::TILE_ID_MAX {
			return Err(OutOfRange::new(tile_id, 0..Self::TILE_ID_MAX + 1));
		}

		let mut se = BitArray::ZERO;
		se[Self::TILE_ID].store(tile_id);

		Ok(ScreenEntry(se))
	}

	pub fn with_flip(mut self, flip_mode: FlipMode) -> Self {
		self.0.set(Self::HFLIP, flip_mode.flip_horizontal());
		self.0.set(Self::VFLIP, flip_mode.flip_vertical());
		self
	}

	pub fn with_palette_bank(mut self, index: PaletteBankIdx) -> Self {
		self.0[Self::PALETTE].store(u8::from(index));
		self
	}
}

// pub struct Tileset8bpp(pub &'static [Tile8bpp]);

// pub struct TilemapBlock(pub [ScreenEntry; 32 * 32]);

// pub struct TilemapAsset<'a> {
// 	tile_blocks: SliceRef<'a, TilemapBlock>,
// 	tileset: SliceRef<'a, Tile8bpp>,
// 	color_mode: ColorMode,
// }
