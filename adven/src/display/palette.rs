// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#[cfg(feature = "repr_writable")]
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};

use super::OutOfRange;
use crate::color::BGR5;

#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Palette4bppIndex(u8);

impl Palette4bppIndex {
	pub const MIN: usize = 0;
	pub const MAX: usize = 15;
}

impl TryFrom<u8> for Palette4bppIndex {
	type Error = OutOfRange;

	fn try_from(value: u8) -> Result<Self, Self::Error> {
		if value > Self::MAX as u8 {
			return Err(OutOfRange::new(value as usize, Self::MIN..Self::MAX));
		}

		Ok(Self(value))
	}
}

impl From<Palette4bppIndex> for u8 {
	fn from(value: Palette4bppIndex) -> Self {
		value.0
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for Palette4bppIndex {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		u8::align(abi)
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[derive(Copy, Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Palette8bppIndex(u8);

impl From<u8> for Palette8bppIndex {
	fn from(value: u8) -> Self {
		Self(value)
	}
}

impl From<Palette8bppIndex> for u8 {
	fn from(value: Palette8bppIndex) -> u8 {
		value.0
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for Palette8bppIndex {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		u8::align(abi)
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		self.0.write(writer)
	}
}

#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[derive(Clone)]
pub struct PaletteBank(pub [BGR5; 256]);

impl PaletteBank {
	pub fn bank(&self, index: PaletteBankIdx) -> &[BGR5] {
		let index = usize::from(index) * 16;
		&self.0[index..index + 16]
	}

	pub fn bank_mut(&mut self, index: PaletteBankIdx) -> &mut [BGR5] {
		let index = usize::from(index) * 16;
		&mut self.0[index..index + 15]
	}
}

#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[derive(Copy, Clone, Eq, PartialEq)]
pub struct PaletteBankIdx(u8);

impl PaletteBankIdx {
	pub const MAX: usize = 15;

	pub fn new(index: usize) -> Result<Self, OutOfRange> {
		if index <= Self::MAX {
			Ok(Self(index as u8))
		} else {
			Err(OutOfRange::new(index, 0..Self::MAX + 1))
		}
	}
}

impl From<PaletteBankIdx> for usize {
	fn from(idx: PaletteBankIdx) -> usize {
		idx.0.into()
	}
}

impl From<PaletteBankIdx> for u8 {
	fn from(idx: PaletteBankIdx) -> u8 {
		idx.0
	}
}

#[repr(transparent)]
#[derive(Clone)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
pub struct PaletteAsset<const N: usize>(pub [BGR5; N]);

#[cfg(feature = "repr_writable")]
impl<const N: usize> ReprWritable for PaletteAsset<N> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[BGR5::align(abi)].iter().max().unwrap()
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		let seq = writer.start_array()?;
		for color in self.0 {
			seq.write_element(&color)?;
		}
		seq.end()
	}
}
