// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::ops::Range;

use adven_math::Vector2;
use bitvec::{field::BitField, order::Lsb0, prelude::BitArray};
use repr_writer::SliceRef;
#[cfg(feature = "repr_writable")]
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};
#[cfg(feature = "serde_derive")]
use serde::{Deserialize, Serialize};

use super::ColorMode;

pub trait Tile {
	/// Tile width in pixels
	const WIDTH: usize;

	/// Tile height in pixels
	const HEIGHT: usize;

	/// Amount of bits used to store a pixel
	const BITS_PER_PIXEL: usize;

	fn get_pixel(&self, index: Vector2<usize>) -> u8;

	// TODO: Review handling of out of range values
	fn set_pixel(&mut self, index: Vector2<usize>, value: u8);
}

#[repr(u16)]
#[derive(Clone, Copy)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
pub enum TilesRef<'a> {
	Color4Bit(SliceRef<'a, Tile4bpp>),
	Color8Bit(SliceRef<'a, Tile8bpp>),
}

impl TilesRef<'_> {
	pub fn color_mode(&self) -> ColorMode {
		match self {
			Self::Color4Bit(_) => ColorMode::Color4Bit,
			Self::Color8Bit(_) => ColorMode::Color8Bit,
		}
	}

	pub fn len(&self) -> usize {
		match self {
			Self::Color4Bit(tiles) => tiles.len(),
			Self::Color8Bit(tiles) => tiles.len(),
		}
	}

	pub fn is_empty(&self) -> bool {
		self.len() == 0
	}
}

#[cfg(feature = "repr_writable")]
impl<'a> ReprWritable for TilesRef<'a> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[
			// Enum tag is part of the struct with repr(u16)
			abi.align_of_u16,
			SliceRef::<'a, Tile4bpp>::align(abi),
			SliceRef::<'a, Tile8bpp>::align(abi),
		]
		.iter()
		.max()
		.unwrap()
	}

	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;

		match self {
			TilesRef::Color4Bit(tiles) => {
				seq.write_element(&0u16)?;
				seq.write_element(tiles)?;
			}
			TilesRef::Color8Bit(tiles) => {
				seq.write_element(&1u16)?;
				seq.write_element(tiles)?;
			}
		}

		seq.end()
	}
}

// NOTE: BitArray not used directly in the struct, because for
// some reason it breaks the copies even though it's repr transparent.
//
// TODO: Should this type be Copy? (It is used in a union)
#[repr(transparent)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[cfg_attr(feature = "serde_derive", derive(Deserialize, Serialize))]
#[derive(Clone, Copy, Default, Eq, PartialEq)]
pub struct Tile4bpp([u16; 16]);

// Align must be 2 (for VRAM writing)
static_assertions::assert_eq_align!(u16, Tile4bpp);
// Size must be 32 bytes
static_assertions::assert_eq_size!([u8; 32], Tile4bpp);

impl Tile for Tile4bpp {
	const WIDTH: usize = 8;
	const HEIGHT: usize = 8;
	const BITS_PER_PIXEL: usize = 4;

	fn get_pixel(&self, index: Vector2<usize>) -> u8 {
		BitArray::<_, Lsb0>::new(self.0)[bit_range_for_index::<Self>(index)].load()
	}

	fn set_pixel(&mut self, index: Vector2<usize>, value: u8) {
		let mut array = BitArray::<_, Lsb0>::new(self.0);

		array[bit_range_for_index::<Self>(index)].store(value);

		self.0 = array.into_inner();
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for Tile4bpp {
	fn align(abi: &repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		abi.align_of_u16
	}

	fn align_dyn(&self, abi: &repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn repr_writer::ReprWriter) -> repr_writer::Result<()> {
		let seq = writer.start_array()?;

		for value in self.0 {
			seq.write_element(&value)?;
		}

		seq.end()
	}
}

// TODO: Should this type be Copy? (It is used in a union)
#[repr(transparent)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[cfg_attr(feature = "serde_derive", derive(Deserialize, Serialize))]
#[derive(Clone, Copy, Default, Eq, PartialEq)]
pub struct Tile8bpp([u16; 32]);
/* pub struct Tile8bpp(pub BitArray<[u16; 32]>); */

// Align must be 2 (for VRAM writing)
static_assertions::assert_eq_align!(u16, Tile8bpp);
// Size must be 64 bytes
static_assertions::assert_eq_size!([u8; 64], Tile8bpp);

impl Tile for Tile8bpp {
	const WIDTH: usize = 8;
	const HEIGHT: usize = 8;
	const BITS_PER_PIXEL: usize = 8;

	/* fn get_pixel(&self, index: Vector2<usize>) -> u8 { */
	/* 	self.0[bit_range_for_index::<Self>(index)].load() */
	/* } */

	/* fn set_pixel(&mut self, index: Vector2<usize>, value: u8) { */
	/* 	self.0[bit_range_for_index::<Self>(index)].store(value) */
	/* } */

	fn get_pixel(&self, index: Vector2<usize>) -> u8 {
		let flat_index = index.x / 2 + index.y * 4;
		let is_even = index.x & 1 == 0;

		if is_even {
			// Pixel where x is 0, 2, 4, 6, etc
			self.0[flat_index] as u8 // Get low byte
		} else {
			// Pixel where x is 1, 3, 5, 6, etc
			(self.0[flat_index] >> u8::BITS) as u8 // Get high byte
		}
	}

	fn set_pixel(&mut self, index: Vector2<usize>, value: u8) {
		let flat_index = index.x / 2 + index.y * 4;
		let is_even = index.x & 1 == 0;

		if is_even {
			// Pixel where x is 0, 2, 4, 6, etc
			self.0[flat_index] &= (u8::MAX as u16) << u8::BITS; // Keep only high bit
			self.0[flat_index] |= value as u16; // Write low bit
		} else {
			// Pixel where x is 1, 3, 5, 6, etc
			self.0[flat_index] &= u8::MAX as u16; // Keep only low bit
			self.0[flat_index] |= (value as u16) << u8::BITS; // Write high bit
		}
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for Tile8bpp {
	fn align(abi: &repr_writer::ReprAbi) -> usize
	where
		Self: Sized,
	{
		abi.align_of_u16
	}

	fn align_dyn(&self, abi: &repr_writer::ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn repr_writer::ReprWriter) -> repr_writer::Result<()> {
		let seq = writer.start_array()?;

		for value in self.0 {
			seq.write_element(&value)?;
		}

		seq.end()
	}
}

fn bit_range_for_index<T: Tile>(index: Vector2<usize>) -> Range<usize> {
	let start = index.x * T::BITS_PER_PIXEL + index.y * T::WIDTH * T::BITS_PER_PIXEL;

	let end = start + T::BITS_PER_PIXEL;

	Range { start, end }
}
