// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::{marker::PhantomData, ops::Range};

use adven_math::{busize, Vector2};
use bitvec::prelude::*;
#[cfg(feature = "repr_writable")]
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};
#[cfg(feature = "serde_derive")]
use serde::{Deserialize, Serialize};

use super::{
	AffineRenderMode, ColorMode, FlipMode, OutOfRange, Priority, RegularRenderMode, RenderMode,
	TilesRef,
};
use crate::math::{fixed::types::I8F8, Matrix3};

#[repr(C)]
#[derive(Clone, Copy)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
pub struct SpriteAsset<'a> {
	pub shape_size: SpriteShapeSize,
	pub frame_count: u16,
	pub tiles: TilesRef<'a>,
}

#[cfg(feature = "repr_writable")]
impl<'a> ReprWritable for SpriteAsset<'a> {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		*[
			SpriteShapeSize::align(abi),
			u16::align(abi),
			TilesRef::<'_>::align(abi),
		]
		.iter()
		.max()
		.unwrap()
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		let align = Self::align(writer.abi());
		let seq = writer.start_struct(align)?;
		seq.write_element(&self.shape_size)?;
		seq.write_element(&self.frame_count)?;
		seq.write_element(&self.tiles)?;
		seq.end()
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for ColorMode {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		u8::align(abi)
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		writer.write_u8(*self as u8)
	}
}

#[repr(u8)]
#[cfg_attr(feature = "debug_trait", derive(Debug))]
#[cfg_attr(feature = "serde_derive", derive(Serialize, Deserialize))]
#[derive(Copy, Clone)]
pub enum SpriteShapeSize {
	Square8x8 = 0b0000,
	Square16x16 = 0b0001,
	Square32x32 = 0b0010,
	Square64x64 = 0b0011,
	Wide16x8 = 0b0100,
	Wide32x8 = 0b0101,
	Wide32x16 = 0b0110,
	Wide64x32 = 0b0111,
	Tall8x16 = 0b1000,
	Tall8x32 = 0b1001,
	Tall16x32 = 0b1010,
	Tall32x64 = 0b1011,
}

impl SpriteShapeSize {
	pub fn height(self) -> u16 {
		match self {
			SpriteShapeSize::Square8x8 | SpriteShapeSize::Wide16x8 | SpriteShapeSize::Wide32x8 => 8,

			SpriteShapeSize::Square16x16
			| SpriteShapeSize::Wide32x16
			| SpriteShapeSize::Tall8x16 => 16,

			SpriteShapeSize::Square32x32
			| SpriteShapeSize::Wide64x32
			| SpriteShapeSize::Tall8x32
			| SpriteShapeSize::Tall16x32 => 32,

			SpriteShapeSize::Square64x64 | SpriteShapeSize::Tall32x64 => 64,
		}
	}

	pub fn pixel_count(self) -> u16 {
		match self {
			SpriteShapeSize::Square8x8 => 64,

			SpriteShapeSize::Wide16x8 | SpriteShapeSize::Tall8x16 => 128,

			SpriteShapeSize::Square16x16
			| SpriteShapeSize::Wide32x8
			| SpriteShapeSize::Tall8x32 => 256,

			SpriteShapeSize::Wide32x16 | SpriteShapeSize::Tall16x32 => 512,

			SpriteShapeSize::Square32x32 => 1024,

			SpriteShapeSize::Wide64x32 | SpriteShapeSize::Tall32x64 => 2048,

			SpriteShapeSize::Square64x64 => 4096,
		}
	}

	pub fn width(self) -> u16 {
		match self {
			SpriteShapeSize::Square8x8 | SpriteShapeSize::Tall8x16 | SpriteShapeSize::Tall8x32 => 8,

			SpriteShapeSize::Square16x16
			| SpriteShapeSize::Wide16x8
			| SpriteShapeSize::Tall16x32 => 16,

			SpriteShapeSize::Square32x32
			| SpriteShapeSize::Wide32x8
			| SpriteShapeSize::Wide32x16
			| SpriteShapeSize::Tall32x64 => 32,

			SpriteShapeSize::Square64x64 | SpriteShapeSize::Wide64x32 => 64,
		}
	}

	fn shape(self) -> u8 {
		((self as u8) & 0b1100) >> 2
	}

	fn size(self) -> u8 {
		(self as u8) & 0b11
	}
}

#[cfg(feature = "repr_writable")]
impl ReprWritable for SpriteShapeSize {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		u8::align(abi)
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		writer.write_u8(*self as u8)
	}
}

#[derive(Copy, Clone)]
#[repr(u8)]
pub enum RegularSpriteMode {
	Regular = 0,
	Hide = 2,
}

#[derive(Copy, Clone)]
#[repr(u8)]
pub enum AffineSpriteMode {
	Affine = 1,
	Hide = 2,
	AffineDouble = 3,
}

#[derive(Copy, Clone)]
#[repr(u8)]
pub enum GraphicsMode {
	Normal = 0,
	AlphaBlending = 1,
	Window = 2,
}

#[derive(Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct SpriteInfo<R: RenderMode>(BitArray<[u16; 3], Lsb0>, PhantomData<R>);

impl<R: RenderMode> SpriteInfo<R> {
	pub const TILE_ID_MAX: usize = 1023;
	pub const AFFINE_ID_MAX: usize = 31;
	const POS_Y: Range<usize> = 0..8;
	const SPRITE_MODE: Range<usize> = 8..10;
	const GRAPHICS_MODE: Range<usize> = 10..12;
	const ENABLE_MOSAIC: usize = 12;
	const COLOR_MODE: usize = 13;
	const SPRITE_SHAPE: Range<usize> = 14..16;
	const POS_X: Range<usize> = 16..25;
	const AFFINE_ID: Range<usize> = 25..30;
	const HFLIP: usize = 28;
	const VFLIP: usize = 29;
	const SPRITE_SIZE: Range<usize> = 30..32;
	const BASE_TILE_ID: Range<usize> = 32..42;
	const PRIORITY: Range<usize> = 42..44;
	const PALETTE_BANK: Range<usize> = 44..48;

	fn into_raw(self) -> [u16; 3] {
		self.0.into_inner()
	}

	pub fn set_enable_mosaic(mut self, enable_mosaic: bool) -> Self {
		self.0.set(Self::ENABLE_MOSAIC, enable_mosaic);
		self
	}

	pub fn set_graphics_mode(mut self, gfx_mode: GraphicsMode) -> Self {
		self.0[Self::GRAPHICS_MODE].store(gfx_mode as u8);
		self
	}

	pub fn set_palette_bank(mut self, index: busize<0, 15>) -> Self {
		self.0[Self::PALETTE_BANK].store(index.get());
		self
	}

	pub fn set_position(mut self, pos: Vector2<u16>) -> Self {
		self.0[Self::POS_X].store(pos.x);
		self.0[Self::POS_Y].store(pos.y);
		self
	}

	pub fn set_priority(mut self, priority: Priority) -> Self {
		self.0[Self::PRIORITY].store(priority as u8);
		self
	}
}

impl SpriteInfo<RegularRenderMode> {
	pub fn new_regular(
		base_tile_id: usize,
		sprite_size: SpriteShapeSize,
		color_mode: ColorMode,
	) -> Result<Self, OutOfRange> {
		if base_tile_id <= Self::TILE_ID_MAX {
			let mut info = BitArray::ZERO;

			info[Self::BASE_TILE_ID].store(base_tile_id);
			info.set(Self::COLOR_MODE, color_mode as u8 == 1);
			info[Self::SPRITE_SHAPE].store(sprite_size.shape());
			info[Self::SPRITE_SIZE].store(sprite_size.size());

			Ok(Self(info, PhantomData::<RegularRenderMode>))
		} else {
			Err(OutOfRange::new(base_tile_id, 0..Self::TILE_ID_MAX + 1))
		}
	}

	pub fn new_hidden() -> Self {
		let mut info = BitArray::ZERO;

		info[Self::SPRITE_MODE].store(RegularSpriteMode::Hide as u8);

		Self(info, PhantomData::<RegularRenderMode>)
	}

	unsafe fn from_raw(data: [u16; 3]) -> Option<Self> {
		let info = BitArray::new(data);

		if info[Self::SPRITE_MODE].any() {
			None
		} else {
			Some(SpriteInfo(info, PhantomData::<RegularRenderMode>))
		}
	}

	pub fn set_flip_mode(mut self, flip_mode: FlipMode) -> Self {
		self.0.set(Self::HFLIP, flip_mode.flip_horizontal());
		self.0.set(Self::VFLIP, flip_mode.flip_vertical());
		self
	}

	pub fn set_is_hidden(mut self, is_hidden: bool) -> Self {
		self.0[Self::SPRITE_MODE].store(if is_hidden {
			RegularSpriteMode::Hide as u8
		} else {
			RegularSpriteMode::Regular as u8
		});
		self
	}
}

impl SpriteInfo<AffineRenderMode> {
	pub fn new_affine(
		base_tile_id: usize,
		sprite_size: SpriteShapeSize,
		color_mode: ColorMode,
		affine_id: usize,
	) -> Result<Self, OutOfRange<usize>> {
		if base_tile_id > Self::TILE_ID_MAX {
			return Err(OutOfRange::new(base_tile_id, 0..Self::TILE_ID_MAX + 1));
		}
		if affine_id > Self::AFFINE_ID_MAX {
			return Err(OutOfRange::new(affine_id, 0..Self::AFFINE_ID_MAX + 1));
		}

		let mut info = BitArray::ZERO;

		info[Self::AFFINE_ID].store(affine_id);
		info[Self::BASE_TILE_ID].store(base_tile_id);
		info.set(Self::COLOR_MODE, color_mode as u8 == 1);
		info[Self::SPRITE_MODE].store(AffineSpriteMode::Affine as u8);
		info[Self::SPRITE_SHAPE].store(sprite_size.shape());
		info[Self::SPRITE_SIZE].store(sprite_size.size());

		Ok(Self(info, PhantomData::<AffineRenderMode>))
	}

	unsafe fn from_raw(data: [u16; 3]) -> Option<Self> {
		let info = BitArray::new(data);

		if info[Self::SPRITE_MODE].any() {
			Some(SpriteInfo(info, PhantomData::<AffineRenderMode>))
		} else {
			None
		}
	}
}

pub trait SpriteControl {
	fn affine_matrix(&mut self, index: busize<0, 31>) -> Matrix3<I8F8>;

	fn affine_sprite_info(&self, index: busize<0, 127>) -> Option<SpriteInfo<AffineRenderMode>>;

	fn regular_sprite_info(&self, index: busize<0, 127>) -> Option<SpriteInfo<RegularRenderMode>>;

	fn set_sprite_info<R: RenderMode>(&mut self, index: busize<0, 127>, info: SpriteInfo<R>);

	fn set_affine_matrix(&mut self, index: busize<0, 31>, matrix: &Matrix3<I8F8>);
}

pub mod gba {
	use volatile::Volatile;

	use super::{AffineRenderMode, RegularRenderMode, RenderMode, SpriteControl, SpriteInfo};
	use crate::math::{busize, fixed::types::I8F8, Matrix3};

	#[repr(C)]
	#[derive(Copy, Clone)]
	pub struct SpriteAttr {
		sprite_info: [u16; 3],
		affine_term: I8F8,
	}

	pub struct GbaSpriteControl {
		sprite_attrs: Volatile<&'static mut [SpriteAttr; 128]>,
	}

	impl GbaSpriteControl {
		pub fn new(sprite_attrs: Volatile<&'static mut [SpriteAttr; 128]>) -> Self {
			Self { sprite_attrs }
		}
	}

	impl SpriteControl for GbaSpriteControl {
		fn affine_matrix(&mut self, index: busize<0, 31>) -> Matrix3<I8F8> {
			let mut matrix = Matrix3::default();
			let index = index.get() * 4;

			matrix.m00 = self
				.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index].affine_term)
				.read();

			matrix.m01 = self
				.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 1].affine_term)
				.read();

			matrix.m10 = self
				.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 2].affine_term)
				.read();

			matrix.m11 = self
				.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 3].affine_term)
				.read();

			matrix
		}

		fn affine_sprite_info(
			&self,
			index: busize<0, 127>,
		) -> Option<SpriteInfo<AffineRenderMode>> {
			let sprite_attr: [u16; 3] = self
				.sprite_attrs
				.map(|sprite_attrs| &sprite_attrs[index.get()].sprite_info)
				.read();

			unsafe { SpriteInfo::<AffineRenderMode>::from_raw(sprite_attr) }
		}

		fn regular_sprite_info(
			&self,
			index: busize<0, 127>,
		) -> Option<SpriteInfo<RegularRenderMode>> {
			let sprite_attr: [u16; 3] = self
				.sprite_attrs
				.map(|sprite_attrs| &sprite_attrs[index.get()].sprite_info)
				.read();

			unsafe { SpriteInfo::<RegularRenderMode>::from_raw(sprite_attr) }
		}

		fn set_sprite_info<R: RenderMode>(&mut self, index: busize<0, 127>, info: SpriteInfo<R>) {
			self.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index.get()].sprite_info)
				.write(info.into_raw());
		}

		fn set_affine_matrix(&mut self, index: busize<0, 31>, matrix: &Matrix3<I8F8>) {
			let index = index.get() * 4;
			self.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index].affine_term)
				.write(matrix.m00);

			self.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 1].affine_term)
				.write(matrix.m01);

			self.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 2].affine_term)
				.write(matrix.m10);

			self.sprite_attrs
				.map_mut(|sprite_attrs| &mut sprite_attrs[index + 3].affine_term)
				.write(matrix.m11);
		}
	}
}
