// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// use fixed::types::I8F8;

// pub struct AffineMatrix([I8F8; 4]);

// impl AffineMatrix
// {
//     pub fn identity() -> Self
//     {
//         Self([
//             I8F8::from_bits(256), I8F8::from_bits(0),
//             I8F8::from_bits(0), I8F8::from_bits(256)])
//     }
// }

// impl Default for AffineMatrix
// {
//     fn default() -> Self
//     {
//         Self::identity()
//     }
// }
