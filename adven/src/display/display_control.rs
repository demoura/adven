// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::marker::PhantomData;

use modular_bitfield::prelude::*;
use volatile::{access::*, Volatile};

use super::*;

#[derive(BitfieldSpecifier)]
pub enum VideoModes {
	Tiled4Regular = 0,
	Tiled2Regular1Affine = 1,
	Tiled2Affine = 2,
	BitmapSingleFullColor = 3,
	BitmapPagedPaletted = 4,
	BitmapPagedFullColor = 5,
	_Invalid = 6,
	_Invalid2 = 7,
}

#[derive(BitfieldSpecifier)]
pub enum SpriteMappingMode {
	TwoDimensional = 0,
	OneDimensional = 1,
}
#[bitfield]
#[derive(Copy, Clone, Default)]
pub struct DisplayControlRegister {
	#[bits = 3]
	#[allow(dead_code)] // FIXME
	video_mode: VideoModes,
	#[allow(dead_code)] // FIXME
	cgb_mode: bool,
	page_select: bool,
	#[allow(dead_code)] // FIXME
	hblank_oam_acesss: bool,
	#[bits = 1]
	sprite_mapping: SpriteMappingMode,
	#[allow(dead_code)] // FIXME
	forced_blank: bool,
	enable_background0: bool,
	enable_background1: bool,
	enable_background2: bool,
	enable_background3: bool,
	enable_sprites: bool,
	#[allow(dead_code)] // FIXME
	enable_window1: bool,
	#[allow(dead_code)] // FIXME
	enable_window2: bool,
	#[allow(dead_code)] // FIXME
	enable_sprite_window: bool,
}

pub trait VideoMode {
	type VRAM;

	fn video_mode() -> VideoModes;
}
pub trait TiledVideoMode: VideoMode {}
pub trait BitmapVideoMode: VideoMode {}
pub trait PagedVideoMode: VideoMode {}

pub struct Tiled4Regular {}
/**< Mode 0: Tiled mode, 4 regular backgrounds */

impl VideoMode for Tiled4Regular {
	type VRAM = TiledVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::Tiled4Regular
	}
}

impl TiledVideoMode for Tiled4Regular {}

pub struct Tiled2Regular1Affine;
/**< Mode 1: Tiled mode, 3 regular backgrounds and 1 affine */

impl VideoMode for Tiled2Regular1Affine {
	type VRAM = TiledVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::Tiled2Regular1Affine
	}
}

impl TiledVideoMode for Tiled2Regular1Affine {}

pub struct Tiled2Affine;
/**< Mode 2: Tiled mode, 2 regular backgrounds and 2 affine */

impl VideoMode for Tiled2Affine {
	type VRAM = TiledVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::Tiled2Affine
	}
}

impl TiledVideoMode for Tiled2Affine {}

pub struct BitmapSingleFullColor;
/**< Mode 3: Bitmap mode, VRAM is mapped as a single 240x160 16bpp image.*/
impl VideoMode for BitmapSingleFullColor {
	type VRAM = BitmapSingleFullColorVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::BitmapSingleFullColor
	}
}
impl BitmapVideoMode for BitmapSingleFullColor {}

pub struct BitmapPagedPaletted;
/**< Mode 4: Bitmap mode, VRAM is mapped as two pages, each 240x160 8bpp paletted (256 colors) images. */
impl VideoMode for BitmapPagedPaletted {
	type VRAM = BitmapPagedPalettedVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::BitmapPagedPaletted
	}
}
impl BitmapVideoMode for BitmapPagedPaletted {}
impl PagedVideoMode for BitmapPagedPaletted {}

pub struct BitmapPagedFullColor;
/**< Mode 5: Bitmap mode, VRAM is mapped as two pages, each 160x128 16bpp images */
impl VideoMode for BitmapPagedFullColor {
	type VRAM = BitmapPagedFullColorVRAM;

	fn video_mode() -> VideoModes {
		VideoModes::BitmapPagedFullColor
	}
}
impl BitmapVideoMode for BitmapPagedFullColor {}
impl PagedVideoMode for BitmapPagedFullColor {}

#[repr(u32)]
pub enum Tiled4RegularBackgrounds {
	Regular0 = 0,
	Regular1 = 1,
	Regular2 = 2,
	Regular3 = 3,
}

#[repr(u32)]
pub enum Tiled2Regular1AffineBackgrounds {
	Regular0 = 0,
	Regular1 = 1,
	Affine = 2,
}
#[repr(u32)]
pub enum Tiled2AffineBackgrounds {
	Affine0 = 2,
	Affine1 = 3,
}

#[repr(u32)]
pub enum BackgroundIds {
	Bg0 = 0,
	Bg1 = 1,
	Bg2 = 2,
	Bg3 = 3,
}

pub trait BackgroundId<V: VideoMode, R: RenderMode> {
	fn background_id() -> BackgroundIds;
}

pub struct Background0;
impl BackgroundId<Tiled4Regular, RegularRenderMode> for Background0 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg0
	}
}
impl BackgroundId<Tiled2Regular1Affine, RegularRenderMode> for Background0 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg0
	}
}

pub struct Background1;
impl BackgroundId<Tiled4Regular, RegularRenderMode> for Background1 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg1
	}
}
impl BackgroundId<Tiled2Regular1Affine, RegularRenderMode> for Background1 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg1
	}
}

pub struct Background2;
impl BackgroundId<Tiled4Regular, RegularRenderMode> for Background2 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg2
	}
}
impl BackgroundId<Tiled2Regular1Affine, AffineRenderMode> for Background2 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg2
	}
}
impl BackgroundId<Tiled2Affine, AffineRenderMode> for Background2 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg2
	}
}
impl<V: BitmapVideoMode> BackgroundId<V, RegularRenderMode> for Background2 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg2
	}
}

pub struct Background3;
impl BackgroundId<Tiled4Regular, RegularRenderMode> for Background3 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg3
	}
}
impl BackgroundId<Tiled2Affine, AffineRenderMode> for Background3 {
	fn background_id() -> BackgroundIds {
		BackgroundIds::Bg3
	}
}

pub struct DisplayRegisters {
	pub display_control: Volatile<&'static mut DisplayControlRegister>,
	pub vertical_count: Volatile<&'static VerticalCountRegister, ReadOnly>,
	pub background0_control: Volatile<&'static mut BackgroundControlRegister>,
	pub background1_control: Volatile<&'static mut BackgroundControlRegister>,
	pub background2_control: Volatile<&'static mut BackgroundControlRegister>,
	pub background3_control: Volatile<&'static mut BackgroundControlRegister>,
	pub background0_offset: Volatile<&'static mut BackgroundOffsetRegister>,
	pub background1_offset: Volatile<&'static mut BackgroundOffsetRegister>,
	pub background2_offset: Volatile<&'static mut BackgroundOffsetRegister>,
	pub background3_offset: Volatile<&'static mut BackgroundOffsetRegister>,
}

#[derive(Default)]
struct DisplayBuffer {
	display_control: DisplayControlRegister,
	background0_control: BackgroundControlRegister,
	background1_control: BackgroundControlRegister,
	background2_control: BackgroundControlRegister,
	background3_control: BackgroundControlRegister,
	background0_offset: BackgroundOffsetRegister,
	background1_offset: BackgroundOffsetRegister,
	background2_offset: BackgroundOffsetRegister,
	background3_offset: BackgroundOffsetRegister,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub union Tiles<const N: usize, const M: usize, const O: usize> {
	tiles_4bpp: [Tile4bpp; N],
	tiles_8bpp: [Tile8bpp; M],
	raw: [u8; O],
}

static_assertions::assert_eq_size!(Tiles<2, 1, 64>, [Tile4bpp; 2]);
static_assertions::assert_eq_size!(Tiles<2, 1, 64>, [Tile8bpp; 1]);
static_assertions::assert_eq_size!(Tiles<2, 1, 64>, [u8; 64]);

impl<const N: usize, const M: usize, const O: usize> Tiles<N, M, O> {
	pub fn borrow_4bpp(&self) -> &[Tile4bpp; N] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[Tile4bpp; N]>()
		);
		unsafe { &self.tiles_4bpp }
	}

	pub fn borrow_8bpp(&self) -> &[Tile8bpp; M] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[Tile8bpp; M]>()
		);
		unsafe { &self.tiles_8bpp }
	}

	pub fn borrow_raw(&self) -> &[u8; O] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[u8; O]>()
		);
		unsafe { &self.raw }
	}

	pub fn borrow_mut_4bpp(&mut self) -> &mut [Tile4bpp; N] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[Tile4bpp; N]>()
		);
		unsafe { &mut self.tiles_4bpp }
	}

	pub fn borrow_mut_8bpp(&mut self) -> &mut [Tile8bpp; M] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[Tile8bpp; M]>()
		);
		unsafe { &mut self.tiles_8bpp }
	}

	pub fn borrow_mut_raw(&mut self) -> &mut [u8; O] {
		debug_assert_eq!(
			core::mem::size_of::<Self>(),
			core::mem::size_of::<[u8; O]>()
		);
		unsafe { &mut self.raw }
	}
}

#[repr(C)]
pub struct TiledVRAM {
	pub bg_tiles: Tiles<1024, 512, { 64 * 1024 }>,
	pub sprite_tiles: Tiles<512, 256, { 32 * 1024 }>,
}

static_assertions::assert_eq_size!(TiledVRAM, [u8; 96 * 1024]);

#[repr(C)]
pub struct BitmapSingleFullColorVRAM {
	pub bitmap: [[BGR5; 240]; 160],
	pub pad0: [u8; 5120],
	pub sprite_tiles: Tiles<256, 128, { 16 * 1024 }>,
}

static_assertions::assert_eq_size!(BitmapSingleFullColorVRAM, [u8; 96 * 1024]);

#[repr(C)]
pub struct BitmapPagedFullColorVRAM {
	pub bitmap_page0: [[BGR5; 160]; 128],
	pub bitmap_page1: [[BGR5; 160]; 128],
	pub sprite_tiles: Tiles<256, 128, { 16 * 1024 }>,
}

static_assertions::assert_eq_size!(BitmapPagedFullColorVRAM, [u8; 96 * 1024]);

#[repr(C)]
pub struct BitmapPagedPalettedVRAM {
	pub page0: [[u16; 120]; 160],
	pub pad0: [u8; 2560],
	pub page1: [[u16; 120]; 160],
	pub pad1: [u8; 2560],
	pub sprite_tiles: Tiles<256, 128, { 16 * 1024 }>,
}

static_assertions::assert_eq_size!(BitmapPagedPalettedVRAM, [u8; 96 * 1024]);

#[repr(C)]
pub union VRAM {
	raw: [u8; 96 * 1024],
	tiled: core::mem::ManuallyDrop<TiledVRAM>,
	bitmap_single_full_color: core::mem::ManuallyDrop<BitmapSingleFullColorVRAM>,
	bitmap_paged_full_color: core::mem::ManuallyDrop<BitmapPagedFullColorVRAM>,
	bitmap_paged_paletted: core::mem::ManuallyDrop<BitmapPagedPalettedVRAM>,
}

static_assertions::assert_eq_size!(VRAM, [u8; 96 * 1024]);

pub struct DisplayControl<V: VideoMode> {
	registers: DisplayRegisters,
	buffer: DisplayBuffer,
	vram: Option<Volatile<&'static mut VRAM>>,
	_video_mode: PhantomData<V>,
}

impl DisplayControl<Tiled4Regular> {
	pub fn new(
		registers: DisplayRegisters,
		vram: Volatile<&'static mut VRAM>,
	) -> DisplayControl<Tiled4Regular> {
		DisplayControl {
			registers,
			vram: Some(vram),
			buffer: DisplayBuffer::default(),
			_video_mode: PhantomData,
		}
	}
}

impl<V> DisplayControl<V>
where
	V: VideoMode,
{
	pub fn commit(&mut self) {
		//TODO: Optimize?
		self.registers
			.display_control
			.write(self.buffer.display_control);

		self.registers
			.background0_control
			.write(self.buffer.background0_control);

		self.registers
			.background1_control
			.write(self.buffer.background1_control);

		self.registers
			.background2_control
			.write(self.buffer.background2_control);

		self.registers
			.background3_control
			.write(self.buffer.background3_control);

		self.registers
			.background0_offset
			.write(self.buffer.background0_offset);

		self.registers
			.background1_offset
			.write(self.buffer.background1_offset);

		self.registers
			.background2_offset
			.write(self.buffer.background2_offset);

		self.registers
			.background3_offset
			.write(self.buffer.background3_offset);
	}

	pub fn set_video_mode<T: VideoMode>(mut self) -> Option<DisplayControl<T>> {
		self.buffer.display_control.set_video_mode(T::video_mode());

		unimplemented!()
		// DisplayControl {
		//     registers: self.registers,
		//     display_ctl: self.display_ctl,
		//     bg0_ctl: self.bg0_ctl,

		//     _video_mode: PhantomData,
		// }
	}

	pub fn sprites_enabled(&self) -> bool {
		self.buffer.display_control.enable_sprites()
	}

	pub fn set_sprites_enabled(&mut self, enable: bool) {
		self.buffer.display_control.set_enable_sprites(enable)
	}

	pub fn sprite_mapping(&self) -> SpriteMappingMode {
		self.buffer.display_control.sprite_mapping()
	}

	pub fn set_sprite_mapping(&mut self, mode: SpriteMappingMode) {
		self.buffer.display_control.set_sprite_mapping(mode)
	}

	pub fn background<B, R>(&self, _: B) -> &BackgroundControl<R>
	where
		B: BackgroundId<V, R>,
		R: RenderMode,
	{
		match B::background_id() {
			BackgroundIds::Bg0 => {
				BackgroundControl::from_register(&self.buffer.background0_control)
			}
			BackgroundIds::Bg1 => {
				BackgroundControl::from_register(&self.buffer.background1_control)
			}
			BackgroundIds::Bg2 => {
				BackgroundControl::from_register(&self.buffer.background2_control)
			}
			BackgroundIds::Bg3 => {
				BackgroundControl::from_register(&self.buffer.background3_control)
			}
		}
	}

	pub fn background_enabled<B, R>(&self, _: B) -> bool
	where
		B: BackgroundId<V, R>,
		R: RenderMode,
	{
		match B::background_id() {
			BackgroundIds::Bg0 => self.buffer.display_control.enable_background0(),
			BackgroundIds::Bg1 => self.buffer.display_control.enable_background1(),
			BackgroundIds::Bg2 => self.buffer.display_control.enable_background2(),
			BackgroundIds::Bg3 => self.buffer.display_control.enable_background3(),
		}
	}

	pub fn set_background_enabled<B, R>(&mut self, enable: bool)
	where
		B: BackgroundId<V, R>,
		R: RenderMode,
	{
		match B::background_id() {
			BackgroundIds::Bg0 => self.buffer.display_control.set_enable_background0(enable),
			BackgroundIds::Bg1 => self.buffer.display_control.set_enable_background1(enable),
			BackgroundIds::Bg2 => self.buffer.display_control.set_enable_background2(enable),
			BackgroundIds::Bg3 => self.buffer.display_control.set_enable_background3(enable),
		}
	}

	pub fn vertical_count(&self) -> Volatile<&'static VerticalCountRegister, ReadOnly> {
		self.registers.vertical_count.clone()
	}
}

impl<V: TiledVideoMode> DisplayControl<V> {
	pub fn take_vram(&mut self) -> Option<Volatile<&'static mut TiledVRAM>> {
		if let Some(vram) = self.vram.take() {
			let vram = vram.extract_inner();

			let vram = unsafe { &mut vram.tiled };

			Some(Volatile::new(vram))
		} else {
			None
		}
	}

	pub fn put_vram(&mut self, vram: Volatile<&'static mut TiledVRAM>) {
		let vram = vram.extract_inner();

		let vram = unsafe { &mut *(vram as *mut TiledVRAM as *mut VRAM) };

		self.vram = Some(Volatile::new(vram));
	}
}

// impl DisplayControl<Tiled4Regular> {
// pub fn background_enabled(&self, background_id: Tiled4RegularBackgrounds) -> bool {
//     match background_id {
//         Tiled4RegularBackgrounds::Regular0 => {
//             self.buffer.display_control.enable_background0()
//         }
//         Tiled4RegularBackgrounds::Regular1 => {
//             self.buffer.display_control.enable_background1()
//         }
//         Tiled4RegularBackgrounds::Regular2 => {
//             self.buffer.display_control.enable_background2()
//         }
//         Tiled4RegularBackgrounds::Regular3 => {
//             self.buffer.display_control.enable_background3()
//         }
//     }
// }
// pub fn set_background_enabled(
//     &mut self,
//     background_id: Tiled4RegularBackgrounds,
//     enable: bool,
// ) {
//     match background_id {
//         Tiled4RegularBackgrounds::Regular0 => {
//             self.buffer.display_control.set_enable_background0(enable)
//         }
//         Tiled4RegularBackgrounds::Regular1 => {
//             self.buffer.display_control.set_enable_background1(enable)
//         }
//         Tiled4RegularBackgrounds::Regular2 => {
//             self.buffer.display_control.set_enable_background2(enable)
//         }
//         Tiled4RegularBackgrounds::Regular3 => {
//             self.buffer.display_control.set_enable_background3(enable)
//         }
//     }
// }
// }

// impl DisplayControl<Tiled2Regular1Affine> {
//     pub fn background_enabled(&self, background_id: Tiled2Regular1AffineBackgrounds) -> bool {
//         match background_id {
//             Tiled2Regular1AffineBackgrounds::Regular0 => {
//                 self.buffer.display_control.enable_background0()
//             }
//             Tiled2Regular1AffineBackgrounds::Regular1 => {
//                 self.buffer.display_control.enable_background1()
//             }
//             Tiled2Regular1AffineBackgrounds::Affine => {
//                 self.buffer.display_control.enable_background2()
//             }
//         }
//     }
//     pub fn set_background_enabled(
//         &mut self,
//         background_id: Tiled2Regular1AffineBackgrounds,
//         enable: bool,
//     ) {
//         match background_id {
//             Tiled2Regular1AffineBackgrounds::Regular0 => {
//                 self.buffer.display_control.set_enable_background0(enable)
//             }
//             Tiled2Regular1AffineBackgrounds::Regular1 => {
//                 self.buffer.display_control.set_enable_background1(enable)
//             }
//             Tiled2Regular1AffineBackgrounds::Affine => {
//                 self.buffer.display_control.set_enable_background2(enable)
//             }
//         }
//     }
// }

// impl DisplayControl<Tiled2Affine> {
//     pub fn background_enabled(&self, background_id: Tiled2AffineBackgrounds) -> bool {
//         match background_id {
//             Tiled2AffineBackgrounds::Affine0 => {
//                 self.buffer.display_control.enable_background2()
//             }
//             Tiled2AffineBackgrounds::Affine1 => {
//                 self.buffer.display_control.enable_background3()
//             }
//         }
//     }
//     pub fn set_background_enabled(&mut self, background_id: Tiled2AffineBackgrounds, enable: bool) {
//         match background_id {
//             Tiled2AffineBackgrounds::Affine0 => {
//                 self.buffer.display_control.set_enable_background2(enable)
//             }
//             Tiled2AffineBackgrounds::Affine1 => {
//                 self.buffer.display_control.set_enable_background3(enable)
//             }
//         }
//     }
// }

impl<V> DisplayControl<V>
where
	V: PagedVideoMode,
{
	pub fn swap_pages(&mut self) {
		self.buffer
			.display_control
			.set_page_select(!self.buffer.display_control.page_select());
	}
}

impl DisplayControl<BitmapSingleFullColor> {
	pub fn take_vram(&mut self) -> Option<Volatile<&'static mut BitmapSingleFullColorVRAM>> {
		if let Some(vram) = self.vram.take() {
			let vram = vram.extract_inner();

			let vram = unsafe { &mut vram.bitmap_single_full_color };

			Some(Volatile::new(vram))
		} else {
			None
		}
	}

	pub fn put_vram(&mut self, vram: Volatile<&'static mut BitmapSingleFullColorVRAM>) {
		let vram = vram.extract_inner();

		let vram = unsafe { &mut *(vram as *mut BitmapSingleFullColorVRAM as *mut VRAM) };

		self.vram = Some(Volatile::new(vram));
	}
}

impl DisplayControl<BitmapPagedFullColor> {
	pub fn take_vram(&mut self) -> Option<Volatile<&'static mut BitmapPagedFullColorVRAM>> {
		if let Some(vram) = self.vram.take() {
			let vram = vram.extract_inner();

			let vram = unsafe { &mut vram.bitmap_paged_full_color };

			Some(Volatile::new(vram))
		} else {
			None
		}
	}

	pub fn put_vram(&mut self, vram: Volatile<&'static mut BitmapPagedFullColorVRAM>) {
		let vram = vram.extract_inner();

		let vram = unsafe { &mut *(vram as *mut BitmapPagedFullColorVRAM as *mut VRAM) };

		self.vram = Some(Volatile::new(vram));
	}
}

impl DisplayControl<BitmapPagedPaletted> {
	pub fn take_vram(&mut self) -> Option<Volatile<&'static mut BitmapPagedPalettedVRAM>> {
		if let Some(vram) = self.vram.take() {
			let vram = vram.extract_inner();

			let vram = unsafe { &mut vram.bitmap_paged_paletted };

			Some(Volatile::new(vram))
		} else {
			None
		}
	}

	pub fn put_vram(&mut self, vram: Volatile<&'static mut BitmapPagedPalettedVRAM>) {
		let vram = vram.extract_inner();

		let vram = unsafe { &mut *(vram as *mut BitmapPagedPalettedVRAM as *mut VRAM) };

		self.vram = Some(Volatile::new(vram));
	}
}
