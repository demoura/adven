// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

/// An BGR555 (15bit) color value.
/// Each component has a max value of 31 (5 bits).
///
/// It is the color format used by the GBA hardware renderer.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
#[repr(transparent)]
pub struct BGR5(pub u16);

impl BGR5 {
	pub const BLACK: BGR5 = BGR5::from_rgb555(0, 0, 0);
	pub const WHITE: BGR5 = BGR5::from_rgb555(31, 31, 31);
	pub const RED: BGR5 = BGR5::from_rgb555(31, 0, 0);
	pub const CYAN: BGR5 = BGR5::from_rgb555(0, 31, 31);
	pub const GREEN: BGR5 = BGR5::from_rgb555(0, 31, 0);
	pub const YELLOW: BGR5 = BGR5::from_rgb555(31, 31, 0);
	pub const BLUE: BGR5 = BGR5::from_rgb555(0, 0, 31);
	pub const PURPLE: BGR5 = BGR5::from_rgb555(31, 0, 31);

	pub const LBLUE: BGR5 = BGR5::from_rgb24(0x5BCEFA);
	pub const LPINK: BGR5 = BGR5::from_rgb24(0xF5A9B8);

	/// Create a color from a raw BGR555 value.
	pub const fn new_bgr15(bgr5: u16) -> BGR5 {
		BGR5(bgr5)
	}

	/// Create a color from it's RGB components.
	/// Each component has a max value of 31.
	/// Input values greater than 31 will be clamped.
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb555(31, 31, 31), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb555(31, 31, 31), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb555(31, 0, 31), BGR5(0x7C1F));
	/// assert_eq!(BGR5::from_rgb555(0, 0, 31), BGR5(0x7C00));
	/// assert_eq!(BGR5::from_rgb555(31, 31, 0), BGR5(0x3FF));
	/// assert_eq!(BGR5::from_rgb555(0, 31, 0), BGR5(0x3E0));
	/// assert_eq!(BGR5::from_rgb555(31, 0, 0), BGR5(0x1F));
	/// assert_eq!(BGR5::from_rgb555(0, 0, 0), BGR5(0));
	/// ```
	/// Intermediate values also work.
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb555(23, 23, 23), BGR5(0x5EF7));
	/// assert_eq!(BGR5::from_rgb555(15, 15, 15), BGR5(0x3DEF));
	/// assert_eq!(BGR5::from_rgb555(7, 7, 7), BGR5(0x1CE7));
	/// ```
	/// Values larger than 31 are clamped to 31.
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb555(255, 255, 255), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb555(32, 32, 32), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb555(31, 31, 31), BGR5(0x7FFF));
	/// ```
	pub const fn from_rgb555(r: u8, g: u8, b: u8) -> BGR5 {
		// Keep only the first 5 bits.
		let r = if r <= 31 { (r & 31) as u16 } else { 31 };
		let g = if g <= 31 { (g & 31) as u16 } else { 31 };
		let b = if b <= 31 { (b & 31) as u16 } else { 31 };

		BGR5((b << 10) | (g << 5) | r)
	}

	/// Create a color from it's RGB components.
	/// Each component has a max input value of 255,
	/// but will be divided by 8 (rounding down) to
	/// output to the [0..31] range.
	///
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb888(255, 255, 255), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb888(255, 0, 255), BGR5(0x7C1F));
	/// assert_eq!(BGR5::from_rgb888(0, 0, 255), BGR5(0x7C00));
	/// assert_eq!(BGR5::from_rgb888(255, 255, 0), BGR5(0x3FF));
	/// assert_eq!(BGR5::from_rgb888(0, 255, 0), BGR5(0x3E0));
	/// assert_eq!(BGR5::from_rgb888(255, 0, 0), BGR5(0x1F));
	/// assert_eq!(BGR5::from_rgb888(0, 0, 0), BGR5(0));
	/// ```
	/// Intermediate values also work.
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb888(191, 191, 191), BGR5(0x5EF7));
	/// assert_eq!(BGR5::from_rgb888(127, 127, 127), BGR5(0x3DEF));
	/// assert_eq!(BGR5::from_rgb888(63, 63, 63), BGR5(0x1CE7));
	/// ```
	pub const fn from_rgb888(r: u8, g: u8, b: u8) -> BGR5 {
		// Divide everything by 8 (256/32).
		let r = r >> 3;
		let g = g >> 3;
		let b = b >> 3;

		Self::from_rgb555(r, g, b)
	}

	/// Create a color from an RGB8 (24bit) value.
	/// Each component has a max input value of 255,
	/// but will be divided by 8 (rounding down) to
	/// output to the [0..31] range.
	///
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb24(0xFF_FF_FF), BGR5(0x7FFF));
	/// assert_eq!(BGR5::from_rgb24(0xFF_00_FF), BGR5(0x7C1F));
	/// assert_eq!(BGR5::from_rgb24(0x00_00_FF), BGR5(0x7C00));
	/// assert_eq!(BGR5::from_rgb24(0xFF_FF_00), BGR5(0x3FF));
	/// assert_eq!(BGR5::from_rgb24(0x00_FF_00), BGR5(0x3E0));
	/// assert_eq!(BGR5::from_rgb24(0xFF_00_00), BGR5(0x1F));
	/// assert_eq!(BGR5::from_rgb24(0x00_00_00), BGR5(0));
	/// ```
	/// Intermediate values also work.
	/// ```
	/// # use adven::color::BGR5;
	/// assert_eq!(BGR5::from_rgb24(0xBE_BE_BE), BGR5(0x5EF7));
	/// assert_eq!(BGR5::from_rgb24(0x7F_7F_7F), BGR5(0x3DEF));
	/// assert_eq!(BGR5::from_rgb24(0x3F_3F_3F), BGR5(0x1CE7));
	/// ```
	pub const fn from_rgb24(rgb8: u32) -> BGR5 {
		let rgb8 = rgb8.to_le_bytes();

		Self::from_rgb888(rgb8[2], rgb8[1], rgb8[0])
	}
}

#[cfg(feature = "repr_writable")]
use repr_writer::{ReprAbi, ReprWritable, ReprWriter};

#[cfg(feature = "repr_writable")]
impl ReprWritable for BGR5 {
	fn align(abi: &ReprAbi) -> usize
	where
		Self: Sized,
	{
		u16::align(abi)
	}
	fn align_dyn(&self, abi: &ReprAbi) -> usize {
		Self::align(abi)
	}

	fn write(&self, writer: &mut dyn ReprWriter) -> repr_writer::Result<()> {
		writer.write_u16(self.0)
	}
}
