// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::sync::atomic::{AtomicU32, Ordering};

const INTERRUPT_MASTER_ENABLE: *const AtomicU32 = 0x400_0208 as *const AtomicU32;

pub fn run_no_interrupts<F, R>(f: F) -> R
where
	F: FnOnce() -> R,
{
	let ime = unsafe { &*INTERRUPT_MASTER_ENABLE };
	let were_interrupts_enabled: bool = ime.swap(0, Ordering::Relaxed) == 1;

	let result = f();

	if were_interrupts_enabled {
		ime.store(0, Ordering::Relaxed);
	}

	result
}
