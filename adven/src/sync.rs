// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#[cfg(feature = "std")]
pub use std::sync::*;

#[cfg(feature = "gba")]
pub use gba::*;

#[cfg(feature = "gba")]
mod gba {
	pub type Mutex<T> = lock_api::Mutex<GbaMutex, T>;
	pub type MutexGuard<'a, T> = lock_api::MutexGuard<'a, GbaMutex, T>;
	pub type RwLock<T> = lock_api::RwLock<GbaRwLock, T>;
	pub type RwLockReadGuard<'a, T> = lock_api::RwLockReadGuard<'a, GbaRwLock, T>;
	pub type RwLockWriteGuard<'a, T> = lock_api::RwLockWriteGuard<'a, GbaRwLock, T>;

	use core::sync::atomic::{AtomicBool, AtomicUsize, Ordering};

	use lock_api::{GuardNoSend, RawMutex, RawRwLock};

	use crate::interrupt::run_no_interrupts;

	pub struct GbaMutex {
		locked: AtomicBool,
	}

	unsafe impl Sync for GbaMutex {}

	unsafe impl RawMutex for GbaMutex {
		const INIT: GbaMutex = GbaMutex {
			locked: AtomicBool::new(false),
		};

		// There's really no other threads to send a guard to.
		// So if anything is relying on Send guards it is probably very wrong.
		type GuardMarker = GuardNoSend;

		fn lock(&self) {
			while !self.try_lock() {}
		}

		fn try_lock(&self) -> bool {
			self.locked
				.compare_exchange(false, true, Ordering::Relaxed, Ordering::Relaxed)
				.is_ok()
		}

		unsafe fn unlock(&self) {
			self.locked.store(false, Ordering::Relaxed);
		}
	}

	pub struct GbaRwLock {
		shared_lock_count: AtomicUsize,
		exclusive_lock: AtomicBool,
	}

	unsafe impl RawRwLock for GbaRwLock {
		const INIT: GbaRwLock = GbaRwLock {
			shared_lock_count: AtomicUsize::new(0),
			exclusive_lock: AtomicBool::new(false),
		};

		type GuardMarker = GuardNoSend;

		fn lock_shared(&self) {
			while !self.try_lock_shared() {}
		}

		fn try_lock_shared(&self) -> bool {
			run_no_interrupts(|| {
				if !self.exclusive_lock.load(Ordering::Acquire) {
					self.shared_lock_count.fetch_add(1, Ordering::Release);
					return true;
				}

				false
			})
		}

		unsafe fn unlock_shared(&self) {
			run_no_interrupts(|| {
				let locks = self.shared_lock_count.load(Ordering::Acquire);
				self.shared_lock_count.store(locks - 1, Ordering::Release);
			})
		}

		fn lock_exclusive(&self) {
			while !self.try_lock_exclusive() {}
		}

		fn try_lock_exclusive(&self) -> bool {
			run_no_interrupts(|| {
				if !self.exclusive_lock.load(Ordering::Acquire)
					&& self.shared_lock_count.load(Ordering::Relaxed) == 0
				{
					self.exclusive_lock.store(true, Ordering::Release);
					return true;
				}

				false
			})
		}

		unsafe fn unlock_exclusive(&self) {
			self.exclusive_lock.store(false, Ordering::Relaxed);
		}
	}
}
