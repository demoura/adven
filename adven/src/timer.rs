// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::ops::Range;

use bitvec::prelude::*;
use volatile::Volatile;

#[derive(Copy, Clone, Eq, PartialEq)]
#[repr(u16)]
pub enum TimerFrequency {
	Cycles1 = 0,
	Cycles64 = 1,
	Cycles256 = 2,
	Cycles1024 = 3,
}

#[derive(Copy, Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct TimerControl(BitArray<u16, Lsb0>);

impl TimerControl {
	const FREQUENCY: Range<usize> = 0..2;
	const CASCADE: usize = 2;
	const INTERRUPT: usize = 6;
	const ENABLE: usize = 7;

	pub fn enable(&mut self, enable: bool) {
		self.0.set(Self::ENABLE, enable);
	}

	pub fn enable_cascade_mode(&mut self, enable: bool) {
		self.0.set(Self::CASCADE, enable)
	}

	pub fn enable_interrupt(&mut self, enable: bool) {
		self.0.set(Self::INTERRUPT, enable)
	}

	pub fn get_frequency(&self) -> TimerFrequency {
		match self.0[Self::FREQUENCY].load::<u16>() {
			0 => TimerFrequency::Cycles1,
			1 => TimerFrequency::Cycles64,
			2 => TimerFrequency::Cycles256,
			3 => TimerFrequency::Cycles1024,
			_ => panic!("Unexpected value"),
		}
	}

	pub fn is_enabled(&self) -> bool {
		self.0[Self::ENABLE]
	}

	pub fn is_cascade_mode_enabled(&self) -> bool {
		self.0[Self::CASCADE]
	}

	pub fn is_interrupt_enabled(&self) -> bool {
		self.0[Self::INTERRUPT]
	}

	pub fn set_frequency(&mut self, freq: TimerFrequency) {
		self.0[Self::FREQUENCY].store(freq as u16);
	}
}

#[repr(transparent)]
pub struct TimerStateRegister(Volatile<&'static mut u16>);

impl TimerStateRegister {
	#[cfg(feature = "gba")]
	pub(crate) fn new(register: Volatile<&'static mut u16>) -> Self {
		Self(register)
	}

	pub fn get_current_cycles(&self) -> u16 {
		self.0.read()
	}

	pub fn set_initial_cycles(&mut self, cycles: u16) {
		self.0.write(cycles);
	}
}
