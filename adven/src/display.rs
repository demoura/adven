// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

mod affine;
mod bg_control;
mod display_control;
mod palette;
mod sprite;
mod tile;

pub use bg_control::*;
pub use display_control::*;
pub use palette::*;
pub use sprite::*;
pub use tile::*;

pub mod gba {
	pub use super::sprite::gba::*;
}

use core::{cmp::Ordering, convert::TryFrom, ops::Range};

#[cfg(feature = "serde_derive")]
use serde::{Deserialize, Serialize};

use super::color::BGR5;

pub trait RenderMode {}

pub struct RegularRenderMode;
impl RenderMode for RegularRenderMode {}

pub struct AffineRenderMode;
impl RenderMode for AffineRenderMode {}

#[derive(Debug)]
pub struct OutOfRange<T = usize> {
	pub range: Range<T>,
	pub error_value: T,
}

impl<T> OutOfRange<T> {
	pub fn new(error_value: T, range: Range<T>) -> Self {
		Self { range, error_value }
	}
}

#[repr(u8)]
#[cfg_attr(feature = "serde_derive", derive(Serialize, Deserialize))]
#[derive(Copy, Clone, Debug)]
pub enum ColorMode {
	Color4Bit = 0,
	Color8Bit = 1,
}

impl ColorMode {
	pub const fn pixels_per_byte(self) -> u8 {
		match self {
			ColorMode::Color4Bit => 2,
			ColorMode::Color8Bit => 1,
		}
	}
}

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum FlipMode {
	None,
	Horizontal,
	Vertical,
	Both,
}

impl FlipMode {
	pub fn new(flip_horizontal: bool, flip_vertical: bool) -> Self {
		if flip_horizontal && flip_vertical {
			FlipMode::Both
		} else if flip_horizontal {
			FlipMode::Horizontal
		} else if flip_vertical {
			FlipMode::Vertical
		} else {
			FlipMode::None
		}
	}

	pub fn flip_horizontal(self) -> bool {
		matches!(self, FlipMode::Horizontal | FlipMode::Both)
	}

	pub fn flip_vertical(self) -> bool {
		matches!(self, FlipMode::Vertical | FlipMode::Both)
	}

	pub fn set_flip_horizontal(&mut self, flip: bool) {
		if flip {
			match self {
				FlipMode::None => *self = FlipMode::Horizontal,
				FlipMode::Vertical => *self = FlipMode::Both,
				FlipMode::Both | FlipMode::Horizontal => (),
			}
		} else {
			match self {
				FlipMode::Horizontal => *self = FlipMode::None,
				FlipMode::Both => *self = FlipMode::Vertical,
				FlipMode::None | FlipMode::Vertical => (),
			}
		}
	}

	pub fn set_flip_vertical(&mut self, flip: bool) {
		if flip {
			match self {
				FlipMode::None => *self = FlipMode::Vertical,
				FlipMode::Horizontal => *self = FlipMode::Both,
				FlipMode::Both | FlipMode::Vertical => (),
			}
		} else {
			match self {
				FlipMode::Vertical => *self = FlipMode::None,
				FlipMode::Both => *self = FlipMode::Horizontal,
				FlipMode::None | FlipMode::Horizontal => (),
			}
		}
	}
}

pub const VBLANK_START: u16 = 160;
pub const VBLANK_END: u16 = 227;
pub type VerticalCountRegister = u16;

#[repr(u8)]
#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum Priority {
	High = 0,
	Medium = 1,
	Low = 2,
}

impl Ord for Priority {
	fn cmp(&self, other: &Self) -> Ordering {
		// Priority order is basically reverse integer order.
		// 0 (High) is higher priority than 1 (Medium) and 2 (Low)
		(*self as u8).cmp(&(*other as u8)).reverse()
	}
}

impl PartialOrd for Priority {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

macro_rules! impl_try_from {
	($t:tt) => {
		impl TryFrom<$t> for Priority {
			type Error = OutOfRange<$t>;

			fn try_from(val: $t) -> Result<Self, Self::Error> {
				match val {
					0 => Ok(Priority::High),
					1 => Ok(Priority::Medium),
					2 => Ok(Priority::Low),
					_ => Err(OutOfRange::new(val, 0..3)),
				}
			}
		}
	};
}

impl_try_from!(u8);
impl_try_from!(u16);
impl_try_from!(u32);
impl_try_from!(u64);
impl_try_from!(u128);
impl_try_from!(usize);
impl_try_from!(i8);
impl_try_from!(i16);
impl_try_from!(i32);
impl_try_from!(i64);
impl_try_from!(i128);
impl_try_from!(isize);
