// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use alloc::vec::Vec;
use crate::{
    Transform,
    badecs::*,
    sprite::*,
//    register_bank::{GBA_REGISTER_BANK, SPRITE_ATTR_COUNT},
};
use hashbrown::HashMap;

#[cfg_attr(debug_assertions, derive(Debug))]
#[derive(Copy, Clone, Component)]
pub struct SpriteRender {
    sprite: &'static SpriteAsset,
    horizontal_flip: bool,
    vertical_flip: bool,
    sprite_priority: i32,
    frame: u16
}

#[derive(Default)]
pub struct SpriteRendererSystem {}

impl System for SpriteRendererSystem {
    fn run(&mut self, entity_manager: &mut EntityManager, resource_manager: &mut ResourceManager) {
        let query = entity_manager.query(query!{SpriteRender, Transform});
  //      let mut regbank = GBA_REGISTER_BANK;

        for entity in query {
            let tranform: &Transform = entity_manager.component(entity).unwrap();
            let sprite: &SpriteRender = entity_manager.component(entity).unwrap();

        }
    }
}
