// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![cfg_attr(not(feature = "std"), no_std)]
#![warn(clippy::all)]

pub mod color;
pub mod display;
pub mod keypad;
#[cfg(feature = "gba")]
pub mod register_bank;
// pub mod sprite_renderer;
#[cfg(feature = "gba")]
pub mod interrupt;
pub mod sync;
pub mod timer;

pub use adven_math as math;
pub use repr_writer;
pub use repr_writer::SliceRef;
pub use volatile;
