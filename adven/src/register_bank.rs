// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Register arch:
// For "normal" register define a Volatile<&mut Register>,
// then you can read it at once, modify it as you wish,
// then write it back when you want.
//
// For register with non stardard write or read behaviours
// such as TimerStateRegister, define a type that wraps the
// volatile accessor and exposes functions.

use volatile::{access::ReadOnly, Volatile};

use crate::color::BGR5;
use crate::display::{gba::SpriteAttr, DisplayRegisters, VRAM};
use crate::keypad::KeyInput;
use crate::timer::{TimerControl, TimerStateRegister};

pub struct HardwareRegisterBank {
	pub display: DisplayRegisters,

	// Sound
	// DMA

	// Timers
	pub timer0_state: TimerStateRegister,
	pub timer0_control: Volatile<&'static mut TimerControl>,
	pub timer1_state: TimerStateRegister,
	pub timer1_control: Volatile<&'static mut TimerControl>,
	pub timer2_state: TimerStateRegister,
	pub timer2_control: Volatile<&'static mut TimerControl>,
	pub timer3_state: TimerStateRegister,
	pub timer3_control: Volatile<&'static mut TimerControl>,

	// Serial

	// Input
	pub key_input: Volatile<&'static mut KeyInput, ReadOnly>,

	// System Control

	// Memory
	pub bg_palette: Volatile<&'static mut [BGR5; 256]>,
	pub sprite_palette: Volatile<&'static mut [BGR5; 256]>,
	pub vram: Volatile<&'static mut VRAM>,
	pub sprite_control: Volatile<&'static mut [SpriteAttr; 128]>,
}

unsafe fn reg<T>(address: usize) -> &'static mut T {
	(address as *mut T).as_mut().unwrap()
}

impl HardwareRegisterBank {
	pub unsafe fn single_time_init() -> Self {
		Self {
			display: DisplayRegisters {
				display_control: Volatile::new(reg(0x0400_0000)),
				vertical_count: Volatile::new_read_only(reg(0x0400_0006)),
				background0_control: Volatile::new(reg(0x0400_0008)),
				background1_control: Volatile::new(reg(0x0400_000A)),
				background2_control: Volatile::new(reg(0x0400_000C)),
				background3_control: Volatile::new(reg(0x0400_000E)),
				background0_offset: Volatile::new(reg(0x0400_0010)),
				background1_offset: Volatile::new(reg(0x0400_0012)),
				background2_offset: Volatile::new(reg(0x0400_0014)),
				background3_offset: Volatile::new(reg(0x0400_0016)),
			},

			timer0_state: TimerStateRegister::new(Volatile::new(reg(0x0400_0100))),
			timer0_control: Volatile::new(reg(0x0400_0102)),
			timer1_state: TimerStateRegister::new(Volatile::new(reg(0x0400_0104))),
			timer1_control: Volatile::new(reg(0x0400_0106)),
			timer2_state: TimerStateRegister::new(Volatile::new(reg(0x0400_0108))),
			timer2_control: Volatile::new(reg(0x0400_010A)),
			timer3_state: TimerStateRegister::new(Volatile::new(reg(0x0400_010C))),
			timer3_control: Volatile::new(reg(0x0400_010E)),

			key_input: Volatile::new_read_only(reg(0x0400_0130)),
			bg_palette: Volatile::new(reg(0x0500_0000)),
			sprite_palette: Volatile::new(reg(0x0500_0200)),
			vram: Volatile::new(reg(0x0600_0000)),
			sprite_control: Volatile::new(reg(0x0700_0000)),
		}
	}
}
