// adven <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use core::mem;

#[derive(Copy, Clone, Debug)]
#[repr(u16)]
pub enum Key {
	A = 0b1,
	B = 0b10,
	Select = 0b100,
	Start = 0b1000,
	Right = 0b10000,
	Left = 0b100000,
	Up = 0b1000000,
	Down = 0b10000000,
	R = 0b100000000,
	L = 0b1000000000,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct KeyInput(pub u16);

impl KeyInput {
	pub fn key_pressed(self, key: Key) -> bool {
		(self.0 & key as u16) == 0
	}
}

#[derive(Copy, Clone)]
pub struct Keypad {
	previous: KeyInput,
	current: KeyInput,
}

impl Keypad {
	pub fn new(keyinput: KeyInput) -> Keypad {
		Keypad {
			previous: KeyInput(0),
			current: keyinput,
		}
	}

	pub fn update(&mut self, new: KeyInput) {
		mem::swap(&mut self.previous, &mut self.current);
		self.current = new;
	}

	pub fn key_pressed(self, key: Key) -> bool {
		self.current.key_pressed(key)
	}

	pub fn key_held(self, key: Key) -> bool {
		self.previous.key_pressed(key) && self.current.key_pressed(key)
	}

	pub fn on_key_down(self, key: Key) -> bool {
		!self.previous.key_pressed(key) && self.current.key_pressed(key)
	}

	pub fn on_key_up(self, key: Key) -> bool {
		self.previous.key_pressed(key) && !self.current.key_pressed(key)
	}
}
