// mgba-logger <https://gitlab.com/demoura/adven>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![no_std]
#![feature(core_intrinsics)]

// extern crate alloc;

// use alloc::format;
use core::{cmp, fmt::Write, intrinsics, ptr, write};

use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};

const REG_ENABLE_DEBUG: *mut u16 = 0x04FF_F780 as *mut u16;
const REG_DEBUG_FLAGS: *mut u16 = 0x04FF_F700 as *mut u16;
const REG_DEBUG_STRING: *mut u8 = 0x04FF_F600 as *mut u8;

const DEBUG_SEND_FLAG: u16 = 0x100;
const DEBUG_ENABLE: u16 = 0xC0DE;
const DEBUG_ENABLED: u16 = 0x1DEA;
const DEBUG_STR_SIZE: usize = 256;

static MGBA_LOGGER: MgbaLogger = MgbaLogger {};

#[repr(u16)]
#[derive(Copy, Clone)]
pub enum LogLevel {
	Fatal = 0,
	Error = 1,
	Warn = 2,
	Info = 3,
	Debug = 4,
}

pub fn init() -> Result<(), SetLoggerError> {
	// GBA has no atomics but also no threads.
	unsafe { log::set_logger_racy(&MGBA_LOGGER).map(|()| log::set_max_level(LevelFilter::max())) }
}

/// Attempts to enable mGBA debugging.
/// Returns true on success. Will only fail in non mGBA enviroments.
pub fn mgba_enable_debug() -> bool {
	unsafe {
		ptr::write_volatile(REG_ENABLE_DEBUG, DEBUG_ENABLE);
		ptr::read_volatile(REG_ENABLE_DEBUG) == DEBUG_ENABLED
	}
}

pub fn mgba_write_string(string: &str, log_level: LogLevel) {
	unsafe {
		let src = string.as_ptr();
		let dest = REG_DEBUG_STRING;
		// Min amount of bytes up to 254, leaving char 255 for the terminator.
		let len = cmp::min(string.len(), DEBUG_STR_SIZE - 1);
		// Volatile copy, because this program never reads from the DEBUG_STRING, but the emulator does.
		intrinsics::volatile_copy_nonoverlapping_memory(dest, src, len);
		// Set the desired log level, and "send" the string.
		ptr::write_volatile(REG_DEBUG_FLAGS, log_level as u16 | DEBUG_SEND_FLAG);
	}
}

//TODO: Buffer writes
pub struct MgbaWriteAdapter {
	log_level: LogLevel,
	buffer: [u8; DEBUG_STR_SIZE],
	position: usize,
}

impl MgbaWriteAdapter {
	pub fn new(log_level: LogLevel) -> MgbaWriteAdapter {
		MgbaWriteAdapter {
			log_level,
			buffer: [0; 256],
			position: 0,
		}
	}

	fn flush(&mut self) {
		// There's no need for a null terminator when filling the whole buffer.
		if self.position < self.buffer.len() {
			self.buffer[self.position] = 0;
		}

		unsafe {
			let src = self.buffer.as_ptr();
			let dest = REG_DEBUG_STRING;
			let len = cmp::min(self.position, DEBUG_STR_SIZE);
			// Volatile copy, because this program never reads from the DEBUG_STRING, but the emulator does.
			intrinsics::volatile_copy_nonoverlapping_memory(dest, src, len);
			// Set the desired log level, and "send" the string.
			ptr::write_volatile(REG_DEBUG_FLAGS, self.log_level as u16 | DEBUG_SEND_FLAG);
		}

		self.position = 0;
	}
}

impl Write for MgbaWriteAdapter {
	fn write_str(&mut self, input: &str) -> core::fmt::Result {
		let mut input_position = 0;

		while input_position < input.len() {
			let buffer_len = self.buffer.len();
			let dst =
				&mut self.buffer[self.position..cmp::min(self.position + input.len(), buffer_len)];
			let src = &input.as_bytes()[input_position..cmp::min(input.len(), dst.len())];
			dst.copy_from_slice(src);

			input_position += src.len();
			self.position += src.len();

			if self.position == self.buffer.len() {
				self.flush();
			}
		}

		// for chunk in input.as_bytes().chunks(DEBUG_STR_SIZE) {
		//     unsafe {
		//         let src = chunk.as_ptr();
		//         let dest = REG_DEBUG_STRING;
		//         // Min amount of bytes up to 254, leaving char 255 for the terminator.
		//         let len = cmp::min(chunk.len(), DEBUG_STR_SIZE - 1);
		//         // Volatile copy, because this program never reads from the DEBUG_STRING, but the emulator does.
		//         intrinsics::volatile_copy_nonoverlapping_memory(dest, src, len);
		//         // Set the desired log level, and "send" the string.
		//         ptr::write_volatile(REG_DEBUG_FLAGS, self.log_level as u16 | DEBUG_SEND_FLAG);
		//     }
		// }

		Ok(())
	}
}

impl Drop for MgbaWriteAdapter {
	fn drop(&mut self) {
		if self.position > 0 {
			self.flush()
		}
	}
}

//TODO: Add Mutex
struct MgbaLogger {}

impl log::Log for MgbaLogger {
	fn enabled(&self, _metadata: &Metadata) -> bool {
		mgba_enable_debug()
	}

	fn log(&self, record: &Record) {
		if self.enabled(record.metadata()) {
			// let msg = format!("[{}]: {}", record.level(), record.args());

			let log_level = match record.level() {
				Level::Trace => LogLevel::Debug,
				Level::Debug => LogLevel::Debug,
				Level::Info => LogLevel::Info,
				Level::Warn => LogLevel::Warn,
				Level::Error => LogLevel::Error,
			};

			let mut writer = MgbaWriteAdapter::new(log_level);

			let _ = write!(writer, "[{}]: {}", record.level(), record.args());

			// mgba_write_string(&msg, log_level);
		}
	}

	fn flush(&self) {}
}
